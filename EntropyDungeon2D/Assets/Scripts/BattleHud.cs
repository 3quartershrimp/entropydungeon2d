using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleHud : MonoBehaviour
{
    public Text nameText;
    public Text speedText;
    public Text damageText;
    public Text armorText;
    public Text dodgeText;
    public Text accuracyText;
    public Slider hpSlider;

    public Unit attatchedUnit;

    public void SetHP(float hp)
    {
        hpSlider.value = hp;
    }
    public virtual void Update()
    {
        if (attatchedUnit != null)
        {
            UpdateText();
        }
    }
    public virtual void UpdateText()
    {
        if (attatchedUnit != null)
        {
            nameText.text = attatchedUnit.unitName;
            hpSlider.maxValue = attatchedUnit.maxHP;
            hpSlider.value = attatchedUnit.currentHP;
            speedText.text = attatchedUnit.speed.ToString();
            damageText.text = attatchedUnit.damageMultiplier.ToString();
            armorText.text = attatchedUnit.armor.ToString();
            dodgeText.text = attatchedUnit.dodge.ToString();
            accuracyText.text = attatchedUnit.accuracy.ToString();
        }
    }

}
