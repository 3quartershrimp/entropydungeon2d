using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{

    public static List<string> usedUnitPositions = new List<string>();
    public static Card activeCard = null;
    public static Unit currentTurnUnit;
    public static List<Vector2> positionTransforms = new List<Vector2>();

    public static void SetRandomSeed()
    {
        UnityEngine.Random.InitState((int)DateTime.Now.Ticks);
    }
    public static bool DetermineTargetHit(Unit attacker, Unit target)
    {
        if (attacker.type == target.type)
        {
            return true;
        }
        SetRandomSeed();
        int roll = UnityEngine.Random.Range(1, 10);
        if (roll == 1)
        {
            Debug.Log(attacker + " is attacking " + target + " and AttackRolled a:" + roll + " MISS!");
            return false;
        }
        else if (roll == 10)
        {
            Debug.Log(attacker + " is attacking " + target + " and AttackRolled a:" + roll + " HIT!");
            return true;
        }
        else
        {
            if (roll + (attacker.accuracy - target.dodge) > 1)
            {
                Debug.Log(attacker + " is attacking " + target + " and AttackRolled a:" + roll + " HIT!");
                return true;
            }
            else
            {
                Debug.Log(attacker + " is attacking " + target + " and AttackRolled a:" + roll + " Miss!");
                return false;
            }

            //return (roll + (attacker.accuracy - target.dodge) > 1);
        }
    }

    public static void DeactivateCard()
    {
        activeCard = null;
    }
    public static void ActivateCard(Card card)
    {
        activeCard = card;
    }
    public static Unit GetUnitFromPositionNumber(int pos)
    {
        GameObject targetedObject = GameObject.Find("Pos" + pos).transform.GetChild(0).gameObject;
        return targetedObject.GetComponent<Unit>();
    }
    public static List<int> UnusedUnitPositions()
    {
        List<int> unusedUnitPositions = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7 };
        for (int i = 0; i < usedUnitPositions.Count; i++)
        {
            unusedUnitPositions.Remove(Int32.Parse(usedUnitPositions[i]));
        }
        return unusedUnitPositions;
    }
}
