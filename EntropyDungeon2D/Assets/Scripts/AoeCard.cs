// using System.Collections.Generic;
// using UnityEngine;

// public class AoeCard : Card
// {
//     public bool aoe = true;


//     Dictionary<Unit, int> aoeTargetIndex = new Dictionary<Unit, int>();
//     // Start is called before the first frame update

//     //If range is 2 set head targets as the one on the left if range is 3 set head target as one in the middle

//     public override void OnMouseDown()
//     {
//         base.OnMouseDown();
//         GameManager.activeAoeCard = true;
//         if (active)
//         {
//             SetAoETargets();
//         }

//     }
//     public void SetAoETargets()
//     {
//         for (int i = 0; i < headTargets.Count; i++)
//         {
//             float leftMostTarget = 0;
//             float rightMostTarget = 0;
//             if (i == 0)
//             {
//                 leftMostTarget = 1f;
//             }
//             if (i == headTargets.Count - 1)
//             {
//                 rightMostTarget = 1f;
//             }

//             int currentHeadTarget = headTargets[i];
//             for (int j = 0; j < range; j++)
//             {
//                 if (range == 1)
//                 {
//                     GameManager.GetUnitFromPositionNumber(currentHeadTarget).SetHighLightMargins(GameManager.positionTransforms[currentHeadTarget].x - leftMostTarget, GameManager.positionTransforms[currentHeadTarget].x + rightMostTarget);
//                 }
//                 if (range == 2)
//                 {
//                     GameManager.GetUnitFromPositionNumber(currentHeadTarget + j).SetHighLightMargins(GameManager.positionTransforms[currentHeadTarget].x - leftMostTarget, GameManager.positionTransforms[currentHeadTarget + 1].x + rightMostTarget);
//                 }
//                 else if (range == 3)
//                 {
//                     GameManager.GetUnitFromPositionNumber(currentHeadTarget - 1 + j).SetHighLightMargins(GameManager.positionTransforms[currentHeadTarget - 1].x - leftMostTarget, GameManager.positionTransforms[currentHeadTarget + 1].x + rightMostTarget);
//                 }
//                 else if (range == 4)
//                 {
//                     GameManager.GetUnitFromPositionNumber(currentHeadTarget + j).SetHighLightMargins(GameManager.positionTransforms[currentHeadTarget].x - leftMostTarget, GameManager.positionTransforms[currentHeadTarget + 3].x + rightMostTarget);
//                 }
//             }
//         }
//     }
//     //     aoeTargetIndex = new Dictionary<Unit, int>();
//     //     List<int[]> notDeadTargets = new List<int[]>();
//     //     foreach (int[] targetArray in targets)
//     //     {
//     //         int notDeadTargetArrayLength = targetArray.Length;
//     //         int[] aliveTargetArray = new int[0];
//     //         foreach (int target in targetArray)
//     //         {
//     //             if (!GameManager.GetUnitFromPositionNumber(target).isDead)
//     //             {
//     //                 Array.Resize(ref aliveTargetArray, aliveTargetArray.Length + 1);
//     //                 aliveTargetArray[aliveTargetArray.GetUpperBound(0)] = GetUnitFromPositionNumber(target);
//     //             }
//     //         }

//     //         for (int j = 0; j < notDeadTargetArrayLength; j++)
//     //         {
//     //             notDeadTargetArray[j] = notDeadTargetList[j];
//     //         }
//     //         notDeadTargets.Add(notDeadTargetArray);
//     //     }
//     //     List<int> unsharedAoETargets = new List<int>();

//     //     int i = 0;
//     //     foreach (int[] targetArray in notDeadTargets)
//     //     {
//     //         bool newHeadTarget = false;
//     //         Unit attackHeadTarget = null;
//     //         List<Unit> addAoETargetPostLoop = new List<Unit>();
//     //         foreach (int target in targetArray)
//     //         {
//     //             unsharedAoETargets.Add(target);
//     //             Unit unitTarget = GameManager.GetUnitFromPositionNumber(target);
//     //             if (!newHeadTarget)
//     //             {
//     //                 newHeadTarget = true;
//     //                 if (!unitTarget.headTargetBool)
//     //                 {
//     //                     aoeTargetIndex.Add(unitTarget, i);
//     //                     unitTarget.headTargetBool = true;
//     //                     attackHeadTarget = unitTarget;
//     //                 }

//     //             }
//     //             if (attackHeadTarget)
//     //             {
//     //                 unitTarget.AddAoETargetListener(attackHeadTarget);
//     //             }
//     //             else
//     //             {
//     //                 addAoETargetPostLoop.Add(unitTarget);
//     //             }
//     //         }
//     //         foreach (Unit unit in addAoETargetPostLoop)
//     //         {
//     //             unit.AddAoETargetListener(attackHeadTarget);
//     //         }
//     //         i++;
//     //     }
//     //     IEnumerable<int> duplicates = unsharedAoETargets.GroupBy(x => x)
//     //                                     .Where(g => g.Count() > 1)
//     //                                     .Select(x => x.Key);
//     //     foreach (int target in duplicates)
//     //     {
//     //         unsharedAoETargets.Remove(target);
//     //     }
//     //     i = 0;
//     //     foreach (int[] targetArray in notDeadTargets)
//     //     {
//     //         foreach (int target in targetArray)
//     //         {
//     //             if (unsharedAoETargets.Contains(target))
//     //             {
//     //                 Unit newHeadTargetUnit = GameManager.GetUnitFromPositionNumber(target);
//     //                 if (!newHeadTargetUnit.headTargetBool)
//     //                 {
//     //                     aoeTargetIndex.Add(newHeadTargetUnit, i);
//     //                     newHeadTargetUnit.headTargetBool = true;
//     //                     for (int j = 0; j < targetArray.Length; j++)
//     //                     {
//     //                         Unit unitToAddListener = GameManager.GetUnitFromPositionNumber(targetArray[j]);
//     //                         if (!unitToAddListener.headTargetUnits.Contains(newHeadTargetUnit))
//     //                         {
//     //                             unitToAddListener.AddAoETargetListener(newHeadTargetUnit);
//     //                         }
//     //                     }
//     //                 }

//     //             }
//     //         }
//     //         i++;
//     //     }

//     // }
//     public override void RemoveTargets()
//     {
//         base.RemoveTargets();
//         GameManager.activeAoeCard = false;

//         for (int i = 0; i < 8; i++)
//         {
//             Unit target = GameManager.GetUnitFromPositionNumber(i);
//             target.RemoveHighlight();
//             target.ResetMargins();
//         }
//     }
//     public override void TriggerCard(Player cardConsumer, Unit targetUnit)
//     {
//         Debug.Log("Card Triggered");
//         EventManager.CardActivatedClick -= TriggerCard;
//         EventManager.OnRightClick -= RemoveTargets;

//         cardConsumer.accuracy += precision;
//         bool attackMiss = GameManager.DetermineTargetHit(cardConsumer, targetUnit);
//         TriggerAttackAoe(cardConsumer, targetUnit);
//         cardConsumer.accuracy -= precision;


//         DeactivateCard();
//         HideCard();
//         MoveToDiscardPile();
//         //Invoke("moveToDiscardPile", 2f);
//     }
//     public void TriggerAttackAoe(Player cardConsumer, Unit targetUnit)
//     {
//         // EventManager.ShowAoETargets = null;
//         // EventManager.OnRemoveAoEHighlight();
//         bool missAnimation = true;
//         cardConsumer.accuracy += precision;
//         for (int i = 0; i < 8; i++)
//         {
//             Unit target = GameManager.GetUnitFromPositionNumber(i);
//             target.ResetMargins();

//             if (target.highlighted)
//             {
//                 Debug.Log("Execute Card on " + i);
//                 if (!target.isDead)
//                 {
//                     if (GameManager.DetermineTargetHit(cardConsumer, target))
//                     {
//                         missAnimation = false;
//                         CardEffects(target, this.owningDeck.owner);

//                     }
//                 }
//             }

//         }
//         GameManager.activeAoeCard = false;
//         EventManager.OnRemoveAoEHighlight();
//         cardConsumer.accuracy -= precision;
//         if (missAnimation)
//         {
//             cardConsumer.TriggerMiss(attackAnimation, cardName);
//         }
//         else
//         {
//             cardConsumer.TriggerAttack(attackAnimation, cardName);
//         }
//     }
// }
