using UnityEngine;


public class EventManager : MonoBehaviour
{

    //Triggers if unit is clicked
    public delegate void UnitClicked();
    public static UnitClicked FocusBattleHud;

    public delegate void RightClick();
    public static RightClick OnRightClick;
    public delegate void AoeActivate();
    public static AoeActivate OnAoeActivateCard;
    public delegate void CardTargetChosen(Player cardComsumer, Unit clickedUnit);
    public static CardTargetChosen CardActivatedClick;
    public delegate void RemoveActiveCards(Player previousPlayer);
    public static RemoveActiveCards DisableCards;
    public delegate void AoeTargetDelegator(Unit headTarget);
    public static AoeTargetDelegator ShowAoETargets;
    public delegate void RemoveHighlight();
    public static RemoveHighlight OnRemoveHighlight;
    public delegate void AttackAnnouncer(string unitname, string attackName);
    public static AttackAnnouncer OnAttackAnnounce;
    public delegate void MissAnnouncer(string unitname, string attackName);
    public static MissAnnouncer OnMissAnnounce;
    //Events used by persistants
    public delegate void TurnEnd(Unit turnEndUnit);
    public static TurnEnd OnTurnEnd;
    public delegate void TurnStart(Unit turnStartUnit);
    public static TurnStart OnTurnStart;
    public delegate void UnitDeath(Unit deadUnit);
    public static UnitDeath OnUnitDeath;
    public delegate void UnitDamaged(Unit hurtUnit);
    public static UnitDamaged OnUnitDamaged;
    public delegate void UnitAttacked(Unit attackingUnit);
    public static UnitAttacked OnUnitAttacked;

    public delegate void TestEventManagerUp();
    public static TestEventManagerUp DebugTestEventManager;


    int unitLayer = 6;


    void Awake()
    {
        DebugTestEventManager += DebugAnnounce;
    }
    void DebugAnnounce()
    {
        Debug.Log("Event manager up");
    }
    // void AddActiveAoEHighlight(List<int> headTargets, int range){
    //     for(int i = 0; i < headTargets.Count; i++){
    //                 int leftMostTarget = 0;
    //     int rightMostTarget = 0;
    //     if (i == 0){
    //         leftMostTarget = 1;
    //     }
    //     if (i == headTargets.Count - 1){
    //         rightMostTarget = 1;
    //     }
    //         if (range == 2){
    //                 if (Camera.main.ScreenToWorldPoint(GameManager.positionTransforms[headTargets[i]].x - leftMostTarget <  Input.mousePosition.x && Input.mousePosition.x < GameManager.positionTransforms[headTargets[i+1]].x + rightMostTarget)){

    //                 }

    //         }
    //         else if(range == 3){
    //             if (Camera.main.ScreenToWorldPoint(GameManager.positionTransforms[headTargets[i - 1]].x - leftMostTarget <  Input.mousePosition.x && Input.mousePosition.x < GameManager.positionTransforms[headTargets[i+1]].x + rightMostTarget)){

    //                 }

    //         }else if (range == 4){
    //             if (Camera.main.ScreenToWorldPoint(GameManager.positionTransforms[headTargets[i]].x - leftMostTarget <  Input.mousePosition.x && Input.mousePosition.x < GameManager.positionTransforms[headTargets[i+3]].x + rightMostTarget)){

    //                 }
    //         }
    //     }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            OnRightClick();
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (GameManager.activeCard)
            {
                for (int i = 0; i < 8; i++)
                {
                    if (GameManager.GetUnitFromPositionNumber(i).highlighted)
                    {
                        CardActivatedClick((Player)GameManager.currentTurnUnit, GameManager.GetUnitFromPositionNumber(i));
                        OnRemoveHighlight();
                        break;
                    }
                }
            }
            else
            {
                Vector3 mousePosition = Input.mousePosition;
                mousePosition.z = Mathf.Infinity;

                //RaycastHit2D hit = Physics2D.Raycast(Input.mousePosition ,Vector2.zero,Mathf.Infinity); //Hit object that contains gameobject Information
                ////RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, 1 << unitLayer);
                //Debug.DrawRay(mousePosition, mousePosition - Camera.main.ScreenToWorldPoint(mousePosition), Color.blue);
                // if (hit)
                // {
                //     Unit clickedUnit = hit.collider.gameObject.GetComponent<Unit>();
                //     if (clickedUnit.targeted)
                //     {
                //         CardActivatedClick((Player)GameManager.currentTurnUnit, clickedUnit);
                //         OnRightClick();
                //     }

                // }
            }
        }

    }
}