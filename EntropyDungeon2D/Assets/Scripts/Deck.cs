using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    public List<Card> cards = new List<Card>();
    public List<Card> availableCards = new List<Card>();
    public List<Card> allCards = new List<Card>();
    public List<Card> discard = new List<Card>();
    public Player owner;
    // Start is called before the first frame update

    public void Start()
    {
        foreach (Card card in cards)
        {
            allCards.Add(card);
        }
        foreach (Card card in availableCards)
        {
            allCards.Add(card);
        }
    }
    public void ConnectCards()
    {
        foreach (Card card in cards)
        {
            card.owningDeck = this;
        }
        foreach (Card card in availableCards)
        {
            card.owningDeck = this;
        }
    }
    public void HideCards()
    {
        foreach (Card card in availableCards)
        {
            card.HideCard();
        }
    }
    public void ShowCards()
    {
        foreach (Card card in availableCards)
        {
            card.ShowCard();
        }
    }
    public void setUsableAllCards()
    {
        foreach (Card card in availableCards)
        {
            card.setUsable();
        }
    }
    public void setUnusableAllCards()
    {
        foreach (Card card in allCards)
        {
            card.setUnusable();
        }
    }

}
