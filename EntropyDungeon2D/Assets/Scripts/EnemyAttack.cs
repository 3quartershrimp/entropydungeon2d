using System.Collections.Generic;
using System.Linq;

public class EnemyAttack
{
    public string name;
    public List<int> targetList = new List<int>();
    public int range;
    public int cooldownDuration;
    public int cooldownMax;
    public int attackAnimation;

    public HashSet<int> totalAvailableTargets = new HashSet<int>();
    public enum TargetLogicEnum
    {
        RANDOM, HIGHEST_HEALTH, LOWEST_HEALTH, MOST_UNITS
    }
    public TargetLogicEnum targetLogic;
    public EnemyAttack(string name, List<int> targetList, int range, int cooldownDuration, int cooldownMax, TargetLogicEnum targetLogic, int attackAnimation)
    {
        this.name = name;
        this.targetList = targetList;
        this.range = range;
        this.attackAnimation = attackAnimation;
        this.cooldownDuration = cooldownDuration;
        this.cooldownMax = cooldownMax;
        this.targetLogic = targetLogic;
        ResetTotalAvailableTargets();
    }
    public void ResetTotalAvailableTargets()
    {
        if (range == 2 || range == 3)
        {
            totalAvailableTargets.Add(targetList.Min() - 1);
            totalAvailableTargets.Add(targetList.Min());
            totalAvailableTargets.Add(targetList.Max());
            totalAvailableTargets.Add(targetList.Max() + 1);
        }
        else
        {
            foreach (int target in targetList)
            {
                totalAvailableTargets.Add(target);
            }
        }
    }
}
