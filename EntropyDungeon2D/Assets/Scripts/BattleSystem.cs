using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;
using System.Linq;

public enum BattleState
{
    START, PLAYERTURN, ENEMYTURN, WON, LOST
}
public class BattleSystem : MonoBehaviour
{
    public BattleState state;
    public List<GameObject> playerPrefabs;
    public List<GameObject> enemyPrefabs;

    public Transform unitPositionHolder;
    public GameObject positionPrefab;
    public GameObject playerUIPrefab;
    public GameObject enemyUIPrefab;
    public GameObject environmentPrefab;
    public List<Transform> playerBattleStation;
    public List<Transform> enemyBattleStation;
    public GameObject restartButton;

    public List<PlayerBattleHud> playerHuds;
    public List<BattleHud> enemyHuds;

    public int playerCount;
    public int enemyCount;
    public GameObject dialoguePrefab;
    public Text dialogueText;

    public List<Player> playerUnits = new List<Player>();
    public List<Enemy> enemyUnits = new List<Enemy>();
    public List<Unit> turnOrder = new List<Unit>();
    public int turnTotal;
    public int turnCount;
    public int lastPlayerUnitIndex;
    Player activePlayer;


    // Start is called before the first frame update
    void Start()
    {
        state = BattleState.START;
        EventManager.FocusBattleHud += RemoveBattleHuds;
        EventManager.CardActivatedClick += CardExecuted;
        EventManager.OnAttackAnnounce += AnnounceAttack;
        EventManager.OnMissAnnounce += AnnounceMiss;
        //Events used in persistants
        EventManager.OnUnitDeath += UnitDeath;
        EventManager.OnTurnStart += TurnStart;
        EventManager.OnTurnEnd += NextTurn;
        EventManager.OnUnitAttacked += DoNothing;
        EventManager.OnUnitDamaged += DoNothing;
        EventManager.OnRightClick += DoNothing;
        StartCoroutine(SetupBattle());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator SetupBattle()
    {
        //Set up dialogue text
        GameObject topText = Instantiate(dialoguePrefab, GameObject.Find("TopPanel").transform);
        dialogueText = topText.GetComponent<Text>();
        playerCount = playerPrefabs.Count;
        enemyCount = enemyPrefabs.Count;
        turnTotal = playerCount + enemyCount;
        for (int i = 0; i < playerCount; i++)
        {
            //Set up positions
            GameObject positionGo = Instantiate(positionPrefab, unitPositionHolder);
            positionGo.name = "Pos" + i;
            positionGo.transform.position = new Vector2(positionGo.transform.position.x + (-9.5f + (i * 2.5f)), positionGo.transform.position.y);
            GameManager.positionTransforms.Add(positionGo.transform.position);

            //Create players
            GameObject playerGo = Instantiate(playerPrefabs[i], positionGo.transform);
            GameManager.usedUnitPositions.Add(positionGo.name.Substring(3));
            playerUnits.Add(playerGo.GetComponent<Player>());
            playerUnits[i].index = i;
            playerUnits[i].fieldPosition = i;

            //Set up hud
            GameObject hudGo = Instantiate(playerUIPrefab, GameObject.Find("Canvas").transform);
            PlayerBattleHud playerUI = hudGo.GetComponent<PlayerBattleHud>();
            playerUnits[i].SetPlayerHud(playerUI);
            for (int j = 0; j < 4; j++)
            {
                playerUnits[i].DrawCard();
            }
        }
        for (int i = 0; i < enemyCount; i++)
        {
            //Set up positions
            GameObject positionGo = Instantiate(positionPrefab, unitPositionHolder);
            positionGo.name = "Pos" + (i + 4);
            positionGo.transform.position = new Vector2(positionGo.transform.position.x + (2 + (i * 2.5f)), positionGo.transform.position.y);
            GameManager.positionTransforms.Add(positionGo.transform.position);

            GameObject enemyGo = Instantiate(enemyPrefabs[i], positionGo.transform);
            GameManager.usedUnitPositions.Add(positionGo.name.Substring(3));
            enemyUnits.Add(enemyGo.GetComponent<Enemy>());
            enemyUnits[i].index = i;
            enemyUnits[i].fieldPosition = i + 4;
            //Set up hud
            GameObject hudGo = Instantiate(enemyUIPrefab, GameObject.Find("Canvas").transform);
            BattleHud unitUI = hudGo.GetComponent<BattleHud>();
            enemyUnits[i].SetHud(unitUI);
        }
        dialogueText.text = "A wild ";
        foreach (Unit enemy in enemyUnits)
        {
            dialogueText.text += enemy.unitName + ", ";
        }
        dialogueText.text += " appoaches!";

        SetUpTurnOrder(playerUnits, enemyUnits);
        RemoveBattleHuds();
        yield return new WaitForSeconds(2f);
        turnCount = 0;
        turnOrder[0].turnMarker.enabled = true;
        TurnStart(turnOrder[0]);

    }
    //Player turn functions
    void PlayerTurn(Player activePlayer)
    {
        lastPlayerUnitIndex = activePlayer.index;
        dialogueText.text = activePlayer.unitName + " Choose an action:";
        activePlayer.deck.setUsableAllCards();

        RemoveBattleHuds();
        activePlayer.FocusHud();
    }

    IEnumerator PlayerAttack(Player cardConsumer)
    {
        cardConsumer.deck.setUnusableAllCards();
        dialogueText.text = "The attack is successful!";
        //activePlayer.deck.hideCards();
        yield return new WaitForSeconds(2f);
        GameManager.DeactivateCard();
        bool win = true;
        foreach (Unit unit in enemyUnits)
        {
            if (!unit.isDead)
            {
                win = false;
            }
        }
        if (win)
        {
            state = BattleState.WON;
            EndBattle();
        }
        else
        {
            EventManager.OnTurnEnd(turnOrder[turnCount % turnTotal]);
        }


    }


    //Enemy turn functions
    IEnumerator EnemyTurn(Enemy activeEnemy)
    {

        yield return new WaitForSeconds(1f);
        if (!activeEnemy.isDead)
        {
            activeEnemy.ExecuteEnemyTurn();
        }
        yield return new WaitForSeconds(1f);
        bool loss = true;
        foreach (Unit player in playerUnits)
        {
            if (!player.isDead)
            {
                loss = false;
            }
        }
        if (loss)
        {
            state = BattleState.LOST;
            EndBattle();
        }
        else
        {
            EventManager.OnTurnEnd(activeEnemy);
        }
    }


    //UI Adjustments
    void SwapPlayerHud()
    {
        //turnOrder[lastPlayerUnitIndex].unitUI.transform.position = new Vector2(1000, 0);
        RemoveBattleHuds();
        turnOrder[turnCount % turnTotal].FocusHud();

    }
    void RemoveBattleHuds()
    {
        if (!GameManager.activeCard)
        {
            foreach (Unit unit in turnOrder)
            {
                unit.selectedMarker.enabled = false;
            }
            foreach (Player player in playerUnits)
            {
                player.deck.HideCards();
                player.playerUI.transform.position = new Vector2(1000, 0);
            }
            foreach (Enemy enemy in enemyUnits)
            {
                enemy.unitUI.transform.position = new Vector2(1000, 0);
            }
        }
    }


    //Game state managers
    void NextTurn(Unit previousTurnUnit)
    {

        turnCount++;
        if (turnCount % turnTotal == 0 && turnCount > 0)
        {
            SetUpTurnOrder(playerUnits, enemyUnits);
        }
        Unit turnPlayer = turnOrder[turnCount % turnTotal];
        if (turnPlayer.isDead)
        {
            NextTurn(turnPlayer);
        }
        else
        {
            turnPlayer.turnMarker.enabled = true;
            if (turnPlayer.type == "Player")
            {
                state = BattleState.PLAYERTURN;

            }
            else
            {
                state = BattleState.ENEMYTURN;
            }
            //Activates TurnStart
            EventManager.OnTurnStart(turnPlayer);
        }
    }
    void SetUpTurnOrder(List<Player> playerUnits, List<Enemy> enemyUnits)
    {
        GameManager.SetRandomSeed();
        List<Unit> allUnits = new List<Unit>();
        Dictionary<Unit, int> speedRollDictionary = new Dictionary<Unit, int>();
        foreach (Unit player in playerUnits)
        {
            allUnits.Add(player);
            speedRollDictionary.Add(player, player.speed + Random.Range(0, 8));
        }
        foreach (Unit enemy in enemyUnits)
        {
            allUnits.Add(enemy);
            speedRollDictionary.Add(enemy, enemy.speed + Random.Range(0, 8));
        }
        foreach (KeyValuePair<Unit, int> unit in speedRollDictionary.OrderByDescending(key => key.Value))
        {
            turnOrder.Add(unit.Key);
        }
    }
    void EndBattle()
    {
        if (state == BattleState.WON)
        {
            dialogueText.text = "You won the battle!";
        }
        else if (state == BattleState.LOST)
        {
            dialogueText.text = "You were defeated.";
        }
        restartButton.SetActive(true);
    }
    public void RestartGame()
    {
        EventManager.CardActivatedClick = null;
        EventManager.DebugTestEventManager = null;
        EventManager.DisableCards = null;
        EventManager.FocusBattleHud = null;
        EventManager.OnAttackAnnounce = null;
        EventManager.OnMissAnnounce = null;
        EventManager.OnRightClick = null;
        EventManager.OnTurnEnd = null;
        EventManager.OnTurnStart = null;
        EventManager.OnUnitAttacked = null;
        EventManager.OnUnitDamaged = null;
        EventManager.OnUnitDeath = null;
        EventManager.ShowAoETargets = null;
        Scene scene = SceneManager.GetActiveScene();

        SceneManager.LoadScene(scene.name);
    }


    //Triggered Game events
    public void CardExecuted(Player cardConsumer, Unit target)
    {

        StartCoroutine(PlayerAttack(cardConsumer));

    }
    void UnitDeath(Unit deadUnit)
    {
        //yield return new WaitForSeconds(1);
        dialogueText.text = deadUnit.unitName + " has fallen!";
    }
    void AnnounceAttack(string attacker, string attack)
    {
        dialogueText.text = attacker + " used " + attack + "!";
    }
    void AnnounceMiss(string attacker, string attack)
    {
        dialogueText.text = attacker + " used " + attack + " and missed!";
    }
    public void TurnStart(Unit TurnUnit)
    {
        GameManager.currentTurnUnit = TurnUnit;
        if (TurnUnit.type == "Player")
        {
            PlayerTurn((Player)TurnUnit);
        }
        else
        {
            StartCoroutine(EnemyTurn((Enemy)TurnUnit));
        }
    }
    public void DoNothing(Unit doNothing)
    {

    }
    public void DoNothing() { }


}
//Old way to do things for set up

// for (int i = 0; i < playerCount; i++)
//         {

//             GameObject playerGo = Instantiate(playerPrefabs[i], playerBattleStation[i]);
//             GameManager.usedUnitPositions.Add(playerBattleStation[i].name.Substring(3));
//             playerUnits.Add(playerGo.GetComponent<Player>());
//             playerUnits[i].index = i;
//             playerUnits[i].fieldPosition = i;
//             playerUnits[i].SetHud(playerHuds[i]);
//             for (int j = 0; j < 4; j++)
//             {
//                 playerUnits[i].DrawCard();
//             }
//         }
//         for (int i = 0; i < enemyCount; i++)
//         {
//             GameObject enemyGo = Instantiate(enemyPrefabs[i], enemyBattleStation[i]);
//             GameManager.usedUnitPositions.Add(enemyBattleStation[i].name.Substring(3));
//             enemyUnits.Add(enemyGo.GetComponent<Enemy>());
//             enemyUnits[i].index = i;
//             enemyUnits[i].fieldPosition = i + 4;
//             enemyUnits[i].SetHud(enemyHuds[i]);
//         }