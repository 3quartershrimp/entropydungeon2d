using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireMagePlayer : Player
{
    public void Awake()
    {
        unitName = "Fire Mage";
        maxHP = 13;
        currentHP = maxHP;
        speed = 10;
    }


    public override void UnitAttackAnimation(int animationNumber)
    {

    }
}
