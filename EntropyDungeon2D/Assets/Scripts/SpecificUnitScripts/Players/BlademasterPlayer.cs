using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlademasterPlayer : Player
{
    public void Awake()
    {
        unitName = "Blade Master";
        maxHP = 18;
        currentHP = maxHP;
        speed = 7;
    }


    public override void UnitAttackAnimation(int animationNumber)
    {

    }
}
