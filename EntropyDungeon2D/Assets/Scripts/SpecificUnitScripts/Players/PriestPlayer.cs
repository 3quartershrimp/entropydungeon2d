using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriestPlayer : Player
{
    public void Awake()
    {
        unitName = "Priest";
        maxHP = 12;
        currentHP = maxHP;
        speed = 10;
    }


    public override void UnitAttackAnimation(int animationNumber)
    {

    }
}
