using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarbarianPlayer : Player
{
    public void Awake()
    {
        unitName = "Barbarian";
        maxHP = 25;
        currentHP = maxHP;
        speed = 4;
    }


    public override void UnitAttackAnimation(int animationNumber)
    {

    }
}
