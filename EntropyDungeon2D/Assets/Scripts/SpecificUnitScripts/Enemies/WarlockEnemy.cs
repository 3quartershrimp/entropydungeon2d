using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarlockEnemy : Enemy
{
    public GameObject damagePrefab;
    public void Awake()
    {
        unitName = "Warlock";
        maxHP = 8;
        currentHP = maxHP;
        speed = 0;
        dodge = 2;
        attackOptions.Add(1, new EnemyAttack("Fel Blast", new List<int>() { 0, 1, 2, 3 }, 1, 0, 2, EnemyAttack.TargetLogicEnum.RANDOM, 1));
        attackOptions.Add(2, new EnemyAttack("Eldrich Abomination", new List<int>() { 1, 2 }, 2, 0, 3, EnemyAttack.TargetLogicEnum.HIGHEST_HEALTH, 0));
        attackOptions.Add(3, new EnemyAttack("Drain Soul", new List<int>() { 0, 1 }, 1, 5, 3, EnemyAttack.TargetLogicEnum.LOWEST_HEALTH, 1));
        attackOptions.Add(4, new EnemyAttack("Evil Fetish", new List<int>() { 4, 5, 6, 7 }, 4, 0, 0, EnemyAttack.TargetLogicEnum.LOWEST_HEALTH, 1));
    }
    public override void ExecuteAttack(EnemyAttack attack, int target, int attackRoll)
    {
        base.ExecuteAttack(attack, target, attackRoll);
        if (GameManager.usedUnitPositions.Contains(target.ToString()))
        {
            Unit attackTarget = GameManager.GetUnitFromPositionNumber(target);
            if (attack.name == "Fel Blast")
            {
                attackTarget.TakeDamage(3f * damageMultiplier);
            }
            else if (attack.name == "Eldrich Abomination")
            {
                attackTarget.TakeDamage(1f * damageMultiplier);
            }
            else if (attack.name == "Drain Soul")
            {
                attackTarget.TakeDamage(2f * damageMultiplier);
                Heal(2);
            }
            else if (attack.name == "Evil Fetish")
            {
                GameObject damageGo = Instantiate(damagePrefab, new Vector3(0, 0, 0), Quaternion.identity);
                Damage damagePersistent = damageGo.GetComponent<Damage>();
                damagePersistent.ActivatePersistent(GameManager.GetUnitFromPositionNumber(target), 2, 5);
            }
        }

    }
    public override void UnitAttackAnimation(int animationNumber)
    {
        m_animator.SetTrigger("Spellcast");
    }
}
