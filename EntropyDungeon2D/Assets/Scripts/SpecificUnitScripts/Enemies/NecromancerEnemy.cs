using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NecromancerEnemy : Enemy
{
    public void Awake()
    {
        unitName = "Necromancer";
        maxHP = 15;
        currentHP = maxHP;
        speed = 3;
        armor = 2;

        attackOptions.Add(3, new EnemyAttack("Plague Bomb", new List<int>() { 1, 2 }, 2, 0, 2, EnemyAttack.TargetLogicEnum.MOST_UNITS, 0));
        attackOptions.Add(2, new EnemyAttack("Reaper Sycthe", new List<int>() { 2, 3 }, 1, 0, 2, EnemyAttack.TargetLogicEnum.LOWEST_HEALTH, 0));
        attackOptions.Add(1, new EnemyAttack("Horrify", new List<int>() { 0, 1, 2, 3 }, 1, 0, 4, EnemyAttack.TargetLogicEnum.RANDOM, 0));
    }
    public override void ExecuteAttack(EnemyAttack attack, int target, int attackRoll)
    {
        base.ExecuteAttack(attack, target, attackRoll);
        if (GameManager.usedUnitPositions.Contains(target.ToString()))
        {
            Unit attackTarget = GameManager.GetUnitFromPositionNumber(target);
            if (attack.name == "Plague Bomb")
            {
                attackTarget.TakeDamage(2f * damageMultiplier);
            }
            else if (attack.name == "Reaper Sycthe")
            {
                attackTarget.TakeDamage(5f * damageMultiplier);
            }
            else if (attack.name == "Horrify")
            {
                attackTarget.TakeDamage(5f * damageMultiplier);
            }
        }

    }
    public override void UnitAttackAnimation(int animationNumber)
    {

    }
}
