using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RogueEnemy : Enemy
{
    public void Awake()
    {
        unitName = "Rogue";
        maxHP = 10;
        currentHP = maxHP;
        speed = 10;
        dodge = 3;


        attackOptions.Add(3, new EnemyAttack("Eviscerate", new List<int>() { 2, 3 }, 1, 0, 3, EnemyAttack.TargetLogicEnum.LOWEST_HEALTH, 0));
        attackOptions.Add(2, new EnemyAttack("Smoke Bomb", new List<int>() { fieldPosition }, 2, 0, 3, EnemyAttack.TargetLogicEnum.MOST_UNITS, 0));
        attackOptions.Add(1, new EnemyAttack("Poison Kunai", new List<int>() { 0, 1, 2, 3 }, 1, 0, 3, EnemyAttack.TargetLogicEnum.HIGHEST_HEALTH, 0));

    }
    public override void ExecuteAttack(EnemyAttack attack, int target, int attackRoll)
    {
        base.ExecuteAttack(attack, target, attackRoll);
        if (GameManager.usedUnitPositions.Contains(target.ToString()))
        {
            Unit attackTarget = GameManager.GetUnitFromPositionNumber(target);
            if (attack.name == "Eviscerate")
            {
                attackTarget.TakeDamage(3f * damageMultiplier);
            }
            else if (attack.name == "Smoke Bomb")
            {
                attackTarget.TakeDamage(1f * damageMultiplier);
            }
            else if (attack.name == "Poison Kunai")
            {
                attackTarget.TakeDamage(2f * damageMultiplier);
            }
        }

    }
    public override void UpdateAttackVariables(Unit turnStartUnit)
    {
        base.UpdateAttackVariables(turnStartUnit);
        if (turnStartUnit == this)
        {
            attackOptions[2].targetList = new List<int>() { fieldPosition };
            attackOptions[2].ResetTotalAvailableTargets();
            Debug.Log(attackOptions[2].name + " is going to target " + attackOptions[2].targetList[0]);
        }
    }

    public override void UnitAttackAnimation(int animationNumber)
    {

    }
}
