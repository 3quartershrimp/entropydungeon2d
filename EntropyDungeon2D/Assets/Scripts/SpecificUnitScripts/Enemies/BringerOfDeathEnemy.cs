using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BringerOfDeathEnemy : Enemy
{
    public GameObject damagePrefab;
    public void Awake()
    {
        unitName = "Bringer of Death";
        maxHP = 20;
        currentHP = maxHP;
        speed = -3;
        armor = 2;
        attackOptions.Add(3, new EnemyAttack("Deafening Screech", new List<int>() { 0, 1, 2, 3 }, 4, 0, 2, EnemyAttack.TargetLogicEnum.RANDOM, 0));
        attackOptions.Add(2, new EnemyAttack("Lick", new List<int>() { 2, 3 }, 1, 0, 2, EnemyAttack.TargetLogicEnum.RANDOM, 0));
        attackOptions.Add(1, new EnemyAttack("DOOM!!!", new List<int>() { 0, 1, 2, 3 }, 1, 8, 8, EnemyAttack.TargetLogicEnum.LOWEST_HEALTH, 1));
        attackOptions.Add(4, new EnemyAttack("Unending Hatred", new List<int>() { fieldPosition }, 1, 0, 0, EnemyAttack.TargetLogicEnum.RANDOM, 0));
    }
    public override void Start()
    {
        base.Start();
        SpriteRenderer sprite = GetComponentInChildren(typeof(SpriteRenderer)) as SpriteRenderer;
        sprite.flipX = false;
    }

    public override void ExecuteAttack(EnemyAttack attack, int target, int attackRoll)
    {
        base.ExecuteAttack(attack, target, attackRoll);
        if (GameManager.usedUnitPositions.Contains(target.ToString()))
        {
            Unit attackTarget = GameManager.GetUnitFromPositionNumber(target);
            if (attack.name == "Deafening Screech")
            {
                attackTarget.TakeDamage(2f * damageMultiplier);
            }
            else if (attack.name == "Lick")
            {
                attackTarget.TakeDamage(2f * damageMultiplier);
            }
            else if (attack.name == "DOOM!!!")
            {
                attackTarget.TakeDamage(2f * damageMultiplier);
            }
            else if (attack.name == "Unending Hatred")
            {
                GameObject damageGo = Instantiate(damagePrefab, new Vector3(0, 0, 0), Quaternion.identity);
                Damage damagePersistent = damageGo.GetComponent<Damage>();
                damagePersistent.ActivatePersistent(GameManager.GetUnitFromPositionNumber(target), 8, 5);
            }
        }

    }
    public override void UpdateAttackVariables(Unit turnStartUnit)
    {
        base.UpdateAttackVariables(turnStartUnit);
        if (turnStartUnit == this)
        {
            attackOptions[4].targetList = new List<int>() { fieldPosition };
            attackOptions[4].ResetTotalAvailableTargets();
        }
    }
    public override void UnitAttackAnimation(int animationNumber)
    {
        m_animator.SetTrigger("Cast");
    }
}
