using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shank : Card
{
    void Start()
    {
        cardName = "Shank";
        headTargets = new List<int>() { 4 , 5, 6, 7 };
        damage = 1;
    }
    public override void CardEffects(Unit Target, Player caster)
    {
        Target.TakeDamage(damage * caster.damageMultiplier);
    }
}