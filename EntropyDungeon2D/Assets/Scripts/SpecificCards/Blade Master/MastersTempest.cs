using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MastersTempest : Card
{
    void Start()
    {
        cardName = "Master's Tempest";
        headTargets = new List<int>() { 4, 5 };
    }
    public override void CardEffects(Unit Target, Player caster)
    {
        damage = caster.persistentEffects.Count * 2;
        Target.TakeDamage(damage * caster.damageMultiplier);
    }
}