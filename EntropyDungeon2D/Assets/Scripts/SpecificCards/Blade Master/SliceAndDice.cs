using System.Collections.Generic;

public class SliceAndDice : Card
{
    // Start is called before the first frame update
    void Start()
    {
        cardName = "Slice and Dice";
        range = 2;
        headTargets = new List<int>() { 4 };
        damage = 2;
    }

    public override void CardEffects(Unit Target, Player caster)
    {
        Target.TakeDamage(damage * caster.damageMultiplier);
    }
}
