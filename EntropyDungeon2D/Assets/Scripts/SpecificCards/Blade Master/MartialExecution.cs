using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This is the derived class whis is
//also know as the Child class.
public class MartialExecution : Card
{
    void Start()
    {
        cardName = "Martial Execution";
        headTargets = new List<int>() {  4  };
        damage = 5;
    }
    public override void CardEffects(Unit Target, Player caster)
    {
        Target.TakeDamage(damage * caster.damageMultiplier);
    }
}