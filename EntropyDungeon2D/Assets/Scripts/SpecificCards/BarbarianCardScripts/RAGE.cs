using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RAGE : Card
{
    public GameObject ragePrefab;
    // Start is called before the first frame update
    void Start()
    {
        cardName = "Rage";
        headTargets = new List<int>() { owningDeck.owner.fieldPosition };
    }

    public override void CardEffects(Unit Target, Player caster)
    {
        GameObject rageGo = Instantiate(ragePrefab, new Vector3(0, 0, 0), Quaternion.identity);
        RagePersistent ragePersistent = rageGo.GetComponent<RagePersistent>();
        ragePersistent.ActivatePersistent(caster, 3);
    }
}
