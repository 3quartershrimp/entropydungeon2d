using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigSwig : Card
{
    public GameObject dodgePrefab;
    int dodgeIncrease;
    int duration;
    // Start is called before the first frame update
    void Start()
    {
        cardName = "Big Swig";
        dodgeIncrease = 2;
        duration = 3;
        headTargets = new List<int>() { owningDeck.owner.fieldPosition };
    }

    public override void CardEffects(Unit Target, Player caster)
    {
        GameObject dodgeGo = Instantiate(dodgePrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Dodge dodgePersistent = dodgeGo.GetComponent<Dodge>();
        dodgePersistent.ActivatePersistent(caster, duration, dodgeIncrease);
    }
}
