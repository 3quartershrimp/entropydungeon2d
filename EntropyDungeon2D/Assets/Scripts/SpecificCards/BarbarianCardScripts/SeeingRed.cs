using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeingRed : Card
{
    public int healAmount;
    // Start is called before the first frame update
    void Start()
    {
        cardName = "Seeing Red";
        healAmount = 5;
        headTargets = new List<int>() { owningDeck.owner.fieldPosition };
    }

    public override void CardEffects(Unit Target, Player caster)
    {
        caster.Heal(healAmount);
    }
}
