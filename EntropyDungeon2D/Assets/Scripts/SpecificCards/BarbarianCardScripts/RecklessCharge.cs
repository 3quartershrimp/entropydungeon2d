using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This is the derived class whis is
//also know as the Child class.
public class RecklessCharge : Card
{
    void Start()
    {
        cardName = "RecklessCharge";
        headTargets = new List<int>() { 4, 5, 6, 7 };
        damage = 4;
    }
    public override void CardEffects(Unit Target, Player caster)
    {
        Target.TakeDamage(damage * caster.damageMultiplier);
        caster.TakeDamage((damage / 2) * caster.damageMultiplier);
    }
}