using System.Collections.Generic;
using UnityEngine;

public class Howl : Card
{
    int dodgeDecrease;
    public GameObject dodgePrefab;
    // Start is called before the first frame update
    void Start()
    {
        precision = 10;
        dodgeDecrease = -2;
        cardName = "Howl";
        range = 4;
        headTargets = new List<int>() { 4 };
    }

    public override void CardEffects(Unit target, Player caster)
    {
        GameObject dodgeGo = Instantiate(dodgePrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Dodge dodgePersistent = dodgeGo.GetComponent<Dodge>();
        dodgePersistent.ActivatePersistent(target, 3, dodgeDecrease);
    }
}
