using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dismember : Card
{
    int bleedDmg;
    public GameObject bleedPrefab;
    // Start is called before the first frame update
    void Start()
    {
        damage = 2;
        bleedDmg = 1;
        cardName = "Dismember";
        headTargets = new List<int>() { 4, 5, 6, 7 };
    }

    public override void CardEffects(Unit target, Player caster)
    {
        GameObject bleedGo = Instantiate(bleedPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Bleed bleedPersistent = bleedGo.GetComponent<Bleed>();
        bleedPersistent.ActivatePersistent(target, 3, bleedDmg);
        target.TakeDamage(damage * caster.damageMultiplier);
    }
}
