using System.Collections.Generic;

public class REND : Card
{
    // Start is called before the first frame update
    void Start()
    {
        cardName = "Rend";
        range = 2;
        headTargets = new List<int>() { 4, 6 };
        damage = 4;
    }

    public override void CardEffects(Unit Target, Player caster)
    {
        Target.TakeDamage(damage * caster.damageMultiplier);
    }
}
