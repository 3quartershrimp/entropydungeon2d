using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToughSkin : Card
{
    public GameObject armorPrefab;
    int armorIncrease;
    int duration;
    // Start is called before the first frame update
    void Start()
    {
        cardName = "Tough Skin";
        duration = 3;
        armorIncrease = 2;
        headTargets = new List<int>() { owningDeck.owner.fieldPosition };
    }

    public override void CardEffects(Unit Target, Player caster)
    {
        GameObject armorGo = Instantiate(armorPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Armor armorPersistent = armorGo.GetComponent<Armor>();
        armorPersistent.ActivatePersistent(caster, duration, armorIncrease);
    }
}
