using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This is the derived class whis is
//also know as the Child class.
public class WarlordsTrophy : Card
{
    int accuracyDecrease;
    int duration;
    public GameObject accuracyPrefab;
    void Start()
    {
        cardName = "Warlord's Trophy";
        accuracyDecrease = 2;
        duration = 3;
        headTargets = new List<int>() { 4, 5 };
        damage = 3;
    }
    public override void CardEffects(Unit Target, Player caster)
    {
        EventManager.OnUnitDeath += TriggerFear;
        Target.TakeDamage(damage * caster.damageMultiplier);
        EventManager.OnUnitDeath -= TriggerFear;
    }
    public void TriggerFear(Unit target)
    {
        for (int i = 4; i <= 7; i++)
        {
            Unit missTarget = GameManager.GetUnitFromPositionNumber(i);
            if (!missTarget.isDead)
            {
                GameObject accuracyGo = Instantiate(accuracyPrefab, new Vector3(0, 0, 0), Quaternion.identity);
                Accuracy accuracyPersistent = accuracyGo.GetComponent<Accuracy>();
                accuracyPersistent.ActivatePersistent(missTarget, duration, accuracyDecrease);
            }
        }
    }
}