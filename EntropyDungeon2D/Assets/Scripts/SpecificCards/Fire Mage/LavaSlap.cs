using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This is the derived class whis is
//also know as the Child class.
public class LavaSlap : Card
{
    void Start()
    {
        cardName = "Lava Slap";
        headTargets = new List<int>() {  5,  6  };
        damage = 3;
    }
    public override void CardEffects(Unit Target, Player caster)
    {
        Target.TakeDamage(damage * caster.damageMultiplier);
    }
}