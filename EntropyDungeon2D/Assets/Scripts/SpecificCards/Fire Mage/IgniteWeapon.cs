using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgniteWeapon : Card
{
    public GameObject damagePrefab;
    int duration;
    int damageIncrease;
    // Start is called before the first frame update
    void Start()
    {
        cardName = "Ignite Weapon";
        damageIncrease = 3;
        duration = 3;
        headTargets = new List<int>() { 2, 3 };
    }

    public override void CardEffects(Unit Target, Player caster)
    {
        GameObject damageGo = Instantiate(damagePrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Damage damagePersistent = damageGo.GetComponent<Damage>();
        damagePersistent.ActivatePersistent(Target, duration, damageIncrease);
    }
}
