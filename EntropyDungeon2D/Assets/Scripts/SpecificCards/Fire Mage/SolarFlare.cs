using System.Collections.Generic;
using UnityEngine;

public class SolarFlare : Card
{
    int armorDecrease;
    public GameObject armorPrefab;
    // Start is called before the first frame update
    void Start()
    {
        armorDecrease = -2;
        cardName = "Solar Flare";
        range = 3;
        headTargets = new List<int>() { 5 };
    }

    public override void CardEffects(Unit target, Player caster)
    {
        GameObject armorGo = Instantiate(armorPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Armor armorPersistent = armorGo.GetComponent<Armor>();
        armorPersistent.ActivatePersistent(target, 3, armorDecrease);
    }
}
