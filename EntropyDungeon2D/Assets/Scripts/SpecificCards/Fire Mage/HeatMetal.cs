using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This is the derived class whis is
//also know as the Child class.
public class HeatMetal : Card
{
    int accuracyDecrease;
    public GameObject accuracyPrefab;
    void Start()
    {
        cardName = "Heat Metal";
        accuracyDecrease = -2;
        headTargets = new List<int>() { 4, 5, 6, 7 };
    }
    public override void CardEffects(Unit Target, Player caster)
    {

        GameObject accuracyGo = Instantiate(accuracyPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Accuracy accuracyPersistent = accuracyGo.GetComponent<Accuracy>();
        accuracyPersistent.ActivatePersistent(Target, 3, accuracyDecrease);

    }

}