using System.Collections.Generic;

public class FireStorm : Card
{
    // Start is called before the first frame update
    void Start()
    {
        cardName = "FireStrom";
        range = 4;
        headTargets = new List<int>() { 4 };
        damage = 3;
    }

    public override void CardEffects(Unit Target, Player caster)
    {
        Target.TakeDamage(damage * caster.damageMultiplier);
    }
}
