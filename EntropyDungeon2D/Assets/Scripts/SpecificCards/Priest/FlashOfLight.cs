using System.Collections.Generic;
using UnityEngine;

public class FlashOfLight : Card
{
    int accuracyDecrease;
    int duration;
    public GameObject accuracyPrefab;
    // Start is called before the first frame update
    void Start()
    {
        accuracyDecrease = -2;
        duration = 3;
        cardName = "Flash of Light";
        range = 4;
        headTargets = new List<int>() { 4 };
    }

    public override void CardEffects(Unit target, Player caster)
    {
        GameObject accuracyGo = Instantiate(accuracyPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Accuracy accuracyPersistent = accuracyGo.GetComponent<Accuracy>();
        accuracyPersistent.ActivatePersistent(target, duration, accuracyDecrease);

    }
}
