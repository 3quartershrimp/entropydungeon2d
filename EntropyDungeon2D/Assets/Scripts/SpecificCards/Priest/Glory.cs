using System.Collections.Generic;
using UnityEngine;

public class Glory : Card
{
    int dodgeIncrease;
    int armorIncrease;
    int damageIncrease;
    int duration;
    public GameObject dodgePrefab;
    public GameObject armorPrefab;
    public GameObject damagePrefab;
    // Start is called before the first frame update
    void Start()
    {
        dodgeIncrease = 1;
        armorIncrease = 1;
        damageIncrease = 1;
        duration = 2;
        cardName = "Glory";
        range = 4;
        headTargets = new List<int>() { 0 };
    }

    public override void CardEffects(Unit target, Player caster)
    {
        GameObject dodgeGo = Instantiate(dodgePrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Dodge dodgePersistent = dodgeGo.GetComponent<Dodge>();
        dodgePersistent.ActivatePersistent(target, duration, dodgeIncrease);
        GameObject armorGo = Instantiate(armorPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Armor armorPersistent = armorGo.GetComponent<Armor>();
        armorPersistent.ActivatePersistent(target, duration, armorIncrease);
        GameObject damageGo = Instantiate(damagePrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Damage damagePersistent = damageGo.GetComponent<Damage>();
        damagePersistent.ActivatePersistent(target, duration, damageIncrease);
    }
}
