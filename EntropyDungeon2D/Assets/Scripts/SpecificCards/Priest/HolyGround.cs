using System.Collections.Generic;

public class HolyGround : Card
{
    public int healAmount;
    // Start is called before the first frame update
    void Start()
    {
        cardName = "Holy Ground";
        healAmount = 2;
        range = 3;
        headTargets = new List<int>() { 1 };
    }

    public override void CardEffects(Unit Target, Player caster)
    {
        Target.Heal(healAmount);
    }
}
