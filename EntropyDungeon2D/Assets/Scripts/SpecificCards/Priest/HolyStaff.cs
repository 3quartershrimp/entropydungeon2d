using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This is the derived class whis is
//also know as the Child class.
public class HolyStaff : Card
{
    void Start()
    {
        cardName = "Holy Ftaff";
        headTargets = new List<int>() { 4, 5, 6, 7};
        damage = 2;
    }
    public override void CardEffects(Unit Target, Player caster)
    {
        Target.TakeDamage(damage * caster.damageMultiplier);
    }
}