using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : Card
{
    public int healAmount;
    // Start is called before the first frame update
    void Start()
    {
        cardName = "Heal";
        healAmount = 4;
        headTargets = new List<int>() {  0, 1, 2,3};
    }

    public override void CardEffects(Unit Target, Player caster)
    {
        Target.Heal(healAmount);
    }
}
