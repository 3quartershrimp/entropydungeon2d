using UnityEngine;
using System.Collections.Generic;

public class Player : Unit
{

    public Deck deck;
    public List<Card> discardPile = new List<Card>();
    public PlayerBattleHud playerUI;
    public override void Start()
    {
        base.Start();
        type = "Player";
        deck.owner = this;
        deck.ConnectCards();
        discardPile = deck.discard;

    }
    public Player()
    {

    }
    public override void Update()
    {
        base.Update();

    }
    public void DrawCard()
    {
        if (deck.cards.Count <= 0)
        {
            ShuffleDeck();
        }
        if (deck.cards.Count >= 1)
        {
            Card randCard = deck.cards[Random.Range(0, deck.cards.Count - 1)];

            for (int i = 0; i < playerUI.availableCardSlots.Length; i++)
            {
                if (playerUI.availableCardSlots[i])
                {
                    //randCard.gameObject.SetActive(true);
                    randCard.handIndex = i;

                    randCard.transform.position = playerUI.cardSlots[i].position;
                    randCard.transform.position = new Vector3(randCard.transform.position.x, randCard.transform.position.y, 1);
                    randCard.hasBeenPlayed = false;

                    playerUI.availableCardSlots[i] = false;
                    deck.cards.Remove(randCard);
                    deck.availableCards.Add(randCard);
                    return;
                }
            }
        }
    }
    public void DeactivateDeck()
    {
        deck.setUnusableAllCards();
    }
    public override void PlayAttackAnimation(int animationNumber, string cardName)
    {
        base.PlayAttackAnimation(animationNumber, cardName);
    }
    public void ShuffleDeck()
    {
        if (discardPile.Count >= 1)
        {
            foreach (Card card in discardPile)
            {
                deck.cards.Add(card);
            }
            discardPile.Clear();
        }
    }
    public override void FocusHud()
    {

        if (!GameManager.activeCard)
        {
            playerUI.transform.position = new Vector2(0, 0);
            selectedMarker.enabled = true;
        }

        if (!GameManager.activeCard)
        {
            deck.ShowCards();
        }
    }
    public void SetPlayerHud(PlayerBattleHud ui)
    {
        playerUI = ui;
        ui.attatchedUnit = this;
        setUI = true;

        ui.SetupPlayerHudVariables();

        Transform selected = gameObject.transform.Find(type + "Markers/Selected");
        selectedMarker = selected.GetComponent<Renderer>();
        Transform targeted = gameObject.transform.Find(type + "Markers/Targeted");
        targetedMarker = targeted.GetComponent<Renderer>();
        Transform turn = gameObject.transform.Find(type + "Markers/Turn");
        turnMarker = turn.GetComponent<Renderer>();

        selectedMarker.enabled = false;
        targetedMarker.enabled = false;
        turnMarker.enabled = false;
    }

}