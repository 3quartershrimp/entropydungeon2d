using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBattleHud : BattleHud
{
    //Player only
    public Text deckSizeText;
    public Text discardSizeText;
    public Transform[] cardSlots;
    public bool[] availableCardSlots;
    public Player attatchedPlayer;

    public Button moveButton;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public override void Update()
    {
        if (attatchedUnit != null)
        {
            base.Update();

        }
    }
    public override void UpdateText()
    {
        if (attatchedUnit != null)
        {
            base.UpdateText();
            //deckSizeText.text = attatchedPlayer.deck.cards.Count.ToString();
            //discardSizeText.text = attatchedPlayer.discardPile.Count.ToString();
        }
    }
    public void SetupPlayerHudVariables()
    {
        Player attatchedPlayer = attatchedUnit.GetComponent<Player>();
        moveButton.onClick.AddListener(attatchedPlayer.MoveStateInitial);
    }
}
