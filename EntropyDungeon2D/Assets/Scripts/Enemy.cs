using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Enemy : Unit
{
    public Dictionary<int, EnemyAttack> attackOptions = new Dictionary<int, EnemyAttack>();
    public bool attackHit;

    // public Dictionary<int, HashSet<int>> availableTargets = new Dictionary<int, HashSet<int>>();
    public Enemy()
    {

    }
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        type = "Enemy";
        sprite = GetComponentInChildren(typeof(SpriteRenderer)) as SpriteRenderer;
        EventManager.OnTurnStart += UpdateAttackVariables;
        sprite.flipX = true;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }
    public EnemyAttack ChooseAttack()
    {
        for (int i = 1; i <= attackOptions.Count; i++)
        {
            if (attackOptions[i].cooldownDuration <= 0 && attackOptions[i].totalAvailableTargets.Count > 0)
            {
                attackOptions[i].cooldownDuration = attackOptions[i].cooldownMax;
                return attackOptions[i];
            }
        }
        return null;
    }
    public int ChoosePrimaryTarget(EnemyAttack attack)
    {
        if (attack == null)
        {
            Debug.Log("null attack");
        }
        EnemyAttack.TargetLogicEnum targetLogic = attack.targetLogic;
        if (targetLogic == EnemyAttack.TargetLogicEnum.RANDOM)
        {
            GameManager.SetRandomSeed();
            return attack.totalAvailableTargets.ElementAt(Random.Range(0, attack.totalAvailableTargets.Count));
        }
        else if (targetLogic == EnemyAttack.TargetLogicEnum.HIGHEST_HEALTH)
        {
            float highestHealth = -1f;
            int highestHealthUnit = -1;
            foreach (int target in attack.totalAvailableTargets)
            {
                Unit currUnit = GameManager.GetUnitFromPositionNumber(target);
                if (currUnit.currentHP > highestHealth)
                {
                    highestHealth = currUnit.currentHP;
                    highestHealthUnit = target;
                }
            }
            return highestHealthUnit;
        }
        else if (targetLogic == EnemyAttack.TargetLogicEnum.LOWEST_HEALTH)
        {
            float lowestHealth = 1000000f;
            int lowestHealthUnit = -1;
            foreach (int target in attack.totalAvailableTargets)
            {
                Unit currUnit = GameManager.GetUnitFromPositionNumber(target);
                if (currUnit.currentHP < lowestHealth)
                {
                    lowestHealth = currUnit.currentHP;
                    lowestHealthUnit = target;
                }
            }
            return lowestHealthUnit;
        }
        else if (targetLogic == EnemyAttack.TargetLogicEnum.MOST_UNITS)
        {
            if (attack.totalAvailableTargets.Count == 1)
            {
                if (attack.totalAvailableTargets.ElementAt(0) >= attack.targetList.Min())
                {
                    return attack.targetList.Min();
                }
                else
                {
                    return attack.targetList.Max();
                }
            }
            else if (!GameManager.usedUnitPositions.Contains(attack.totalAvailableTargets.Max().ToString()))
            {
                return attack.targetList.Min();
            }
            else if (!GameManager.usedUnitPositions.Contains(attack.totalAvailableTargets.Min().ToString()))
            {
                return attack.targetList.Max();
            }
            else
            {
                GameManager.SetRandomSeed();
                return attack.totalAvailableTargets.ElementAt(Random.Range(1, attack.totalAvailableTargets.Count));
            }
        }
        else
        {
            return -1;
        }
    }
    public void ActivateAttack(EnemyAttack attack, int primaryTarget, int attackRoll)
    {
        if (attack.range == 4)
        {
            foreach (int target in attack.totalAvailableTargets)
            {
                ExecuteAttack(attack, target, attackRoll);
            }
        }
        else
        {
            ExecuteAttack(attack, primaryTarget, attackRoll);
            if (attack.range == 2)
            {
                if (primaryTarget < attack.targetList.Min())
                {
                    ExecuteAttack(attack, primaryTarget + 1, attackRoll);
                }
                else if (primaryTarget > attack.targetList.Max())
                {
                    ExecuteAttack(attack, primaryTarget - 1, attackRoll);
                }
                else
                {
                    System.Random random = new System.Random();
                    int addOrSub = ((random.Next(0, 2) == 1) ? 1 : -1);
                    ExecuteAttack(attack, primaryTarget + addOrSub, attackRoll);
                }
            }
            if (attack.range == 3)
            {
                if (primaryTarget < attack.targetList.Min())
                {
                    ExecuteAttack(attack, primaryTarget + 1, attackRoll);
                    ExecuteAttack(attack, primaryTarget + 2, attackRoll);
                }
                else if (primaryTarget > attack.targetList.Max())
                {
                    ExecuteAttack(attack, primaryTarget - 1, attackRoll);
                    ExecuteAttack(attack, primaryTarget - 2, attackRoll);
                }
                else
                {
                    System.Random random = new System.Random();
                    ExecuteAttack(attack, primaryTarget + 1, attackRoll);
                    ExecuteAttack(attack, primaryTarget - 1, attackRoll);
                }
            }
        }
        if (attackHit)
        {
            PlayAttackAnimation(attack.attackAnimation, attack.name);
            attackHit = false;
        }
        else
        {
            TriggerMiss(attack.attackAnimation, attack.name);
        }
    }

    public virtual void UpdateAttackVariables(Unit turnStartUnit)
    {
        if (this == turnStartUnit)
        {
            GameManager.UnusedUnitPositions();
            for (int i = 1; i < attackOptions.Count; i++)
            {
                attackOptions[i].totalAvailableTargets.ExceptWith(GameManager.UnusedUnitPositions());
                if (attackOptions[i].cooldownDuration > 0 && attackOptions[i].cooldownDuration <= attackOptions[i].cooldownMax)
                {
                    int[] x = { 0, 1, 2 };
                    attackOptions[i].cooldownDuration -= 1;
                }
            }
        }
    }
    public void ExecuteEnemyTurn()
    {
        EnemyAttack chosenAttack = ChooseAttack();
        int attackRoll = AttackRoll();
        int primaryTarget = ChoosePrimaryTarget(chosenAttack);
        ActivateAttack(chosenAttack, primaryTarget, attackRoll);
        ResetCrit();
        //PlayAttackAnimation(chosenAttack.attackAnimation, chosenAttack.name);
    }
    public virtual void ExecuteAttack(EnemyAttack attack, int target, int attackRoll)
    {
        Debug.Log("Using " + attack.name + " on " + GameManager.GetUnitFromPositionNumber(target));
        if (AttackUnit(GameManager.GetUnitFromPositionNumber(target), attackRoll))
        {
            attackHit = true;

        }
        else
        {
            return;
            Debug.Log("Did not close function");
        }
    }
}
