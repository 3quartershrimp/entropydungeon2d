using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Card : MonoBehaviour
{
    public string cardName;
    public Text cardDescription;
    public List<int> headTargets = new List<int>();

    public float damage;
    public int precision = 0;
    public bool usable = false;
    public bool hasBeenPlayed;
    public int handIndex;
    public int attackAnimation = 0;
    public Deck owningDeck;

    public int range = 1;

    public void HideCard()
    {
        gameObject.SetActive(false);
    }
    public void ShowCard()
    {
        gameObject.SetActive(true);
    }
    public void setUsable()
    {
        usable = true;
    }
    public void setUnusable()
    {
        usable = false;
    }
    public virtual void OnMouseDown()
    {
        if (usable)
        {
            if (GameManager.activeCard)
            {
                EventManager.OnRemoveHighlight();
            }
            for (int i = 0; i < headTargets.Count; i++)
            {
                float leftMostTarget = 0;
                float rightMostTarget = 0;
                if (i == 0)
                {
                    leftMostTarget = 1f;
                }
                if (i == headTargets.Count - 1)
                {
                    rightMostTarget = 1f;
                }

                int currentHeadTarget = headTargets[i];
                for (int j = 0; j < range; j++)
                {
                    if (range == 1)
                    {
                        GameManager.GetUnitFromPositionNumber(currentHeadTarget).SetHighLightMargins(GameManager.positionTransforms[currentHeadTarget].x - 1f, GameManager.positionTransforms[currentHeadTarget].x + 1f);
                    }
                    if (range == 2)
                    {
                        GameManager.GetUnitFromPositionNumber(currentHeadTarget + j).SetHighLightMargins(GameManager.positionTransforms[currentHeadTarget].x - leftMostTarget, GameManager.positionTransforms[currentHeadTarget + 1].x + rightMostTarget);
                    }
                    else if (range == 3)
                    {
                        GameManager.GetUnitFromPositionNumber(currentHeadTarget - 1 + j).SetHighLightMargins(GameManager.positionTransforms[currentHeadTarget - 1].x - leftMostTarget, GameManager.positionTransforms[currentHeadTarget + 1].x + rightMostTarget);
                    }
                    else if (range == 4)
                    {
                        GameManager.GetUnitFromPositionNumber(currentHeadTarget + j).SetHighLightMargins(GameManager.positionTransforms[currentHeadTarget].x - leftMostTarget, GameManager.positionTransforms[currentHeadTarget + 3].x + rightMostTarget);
                    }
                }
            }


            GameManager.ActivateCard(this);
            EventManager.OnRightClick += RemoveTargets;
            EventManager.OnRemoveHighlight += RemoveTargets;
            EventManager.CardActivatedClick += TriggerCard;

        }
    }

    public virtual void RemoveTargets()
    {
        GameManager.DeactivateCard();
        EventManager.CardActivatedClick -= TriggerCard;
        EventManager.OnRightClick -= RemoveTargets;
        EventManager.OnRemoveHighlight -= RemoveTargets;


    }
    public virtual void TriggerCard(Player cardConsumer, Unit targetUnit)
    {
        RemoveTargets();

        cardConsumer.accuracy += precision;
        ActivateCard(cardConsumer, targetUnit);
        cardConsumer.accuracy -= precision;


        owningDeck.setUnusableAllCards();
        HideCard();
        MoveToDiscardPile();
    }

    public virtual void ActivateCard(Player cardConsumer, Unit targetUnit)
    {
        bool missAnimation = true;

        int attackRoll = cardConsumer.AttackRoll();
        if (attackRoll == 10)
        {
            cardConsumer.crit = true;
            cardConsumer.damageMultiplier += 1f;
        }
        for (int i = 0; i < 8; i++)
        {
            Unit target = GameManager.GetUnitFromPositionNumber(i);

            if (target.highlighted)
            {
                if (!target.isDead)
                {
                    if (cardConsumer.AttackUnit(targetUnit, attackRoll))
                    {
                        CardEffects(target, cardConsumer);
                        missAnimation = false;
                    }
                }
            }

        }
        cardConsumer.ResetCrit();
        if (missAnimation)
        {
            cardConsumer.TriggerMiss(attackAnimation, cardName);
        }
        else
        {
            cardConsumer.PlayAttackAnimation(attackAnimation, cardName);
        }
    }
    public void MoveToDiscardPile()
    {
        owningDeck.owner.playerUI.availableCardSlots[handIndex] = true;
        owningDeck.owner.discardPile.Add(this);
        owningDeck.availableCards.Remove(this);

        owningDeck.owner.DrawCard();
    }
    virtual public void CardEffects(Unit Target, Player caster)
    {

    }
    public void OnCollisionEnter2D(Collision2D col)
    {
        Card card = col.gameObject.GetComponent<Card>();
        if (card.owningDeck.owner != owningDeck.owner)
        {
            Debug.Log(owningDeck.owner.unitName + " " + name + " collided with " + card.owningDeck.owner.unitName + " " + card.name);
        }
    }

}