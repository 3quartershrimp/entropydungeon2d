using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dodge : Persistent
{
    // Start is called before the first frame update
    int dodgeModifier;
    public void Start()
    {

    }
    public void ActivatePersistent(Unit unitToAffect, int duration, int dodgeAmount)
    {
        persistentName = "Dodge";
        dodgeModifier = dodgeAmount;
        unitToAffect.dodge += dodgeModifier;
        base.ActivatePersistent(unitToAffect, duration);

    }
    public override void RemovePersistent(Unit triggeringUnit)
    {
        if (affectedUnit == triggeringUnit)
        {
            triggeringUnit.dodge -= dodgeModifier;
            base.RemovePersistent(triggeringUnit);
        }
    }

}
