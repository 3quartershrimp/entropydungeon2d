using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagePersistent : Persistent
{
    //Persistant variables
    float damageModifier;
    float rageMultiplier;
    public void Start()
    {

    }
    public override void TriggerPersistent(Unit triggeringUnit)
    {
        if (affectedUnit == triggeringUnit)
        {
            triggeringUnit.TakeDamage(2);
            affectedUnit.damageMultiplier -= damageModifier;


            damageModifier = CalculateRageDamage();
            affectedUnit.damageMultiplier += damageModifier;

        }
    }
    public override void ActivatePersistent(Unit unitToAffect, int duraction)
    {
        rageMultiplier = 1;
        persistentName = "Rage";
        triggerList.Add(triggerEventEnum.TURNSTART);
        base.ActivatePersistent(unitToAffect, duraction);
        unitToAffect.TakeDamage(2);
        damageModifier = CalculateRageDamage();
        affectedUnit.damageMultiplier += damageModifier;

    }
    public override void RemovePersistent(Unit triggeringUnit)
    {
        if (affectedUnit == triggeringUnit)
        {

            affectedUnit.damageMultiplier -= damageModifier;
            base.RemovePersistent(triggeringUnit);
        }
    }
    public float CalculateRageDamage()
    {
        float missingHealthPercentDamage = (affectedUnit.maxHP - affectedUnit.currentHP) / affectedUnit.maxHP;
        return (missingHealthPercentDamage * rageMultiplier);

    }


}
