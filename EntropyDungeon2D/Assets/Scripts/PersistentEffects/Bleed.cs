using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bleed : Persistent
{
    //Persistant variables
    int damage;
    public void Start()
    {

    }
    public override void TriggerPersistent(Unit triggeringUnit)
    {
        if (affectedUnit == triggeringUnit)
        {
            triggeringUnit.TakeDamage(damage);

        }
    }
    public void ActivatePersistent(Unit unitToAffect, int duration, int bleedDmg)
    {
        persistentName = "Bleed";
        damage = bleedDmg;
        triggerList.Add(triggerEventEnum.TURNSTART);
        base.ActivatePersistent(unitToAffect, duration);
    }
    public override void RemovePersistent(Unit triggeringUnit)
    {
        if (affectedUnit == triggeringUnit)
        {
            base.RemovePersistent(triggeringUnit);
        }
    }

}
