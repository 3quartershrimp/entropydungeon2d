using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Persistent
{
    // Start is called before the first frame update
    int armorModifier;
    public void Start()
    {

    }
    public void ActivatePersistent(Unit unitToAffect, int duration, int armorAmount)
    {
        persistentName = "Armor";
        armorModifier = armorAmount;
        unitToAffect.armor += armorModifier;
        base.ActivatePersistent(unitToAffect, duration);

    }
    public override void RemovePersistent(Unit triggeringUnit)
    {
        if (affectedUnit == triggeringUnit)
        {
            triggeringUnit.armor -= armorModifier;
            base.RemovePersistent(triggeringUnit);
        }
    }

}
