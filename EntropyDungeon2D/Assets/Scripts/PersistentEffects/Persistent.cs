using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Persistent : MonoBehaviour
{
    public enum triggerEventEnum
    {
        ONHIT, ONHURT, TURNSTART, TURNEND, DEATH
    }
    public int maxDuration;
    public int remainingDuration;
    public bool persistentAffectingUnit = false;
    public List<triggerEventEnum> triggerList = new List<triggerEventEnum>();
    public Unit affectedUnit;
    public string persistentName;
    public SpriteRenderer icon;
    public TextMeshPro durationText;
    public GameObject selfPrefab;

    public virtual void TriggerPersistent(Unit triggeringUnit)
    {

    }
    public virtual void UpdatePersistentVariables(Unit triggeringUnit)
    {

    }
    public void TickPersistent(Unit triggeringUnit)
    {
        if (affectedUnit == triggeringUnit)
        {
            remainingDuration -= 1;
            durationText.text = remainingDuration.ToString();
            if (remainingDuration <= 0)
            {
                EventManager.OnTurnStart += RemovePersistent;
            }
        }
    }
    public virtual void RemovePersistent(Unit triggeringUnit)
    {
        if (affectedUnit == triggeringUnit)
        {
            triggeringUnit.iconOffset -= .5f;
            EventManager.OnUnitDeath -= RemovePersistent;
            affectedUnit.RemovePersistent(this);
            EventManager.OnTurnStart -= RemovePersistent;
            RemoveTriggersFromEventListener();
            Destroy(selfPrefab);
            affectedUnit = null;
        }
    }
    public virtual void ActivatePersistent(Unit unitToAffect, int duration)
    {
        maxDuration = duration;
        remainingDuration = maxDuration;
        durationText.text = remainingDuration.ToString();
        transform.parent = unitToAffect.transform;
        transform.position = new Vector2(unitToAffect.iconContainer.position.x + unitToAffect.iconOffset, unitToAffect.iconContainer.position.y);
        unitToAffect.iconOffset += .5f;
        affectedUnit = unitToAffect;
        affectedUnit.ActivatePersistent(this);
        persistentAffectingUnit = true;
        EventManager.OnTurnEnd += TickPersistent;
        EventManager.OnUnitDeath += RemovePersistent;
        AddTriggersToEventListener();
    }
    public virtual void AddTriggersToEventListener()
    {
        if (triggerList.Contains(triggerEventEnum.ONHIT))
        {
            EventManager.OnUnitAttacked += TriggerPersistent;
        }
        if (triggerList.Contains(triggerEventEnum.ONHURT))
        {
            EventManager.OnUnitDamaged += TriggerPersistent;
        }
        if (triggerList.Contains(triggerEventEnum.TURNSTART))
        {
            EventManager.OnTurnStart += TriggerPersistent;
        }
        if (triggerList.Contains(triggerEventEnum.TURNEND))
        {
            EventManager.OnTurnEnd += TriggerPersistent;
        }
        if (triggerList.Contains(triggerEventEnum.DEATH))
        {
            EventManager.OnUnitDeath += TriggerPersistent;
        }
    }
    public virtual void RemoveTriggersFromEventListener()
    {
        if (triggerList.Contains(triggerEventEnum.ONHIT))
        {
            EventManager.OnUnitAttacked -= TriggerPersistent;
        }
        if (triggerList.Contains(triggerEventEnum.ONHURT))
        {
            EventManager.OnUnitDamaged -= TriggerPersistent;
        }
        if (triggerList.Contains(triggerEventEnum.TURNSTART))
        {
            EventManager.OnTurnStart -= TriggerPersistent;
        }
        if (triggerList.Contains(triggerEventEnum.TURNEND))
        {
            EventManager.OnTurnEnd -= TriggerPersistent;
        }
        if (triggerList.Contains(triggerEventEnum.DEATH))
        {
            EventManager.OnUnitDeath -= TriggerPersistent;
        }
    }
}
