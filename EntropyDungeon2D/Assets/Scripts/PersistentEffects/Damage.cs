using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : Persistent
{
    // Start is called before the first frame update
    public float damageModifier;
    public void Start()
    {

    }
    public void ActivatePersistent(Unit unitToAffect, int duration, int damageAmount)
    {
        persistentName = "Damage";
        damageModifier = damageAmount * .1f;
        unitToAffect.damageMultiplier += damageModifier;
        base.ActivatePersistent(unitToAffect, duration);

    }
    public override void RemovePersistent(Unit triggeringUnit)
    {
        if (affectedUnit == triggeringUnit)
        {
            triggeringUnit.damageMultiplier -= damageModifier;
            base.RemovePersistent(triggeringUnit);
        }
    }

}
