using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accuracy : Persistent
{
    // Start is called before the first frame update
    int accuracyModifier;
    public void Start()
    {

    }
    public void ActivatePersistent(Unit unitToAffect, int duration, int accuracyAmount)
    {
        persistentName = "Accuracy";
        accuracyModifier = accuracyAmount;
        unitToAffect.accuracy += accuracyModifier;
        base.ActivatePersistent(unitToAffect, duration);

    }
    public override void RemovePersistent(Unit triggeringUnit)
    {
        if (affectedUnit == triggeringUnit)
        {
            triggeringUnit.dodge -= accuracyModifier;
            base.RemovePersistent(triggeringUnit);
        }
    }

}
