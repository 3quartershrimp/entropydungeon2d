using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    //Unit Stats/Modifiers
    public string unitName;
    public float maxHP;
    public float currentHP;
    public int speed;
    public int dodge = 0;
    public int accuracy = 0;
    public float damageMultiplier = 1;
    public int armor;
    public bool isDead = false;
    public int index;
    public int fieldPosition;
    public bool crit;

    public BattleHud unitUI;
    public bool setUI = false;
    public string type;
    [SerializeField] bool m_noBlood = false;

    protected Animator m_animator;
    public Renderer selectedMarker;
    public Renderer targetedMarker;
    public Renderer turnMarker;
    protected SpriteRenderer sprite;
    protected Color startColor;

    public bool highlighted;
    public float leftHighlightMargin;
    public float rightHighlightMargin;
    public bool targeted = false;
    public bool headTargetBool = false;
    public List<Unit> headTargetUnits = new List<Unit>();
    //Persistant display
    public Transform iconContainer;
    public float iconOffset;
    public List<Persistent> persistentEffects = new List<Persistent>();
    // Start is called before the first frame update
    public virtual void Start()
    {
        m_animator = GetComponentInChildren(typeof(Animator)) as Animator;
        sprite = GetComponentInChildren(typeof(SpriteRenderer)) as SpriteRenderer;
        startColor = sprite.material.color;
        EventManager.OnRightClick += RemoveTargets;
        EventManager.OnRemoveHighlight += RemoveTargets;
    }
    public virtual void Update()
    {
        if (GameManager.activeCard != null)
        {
            checkMouseToWorldPosition();
        }
        else
        {
            RemoveHighlight();
        }
    }
    public void checkMouseToWorldPosition()
    {
        if (leftHighlightMargin != 0 && rightHighlightMargin != 0)
        {
            float mouseX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
            float mouseY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
            if (leftHighlightMargin < mouseX && mouseX < rightHighlightMargin && .5 <= mouseY && mouseY <= 3)
            {
                highlighted = true;
                sprite.material.color = Color.yellow;
            }
            else
            {
                RemoveHighlight();
            }
        }
    }
    public virtual void TakeDamage(float dmg)
    {
        if (!isDead)
        {
            EventManager.OnUnitAttacked(this);
            currentHP -= dmg * (1 - armor / 10);
            if (currentHP <= 0)
            {
                KillUnit();
            }
            else
            {
                EventManager.OnUnitDamaged(this);
                m_animator.SetTrigger("Hurt");
            }
        }
    }
    void KillUnit()
    {
        m_animator.SetBool("noBlood", m_noBlood);
        m_animator.SetTrigger("Death");
        isDead = true;
        currentHP = 0;
        GameManager.usedUnitPositions.Remove(fieldPosition.ToString());
        turnMarker.enabled = false;
        EventManager.OnUnitDeath(this);
    }
    void OnMouseDown()
    {
        EventManager.FocusBattleHud += FocusHud;
        EventManager.FocusBattleHud();
        EventManager.FocusBattleHud -= FocusHud;
    }
    void OnMouseOver()
    {
        if (headTargetBool)
        {
            EventManager.ShowAoETargets(this);
        }
    }
    void OnMouseExit()
    {

    }

    public virtual void FocusHud()
    {
        if (!GameManager.activeCard)
        {
            unitUI.transform.position = new Vector2(0, 0);
            selectedMarker.enabled = true;
        }
    }



    public virtual void PlayAttackAnimation(int animationNumber, string attackName)
    {
        EventManager.OnAttackAnnounce(unitName, attackName);
        turnMarker.enabled = false;
        if (animationNumber == 0)
        {
            m_animator.SetTrigger("Attack");
        }
        else
        {
            UnitAttackAnimation(animationNumber);
        }

    }
    public virtual void TriggerMiss(int animationNumber, string attackName)
    {
        EventManager.OnMissAnnounce(unitName, attackName);
        turnMarker.enabled = false;
        if (animationNumber == 0)
        {
            m_animator.SetTrigger("Attack");
        }
        else
        {
            UnitAttackAnimation(animationNumber);
        }
    }
    public virtual int AttackRoll()
    {
        int roll = Random.Range(1, 11);
        if (roll == 10)
        {
            crit = true;
            damageMultiplier += 1f;
        }
        return roll;
    }
    public virtual void ResetCrit()
    {
        if (crit)
        {
            crit = false;
            damageMultiplier -= 1f;
        }
    }
    public virtual void UnitAttackAnimation(int animationNumber)
    {
    }
    public virtual bool AttackUnit(Unit target, int attackRoll)
    {
        if (type == target.type)
        {
            return true;
        }
        if (attackRoll == 1)
        {
            return false;
        }
        else if (attackRoll == 10)
        {
            Debug.Log(name + " is attacking " + target + " and AttackRolled a:" + attackRoll + " CRIT!");
            return true;
        }
        else
        {
            if (attackRoll + (accuracy - target.dodge) > 1)
            {
                Debug.Log(name + " is attacking " + target + " and AttackRolled a:" + attackRoll + " HIT!");
                return true;
            }
            else
            {
                Debug.Log(name + " is attacking " + target + " and AttackRolled a:" + attackRoll + " Miss!");
                return false;
            }

        }
    }
    public void SetHud(BattleHud ui)
    {
        unitUI = ui;
        ui.attatchedUnit = this;
        setUI = true;

        Transform selected = gameObject.transform.Find(type + "Markers/Selected");
        selectedMarker = selected.GetComponent<Renderer>();
        Transform targeted = gameObject.transform.Find(type + "Markers/Targeted");
        targetedMarker = targeted.GetComponent<Renderer>();
        Transform turn = gameObject.transform.Find(type + "Markers/Turn");
        turnMarker = turn.GetComponent<Renderer>();

        selectedMarker.enabled = false;
        targetedMarker.enabled = false;
        turnMarker.enabled = false;
    }
    public void Heal(int health)
    {
        currentHP += health;
        if (currentHP > maxHP)
        {
            currentHP = maxHP;
        }
    }
    public void RemoveTargets()
    {
        targeted = false;
        targetedMarker.enabled = false;
        headTargetUnits = new List<Unit>();
        headTargetBool = false;
        ResetMargins();
        RemoveHighlight();
    }
    public void AddAoETargetListener(Unit headUnitTarget)
    {
        headTargetUnits.Add(headUnitTarget);
        EventManager.ShowAoETargets += HighLightOnAoEHover;
    }
    public void HighLightOnAoEHover(Unit headUnitTarget)
    {
        if (headTargetUnits.Contains(headUnitTarget))
        {
            highlighted = true;

            sprite.material.color = Color.yellow;
        }
    }
    public void SetHighLightMargins(float leftMargin, float rightMargin)
    {
        if (!isDead)
        {
            if (rightHighlightMargin != 0)
            {
                rightHighlightMargin = rightMargin;
            }
            else
            {
                leftHighlightMargin = leftMargin;
                rightHighlightMargin = rightMargin;
            }
        }
    }
    public void ResetMargins()
    {
        leftHighlightMargin = 0;
        rightHighlightMargin = 0;
    }
    public void RemoveHighlight()
    {
        highlighted = false;
        sprite.material.color = startColor;
    }
    public void MoveStateInitial()
    {
        Debug.Log(name + " move pressed");
    }
    public void ActivatePersistent(Persistent persistentEffect)
    {
        persistentEffects.Add(persistentEffect);
        //persistantEffect.ActivatePersistant(this);
    }
    public void RemovePersistent(Persistent persistentEffect)
    {
        persistentEffects.Remove(persistentEffect);
    }

}
