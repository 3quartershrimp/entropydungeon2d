﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Bandit::Start()
extern void Bandit_Start_mEB90DB27250F1DCCE72DAE8E2AD70564A371F810 (void);
// 0x00000002 System.Void Bandit::Update()
extern void Bandit_Update_m4AA56D8697178973D4D5445450331535AB0C78C5 (void);
// 0x00000003 System.Void Bandit::.ctor()
extern void Bandit__ctor_mBB3235C162847932D6883C51E25822D7A1DF4DAF (void);
// 0x00000004 System.Void Sensor_Bandit::OnEnable()
extern void Sensor_Bandit_OnEnable_m4ED93834FCB211044DF2BACA55B8ABE01959922E (void);
// 0x00000005 System.Boolean Sensor_Bandit::State()
extern void Sensor_Bandit_State_m4B507F8CCB75D39B4774381BCF26431C05C27F0E (void);
// 0x00000006 System.Void Sensor_Bandit::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Sensor_Bandit_OnTriggerEnter2D_mA863ABC3FE70839ADC7FEDFE5D4D35E056AF5CB2 (void);
// 0x00000007 System.Void Sensor_Bandit::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Sensor_Bandit_OnTriggerExit2D_m88D705EA71FE3E1B4757D55C2E66C8893E3FAB24 (void);
// 0x00000008 System.Void Sensor_Bandit::Update()
extern void Sensor_Bandit_Update_m4955C0055975BE059A9E175C6D515ED9D5C35595 (void);
// 0x00000009 System.Void Sensor_Bandit::Disable(System.Single)
extern void Sensor_Bandit_Disable_mC2AF8AE55A2394EABA233CEE33A25F57A606F608 (void);
// 0x0000000A System.Void Sensor_Bandit::.ctor()
extern void Sensor_Bandit__ctor_mCF95685F73F65AC3E74D714A3DC0F5022E97494E (void);
// 0x0000000B System.Void ColorSwap_HeroKnight::Awake()
extern void ColorSwap_HeroKnight_Awake_mC300E77BAC55F3F3762EAB6ED443812CE7CED35E (void);
// 0x0000000C System.Void ColorSwap_HeroKnight::OnValidate()
extern void ColorSwap_HeroKnight_OnValidate_m77635C1E40FFADF346D52DE69DE571DEDA4AC2A4 (void);
// 0x0000000D System.Void ColorSwap_HeroKnight::SwapDemoColors()
extern void ColorSwap_HeroKnight_SwapDemoColors_m7451617A6E75CCC436C10D134AF83D2805A4968A (void);
// 0x0000000E UnityEngine.Color ColorSwap_HeroKnight::ColorFromInt(System.Int32,System.Single)
extern void ColorSwap_HeroKnight_ColorFromInt_mE05FB84D8AD045174A319A09C1CB96818E63E6C2 (void);
// 0x0000000F UnityEngine.Color ColorSwap_HeroKnight::ColorFromIntRGB(System.Int32,System.Int32,System.Int32)
extern void ColorSwap_HeroKnight_ColorFromIntRGB_m207C14A2EAED72FFD3DB2D763E6894149E8B349F (void);
// 0x00000010 System.Void ColorSwap_HeroKnight::InitColorSwapTex()
extern void ColorSwap_HeroKnight_InitColorSwapTex_mBFF880467D0FA6AB9A8B5C909354651BF7B7F9A2 (void);
// 0x00000011 System.Void ColorSwap_HeroKnight::SwapColor(System.Int32,UnityEngine.Color)
extern void ColorSwap_HeroKnight_SwapColor_m744618ACE4BDEF0C04FAFB80C5A3A108BF5C2A69 (void);
// 0x00000012 System.Void ColorSwap_HeroKnight::SwapColors(System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void ColorSwap_HeroKnight_SwapColors_mE1194C38947A0A7047493FBF805C3DEB23F6BDDB (void);
// 0x00000013 System.Void ColorSwap_HeroKnight::ClearColor(System.Int32)
extern void ColorSwap_HeroKnight_ClearColor_m4CA8CA5C65175664E6BD499A73E8F9461A5A5FEB (void);
// 0x00000014 System.Void ColorSwap_HeroKnight::SwapAllSpritesColorsTemporarily(UnityEngine.Color)
extern void ColorSwap_HeroKnight_SwapAllSpritesColorsTemporarily_m1E4E97C84A47A22F27D8A65809F35C01DA177C58 (void);
// 0x00000015 System.Void ColorSwap_HeroKnight::ResetAllSpritesColors()
extern void ColorSwap_HeroKnight_ResetAllSpritesColors_m16C79ABF75ED77292FF5DB7EF6DD51DB0BABCEB9 (void);
// 0x00000016 System.Void ColorSwap_HeroKnight::ClearAllSpritesColors()
extern void ColorSwap_HeroKnight_ClearAllSpritesColors_m39806D8DF38844A8F8866FC0A23ED5C162AAF834 (void);
// 0x00000017 System.Void ColorSwap_HeroKnight::.ctor()
extern void ColorSwap_HeroKnight__ctor_m164CB224E9E17E806EF9F87E0662D5A9A045732D (void);
// 0x00000018 System.Void DestroyEvent_HeroKnight::destroyEvent()
extern void DestroyEvent_HeroKnight_destroyEvent_mABFA897B5C96D82C3AA82CE5425C0380EA336826 (void);
// 0x00000019 System.Void DestroyEvent_HeroKnight::.ctor()
extern void DestroyEvent_HeroKnight__ctor_mCDD3ED77C731A1FD2AFC845DA4A5EC957CD0B1E9 (void);
// 0x0000001A System.Void HeroKnight::Start()
extern void HeroKnight_Start_mB77EC0BF5BC694C067CC37A724F671933FF3A16E (void);
// 0x0000001B System.Void HeroKnight::Update()
extern void HeroKnight_Update_mD460E5567A914F73972E559A4261DF780934100D (void);
// 0x0000001C System.Void HeroKnight::AE_SlideDust()
extern void HeroKnight_AE_SlideDust_mF799813F26CBA8887202F82B369744097CD864D9 (void);
// 0x0000001D System.Void HeroKnight::.ctor()
extern void HeroKnight__ctor_m978840F0B37FAF48B70AC0F1B509DA1BD789CF52 (void);
// 0x0000001E System.Void Sensor_HeroKnight::OnEnable()
extern void Sensor_HeroKnight_OnEnable_m6550F3ADE6F149BDB3E4E81F905EAFB3D598C032 (void);
// 0x0000001F System.Boolean Sensor_HeroKnight::State()
extern void Sensor_HeroKnight_State_m26ACCFC47992B42B5BE0BB5E7708ED786903CCE8 (void);
// 0x00000020 System.Void Sensor_HeroKnight::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Sensor_HeroKnight_OnTriggerEnter2D_m20D86F4E79BE4A4283629D9A4434E061D4681CD5 (void);
// 0x00000021 System.Void Sensor_HeroKnight::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Sensor_HeroKnight_OnTriggerExit2D_m9865B7DD0D122E11D6CF9A22618467C7FBF41F59 (void);
// 0x00000022 System.Void Sensor_HeroKnight::Update()
extern void Sensor_HeroKnight_Update_m29B63C74411E6E502E72C00CAD874F5C5744C0EB (void);
// 0x00000023 System.Void Sensor_HeroKnight::Disable(System.Single)
extern void Sensor_HeroKnight_Disable_m6759A3052DF6C5FBBA89E598D8C4E85E4C7B02C3 (void);
// 0x00000024 System.Void Sensor_HeroKnight::.ctor()
extern void Sensor_HeroKnight__ctor_m20BB68D68636C8356A8B857FE0855BF7E61C37DB (void);
// 0x00000025 System.Void ColorSwap_PixelHeroes::Awake()
extern void ColorSwap_PixelHeroes_Awake_m5C9F26E1B3E3CBA8C1E5A8E21B7839217CBE495D (void);
// 0x00000026 System.Void ColorSwap_PixelHeroes::OnValidate()
extern void ColorSwap_PixelHeroes_OnValidate_m243DE130C38F16CD1A8FD4A09AADABACD7953451 (void);
// 0x00000027 System.Void ColorSwap_PixelHeroes::SwapDemoColors()
extern void ColorSwap_PixelHeroes_SwapDemoColors_mD57FB295D42BBEE6E7D9436FCC6A1F89AD24C585 (void);
// 0x00000028 UnityEngine.Color ColorSwap_PixelHeroes::ColorFromInt(System.Int32,System.Single)
extern void ColorSwap_PixelHeroes_ColorFromInt_mB2DF7EE96D293C4BC36C7B81C637200B9F0A1DDB (void);
// 0x00000029 UnityEngine.Color ColorSwap_PixelHeroes::ColorFromIntRGB(System.Int32,System.Int32,System.Int32)
extern void ColorSwap_PixelHeroes_ColorFromIntRGB_m8B82CC9540BBFEAEC3C09F410DFFFFEF5B890131 (void);
// 0x0000002A System.Void ColorSwap_PixelHeroes::InitColorSwapTex()
extern void ColorSwap_PixelHeroes_InitColorSwapTex_m0A3A9CCC7781993C1B8BF6F66105C7F4F00EE0A0 (void);
// 0x0000002B System.Void ColorSwap_PixelHeroes::SwapColor(System.Int32,UnityEngine.Color)
extern void ColorSwap_PixelHeroes_SwapColor_mCB1A49EED094C75C475977E92BDED337B915FEBD (void);
// 0x0000002C System.Void ColorSwap_PixelHeroes::SwapColors(System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void ColorSwap_PixelHeroes_SwapColors_mE082CC4D4842C679F9B62352D0201ADED785440B (void);
// 0x0000002D System.Void ColorSwap_PixelHeroes::ClearColor(System.Int32)
extern void ColorSwap_PixelHeroes_ClearColor_mE9B89123B76A3FD3744EB7C3EA65CBE0EF19A6C3 (void);
// 0x0000002E System.Void ColorSwap_PixelHeroes::SwapAllSpritesColorsTemporarily(UnityEngine.Color)
extern void ColorSwap_PixelHeroes_SwapAllSpritesColorsTemporarily_m0677F7365342DB9E4C9AD4AA85BD392EFF6EA3E3 (void);
// 0x0000002F System.Void ColorSwap_PixelHeroes::ResetAllSpritesColors()
extern void ColorSwap_PixelHeroes_ResetAllSpritesColors_m91F26DFCF3CE7F3B402BC1F6181B75DE4E9ABF7D (void);
// 0x00000030 System.Void ColorSwap_PixelHeroes::ClearAllSpritesColors()
extern void ColorSwap_PixelHeroes_ClearAllSpritesColors_m60A77F9607F545A633CB206B4D60DC0B0A060DE5 (void);
// 0x00000031 System.Void ColorSwap_PixelHeroes::.ctor()
extern void ColorSwap_PixelHeroes__ctor_m89B55CFDFE6005C89C7FFB3B2AE09C0B224122DA (void);
// 0x00000032 System.Void Alchemist::Start()
extern void Alchemist_Start_mA2E964110CBF6BCE68715BEF5DC9957BE9F80CDA (void);
// 0x00000033 System.Void Alchemist::Update()
extern void Alchemist_Update_m28E59E450C1AF2E6F2C432F84C523DFEC8750F94 (void);
// 0x00000034 System.Void Alchemist::.ctor()
extern void Alchemist__ctor_mA95765BFFDFC2AD25D4B4F233A4E8E85D2E48DA8 (void);
// 0x00000035 System.Void Barbarian::Start()
extern void Barbarian_Start_mDD53F735E2C3F320B46FE5631E9318429F13D64F (void);
// 0x00000036 System.Void Barbarian::Update()
extern void Barbarian_Update_m2A34B7C51D1477B4D7BA555BE50B1F71018BC0FD (void);
// 0x00000037 System.Void Barbarian::.ctor()
extern void Barbarian__ctor_m1EA293C871BA67DDFB6DF20CA95587E482C10802 (void);
// 0x00000038 System.Void Blademaster::Start()
extern void Blademaster_Start_mA51C5116E3BF7828377DF6B9C9885C829D7178B5 (void);
// 0x00000039 System.Void Blademaster::Update()
extern void Blademaster_Update_m6C28D795265E43ADFB010269703B7FBB4B4A7C69 (void);
// 0x0000003A System.Void Blademaster::.ctor()
extern void Blademaster__ctor_mC08F1D1334C26E2DA10C754659269F1350381CA9 (void);
// 0x0000003B System.Void CharacterSelector_PixelHeroes::Start()
extern void CharacterSelector_PixelHeroes_Start_m8844166B74754031FA361F8AFA065E9CF9C9A0D8 (void);
// 0x0000003C System.Void CharacterSelector_PixelHeroes::Update()
extern void CharacterSelector_PixelHeroes_Update_m673B51FAF8802DA07770C8C0FFC3BD3CB1BF46BC (void);
// 0x0000003D System.Void CharacterSelector_PixelHeroes::changeCharacter(System.Int32)
extern void CharacterSelector_PixelHeroes_changeCharacter_mA03B037A4A28B5567505C5083BB3B09C10135FAD (void);
// 0x0000003E System.Void CharacterSelector_PixelHeroes::.ctor()
extern void CharacterSelector_PixelHeroes__ctor_m541DBE611870C51D8DBB72B009F822727D898751 (void);
// 0x0000003F System.Void CharacterSelector_PixelHeroes/ColorsCharacters::.ctor()
extern void ColorsCharacters__ctor_mC93F552E448526D6E23F301624A8DD2A7552BB60 (void);
// 0x00000040 System.Void Druid::Start()
extern void Druid_Start_m1EE12E6CCE170D4791D9A14390EBD4B9633260DA (void);
// 0x00000041 System.Void Druid::Update()
extern void Druid_Update_m440A8E57C6368579A6ECAFF321F5FF25B0FE9508 (void);
// 0x00000042 System.Void Druid::.ctor()
extern void Druid__ctor_m5CEC17B0C147470377DE5761E5C5488E39B4A7E4 (void);
// 0x00000043 System.Void Effects_DestroyEvent::destroyEvent()
extern void Effects_DestroyEvent_destroyEvent_m37AB761B35C37BF937B3E5F35BE2253016F63C04 (void);
// 0x00000044 System.Void Effects_DestroyEvent::.ctor()
extern void Effects_DestroyEvent__ctor_mF23E44F214E5DD362747BBC9B421C02449AF0372 (void);
// 0x00000045 System.Void Effects_Projectile::Start()
extern void Effects_Projectile_Start_mA70FD1335C00993E97A0138DEDD9F18F0A564207 (void);
// 0x00000046 System.Void Effects_Projectile::Update()
extern void Effects_Projectile_Update_mA2221CBC924A3018CE65A56F00BDEB7284F4622F (void);
// 0x00000047 System.Void Effects_Projectile::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Effects_Projectile_OnTriggerEnter2D_m1681BCDA59DCC12CB4912DE330BC7948DFCCFC02 (void);
// 0x00000048 System.Void Effects_Projectile::.ctor()
extern void Effects_Projectile__ctor_m2A4E4E7591D5E605924087124B746F61B6A1E026 (void);
// 0x00000049 System.Void Firemage::Start()
extern void Firemage_Start_m697201DDB98ABF9A61B02E15FE70981589E6921A (void);
// 0x0000004A System.Void Firemage::Update()
extern void Firemage_Update_mB6374D94CE7D30C0E48A39D9721E3D6CBDB752DD (void);
// 0x0000004B System.Void Firemage::.ctor()
extern void Firemage__ctor_mD8F5E64A1BA2A779907849A89A87B59D370584BC (void);
// 0x0000004C System.Void Necromancer::Start()
extern void Necromancer_Start_m5E3710A74901CFB80CE4628893420BEAC1EC1C42 (void);
// 0x0000004D System.Void Necromancer::Update()
extern void Necromancer_Update_mCF3D573C7B1143ADA73A04DB67D38BAEB1D66F49 (void);
// 0x0000004E System.Void Necromancer::.ctor()
extern void Necromancer__ctor_m666CF782739C68FDDF0C7FEE617BC2FA4B46289E (void);
// 0x0000004F System.Void Priest::Start()
extern void Priest_Start_mD8A4D7389CDCE56823C0C4334DCE4D3358776379 (void);
// 0x00000050 System.Void Priest::Update()
extern void Priest_Update_m46298572E41ED4657757D68844A1DCC0838D7506 (void);
// 0x00000051 System.Void Priest::.ctor()
extern void Priest__ctor_mFD155429FC0A8F10C287295C0CE10BA43F4E2E17 (void);
// 0x00000052 System.Void Rogue::Start()
extern void Rogue_Start_mC43A0AE0DBF2EAC3E02F1BF10227244C374A1EA0 (void);
// 0x00000053 System.Void Rogue::Update()
extern void Rogue_Update_m2B1F193E99CFB06AFDA529DD05CD718B74353468 (void);
// 0x00000054 System.Void Rogue::SpawnProjectile()
extern void Rogue_SpawnProjectile_m55800C5A11644E4BAA5185022434DCA5CEB59172 (void);
// 0x00000055 System.Void Rogue::.ctor()
extern void Rogue__ctor_m44BF4BBB22E449A1A4FA47AB2A6DCAD65ECA3D16 (void);
// 0x00000056 System.Void Sensor_PixelHeroes::OnEnable()
extern void Sensor_PixelHeroes_OnEnable_m2FEDAEFDAD059F7023BB6391D0525A6751054BE0 (void);
// 0x00000057 System.Boolean Sensor_PixelHeroes::State()
extern void Sensor_PixelHeroes_State_mB7F972F38DE9D139BE420CB95B91584AAA6B0023 (void);
// 0x00000058 System.Void Sensor_PixelHeroes::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Sensor_PixelHeroes_OnTriggerEnter2D_mE7FB437A5753CC4923D6494862E948F30513EF7D (void);
// 0x00000059 System.Void Sensor_PixelHeroes::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Sensor_PixelHeroes_OnTriggerExit2D_m986D159070DC74B5AD8CBE4534F84F71F9881207 (void);
// 0x0000005A System.Void Sensor_PixelHeroes::Update()
extern void Sensor_PixelHeroes_Update_mEB023E057D72EA444674C6FDD1633142F840BAEE (void);
// 0x0000005B System.Void Sensor_PixelHeroes::Disable(System.Single)
extern void Sensor_PixelHeroes_Disable_m8BECF797F66C56EE7B68B513C850FCA8A5A75774 (void);
// 0x0000005C System.Void Sensor_PixelHeroes::.ctor()
extern void Sensor_PixelHeroes__ctor_mB7743927F55FC4DEA9EB137FB228C18547B4B803 (void);
// 0x0000005D System.Void StormMage::Start()
extern void StormMage_Start_mAE3A71C2C8FDCF34DE7381AA410C30C0D8BE8229 (void);
// 0x0000005E System.Void StormMage::Update()
extern void StormMage_Update_mB206A2C8D88882D4469149F340F991220B3E0DF5 (void);
// 0x0000005F System.Void StormMage::.ctor()
extern void StormMage__ctor_m0CFD67F741594BF89CEC6DCBFF53CFB9C70E41DC (void);
// 0x00000060 System.Void Warlock::Start()
extern void Warlock_Start_mAD34A241B57717364EEA02F5E6CC1F3A9EDE08F3 (void);
// 0x00000061 System.Void Warlock::Update()
extern void Warlock_Update_m077D58BC2324D8D40A8AA0FC457F43ED5F3B6874 (void);
// 0x00000062 System.Void Warlock::.ctor()
extern void Warlock__ctor_m841198F0B86899788DE536E75E82D1393F898812 (void);
// 0x00000063 System.Void AoeCard::Start()
extern void AoeCard_Start_mB0DE983A477E80AFDA05A742551296EF78D6257A (void);
// 0x00000064 System.Void AoeCard::OnMouseDown()
extern void AoeCard_OnMouseDown_mD26A91C04DD174CCF5D27760A69AA33D99C1B714 (void);
// 0x00000065 System.Void AoeCard::setAoETargets()
extern void AoeCard_setAoETargets_m54BFA79D70D1200CA65DB9BF765ECAD8E857E8AE (void);
// 0x00000066 System.Void AoeCard::removeTargets()
extern void AoeCard_removeTargets_m6218929F1220C92EB7E4F2A0935764E9F6161360 (void);
// 0x00000067 System.Void AoeCard::triggerAttackAmount(Player,Unit,System.Boolean)
extern void AoeCard_triggerAttackAmount_m77FC9A3C6AE446306C67DD09727361B60BA12D0E (void);
// 0x00000068 System.Void AoeCard::.ctor()
extern void AoeCard__ctor_mBDCD34DFB12C8DB86C2840285C4D9F0553498076 (void);
// 0x00000069 System.Void AoeCard/<>c::.cctor()
extern void U3CU3Ec__cctor_m9D072C2CF5C38693CA9FC97D7E524EDDBA8BEE33 (void);
// 0x0000006A System.Void AoeCard/<>c::.ctor()
extern void U3CU3Ec__ctor_mD1E10EA9CFFDCE5DF7A58FA90F79DA0E31034DBA (void);
// 0x0000006B System.Int32 AoeCard/<>c::<setAoETargets>b__4_0(System.Int32)
extern void U3CU3Ec_U3CsetAoETargetsU3Eb__4_0_m6E42BFC02EC593B9F5FA52A2EBB0532DD4AF3E64 (void);
// 0x0000006C System.Boolean AoeCard/<>c::<setAoETargets>b__4_1(System.Linq.IGrouping`2<System.Int32,System.Int32>)
extern void U3CU3Ec_U3CsetAoETargetsU3Eb__4_1_mEC5598C4C69973255EC10279B6340CB50D3F08A4 (void);
// 0x0000006D System.Int32 AoeCard/<>c::<setAoETargets>b__4_2(System.Linq.IGrouping`2<System.Int32,System.Int32>)
extern void U3CU3Ec_U3CsetAoETargetsU3Eb__4_2_mF1AC406C68DDA8A766A541C32E9E664F7EE96C2A (void);
// 0x0000006E System.Void BattleHud::SetHP(System.Single)
extern void BattleHud_SetHP_m61D0507689470D313BFC6F8C88ABB2A0655909DD (void);
// 0x0000006F System.Void BattleHud::.ctor()
extern void BattleHud__ctor_m3846DD309B63E89C342F920DD76EE992832FE03B (void);
// 0x00000070 System.Void BattleSystem::Start()
extern void BattleSystem_Start_m1025D66B5CF5D511911629CA264C0BD577630584 (void);
// 0x00000071 System.Void BattleSystem::Update()
extern void BattleSystem_Update_m624B962FB7102E5B072383F0657CD680BF63B4E7 (void);
// 0x00000072 System.Collections.IEnumerator BattleSystem::SetupBattle()
extern void BattleSystem_SetupBattle_m0D28E699F81949ECAA73B8591E2767488470AFFD (void);
// 0x00000073 System.Void BattleSystem::PlayerTurn(Player)
extern void BattleSystem_PlayerTurn_m59442E1E51D3B5C5C7F855AEF5DFEA9FC5F35096 (void);
// 0x00000074 System.Collections.IEnumerator BattleSystem::PlayerAttack(Player)
extern void BattleSystem_PlayerAttack_m75E91E71E1E8BA5D80D9268AC9DC341BEA40ABFE (void);
// 0x00000075 System.Collections.IEnumerator BattleSystem::EnemyTurn(Enemy)
extern void BattleSystem_EnemyTurn_m109A1BCD53ED5D8EE736B63A92BF883A33670DF6 (void);
// 0x00000076 System.Void BattleSystem::swapPlayerHud()
extern void BattleSystem_swapPlayerHud_m2EC845378E3B2D1CD3018C059CB038A087EF3ECF (void);
// 0x00000077 System.Void BattleSystem::removeBattleHuds()
extern void BattleSystem_removeBattleHuds_m0CD24082EDBA873E0F71865650B3E1A96E098F71 (void);
// 0x00000078 System.Void BattleSystem::nextTurn(Unit)
extern void BattleSystem_nextTurn_m391680418C9963C695024E2BA240A948B48AE27D (void);
// 0x00000079 System.Void BattleSystem::setUpTurnOrder(System.Collections.Generic.List`1<Player>,System.Collections.Generic.List`1<Enemy>)
extern void BattleSystem_setUpTurnOrder_m86C4EF9CF1C4EA5134F96AC49970D37B453217D6 (void);
// 0x0000007A System.Void BattleSystem::EndBattle()
extern void BattleSystem_EndBattle_mB853A74689A6108B08183925B40DA14AF371D294 (void);
// 0x0000007B System.Void BattleSystem::RestartGame()
extern void BattleSystem_RestartGame_m630345BC754A06D2D402321877BFE96F8DE0DE3A (void);
// 0x0000007C System.Void BattleSystem::cardExecuted(Player,Unit)
extern void BattleSystem_cardExecuted_mD3D553DCC4A65AD63544A382F0508FC89F693C03 (void);
// 0x0000007D System.Void BattleSystem::unitDeath(Unit)
extern void BattleSystem_unitDeath_mC7A54488C8007417EDF30694421F34CB6F4334FC (void);
// 0x0000007E System.Void BattleSystem::announceAttack(System.String,System.String)
extern void BattleSystem_announceAttack_m18A613F492AFBFD2E49B4DA0B7E96462FF183603 (void);
// 0x0000007F System.Void BattleSystem::announceMiss(System.String,System.String)
extern void BattleSystem_announceMiss_m829667AD76E199B6FC3365FDA4028C53490306F0 (void);
// 0x00000080 System.Void BattleSystem::turnStart(Unit)
extern void BattleSystem_turnStart_mB0ED601F7A04EADF190FF5EA71DEFD9C2AAE685C (void);
// 0x00000081 System.Void BattleSystem::doNothing(Unit)
extern void BattleSystem_doNothing_mE3052618955C90ACAC2B4A28BDED6B058A98FAFA (void);
// 0x00000082 System.Void BattleSystem::doNothing()
extern void BattleSystem_doNothing_m6B5599B1A53FEEC06A10EC65EB86EB04E1BB0D77 (void);
// 0x00000083 System.Void BattleSystem::.ctor()
extern void BattleSystem__ctor_mEA0CD6AB1AA5795FED3B2A0126F1A42ACE219035 (void);
// 0x00000084 System.Void BattleSystem/<SetupBattle>d__26::.ctor(System.Int32)
extern void U3CSetupBattleU3Ed__26__ctor_mC5002BC828A74E13D8D11C515DD7476E7F6DE633 (void);
// 0x00000085 System.Void BattleSystem/<SetupBattle>d__26::System.IDisposable.Dispose()
extern void U3CSetupBattleU3Ed__26_System_IDisposable_Dispose_m846F497C52C6AAA708BA240625C762A35246CE86 (void);
// 0x00000086 System.Boolean BattleSystem/<SetupBattle>d__26::MoveNext()
extern void U3CSetupBattleU3Ed__26_MoveNext_m925CE0FF7A6F0DE37B28E3320EC16932A5172811 (void);
// 0x00000087 System.Object BattleSystem/<SetupBattle>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetupBattleU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m17796639047F5A07611CC6A7E43EEFE12783F6C7 (void);
// 0x00000088 System.Void BattleSystem/<SetupBattle>d__26::System.Collections.IEnumerator.Reset()
extern void U3CSetupBattleU3Ed__26_System_Collections_IEnumerator_Reset_mE3FFFF2F9ABAED7252A8972825B36C16D0153E29 (void);
// 0x00000089 System.Object BattleSystem/<SetupBattle>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CSetupBattleU3Ed__26_System_Collections_IEnumerator_get_Current_m3FCE34B11C0CAD0B43E3D122FB1E3C0EB0B33C14 (void);
// 0x0000008A System.Void BattleSystem/<PlayerAttack>d__28::.ctor(System.Int32)
extern void U3CPlayerAttackU3Ed__28__ctor_mB66E5D13ACCA1DDFEEB3EBA579D2007F210EBACD (void);
// 0x0000008B System.Void BattleSystem/<PlayerAttack>d__28::System.IDisposable.Dispose()
extern void U3CPlayerAttackU3Ed__28_System_IDisposable_Dispose_m6DFB75D250FA56017A94B249B3F409E06D61ABF1 (void);
// 0x0000008C System.Boolean BattleSystem/<PlayerAttack>d__28::MoveNext()
extern void U3CPlayerAttackU3Ed__28_MoveNext_m81FB46EC2353316D4008C2D3D0C23B2087402C04 (void);
// 0x0000008D System.Object BattleSystem/<PlayerAttack>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayerAttackU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C2BE6E9A1853E9F24D3BB5C557F6755BFB1410 (void);
// 0x0000008E System.Void BattleSystem/<PlayerAttack>d__28::System.Collections.IEnumerator.Reset()
extern void U3CPlayerAttackU3Ed__28_System_Collections_IEnumerator_Reset_m9CC238975D5E94582C5729DE2E61BFC61BE66275 (void);
// 0x0000008F System.Object BattleSystem/<PlayerAttack>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CPlayerAttackU3Ed__28_System_Collections_IEnumerator_get_Current_m226747A1B97C392ABFA9299B8BB098F12ED54881 (void);
// 0x00000090 System.Void BattleSystem/<EnemyTurn>d__29::.ctor(System.Int32)
extern void U3CEnemyTurnU3Ed__29__ctor_m5AC36FD6F5FDBD037EB7D941BD62D522B9D0EDFF (void);
// 0x00000091 System.Void BattleSystem/<EnemyTurn>d__29::System.IDisposable.Dispose()
extern void U3CEnemyTurnU3Ed__29_System_IDisposable_Dispose_m287F93E7AD702483674DF8561E0184C5A4802C6E (void);
// 0x00000092 System.Boolean BattleSystem/<EnemyTurn>d__29::MoveNext()
extern void U3CEnemyTurnU3Ed__29_MoveNext_mAB93ACC2261DA5D3EC53E17CEA2D19436B444BAA (void);
// 0x00000093 System.Object BattleSystem/<EnemyTurn>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnemyTurnU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA0A457697536C41FC21944B39A8DE7CDEE0169D (void);
// 0x00000094 System.Void BattleSystem/<EnemyTurn>d__29::System.Collections.IEnumerator.Reset()
extern void U3CEnemyTurnU3Ed__29_System_Collections_IEnumerator_Reset_m5927CE10D791B5B23B8FA93202C35615F0A9FF70 (void);
// 0x00000095 System.Object BattleSystem/<EnemyTurn>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CEnemyTurnU3Ed__29_System_Collections_IEnumerator_get_Current_mC8AC869BA6F125A9346609BE01FFB022438CC253 (void);
// 0x00000096 System.Void Card::hideCard()
extern void Card_hideCard_m3183E9F5E9F0C97C133BCABDD4A5135EA53680CD (void);
// 0x00000097 System.Void Card::showCard()
extern void Card_showCard_mFA2D37BD5BF2A7B80A4BAB5CE043193FCCB63496 (void);
// 0x00000098 System.Void Card::activateCard()
extern void Card_activateCard_m2AD708BBE6F4C3E9479946F598ACDEDF82A71A76 (void);
// 0x00000099 System.Void Card::deactivateCard()
extern void Card_deactivateCard_mA083E93A51ACF882284935934A65BF8A09C1BC41 (void);
// 0x0000009A System.Void Card::OnMouseDown()
extern void Card_OnMouseDown_m5349CB231B19800A38CF4FC8FFEDFC0ED13127E4 (void);
// 0x0000009B System.Void Card::removeTargets()
extern void Card_removeTargets_mF4C28F9A46BE124751D1C46A71B1B0B93C2F814D (void);
// 0x0000009C System.Void Card::triggerCard(Player,Unit)
extern void Card_triggerCard_m10D85F1493D2F94E9FEBE32B528164C36E717F37 (void);
// 0x0000009D System.Void Card::triggerAttackAmount(Player,Unit,System.Boolean)
extern void Card_triggerAttackAmount_mEBB465927AF112B973D375623AF1213B8D9F4BEE (void);
// 0x0000009E System.Void Card::moveToDiscardPile()
extern void Card_moveToDiscardPile_mADBE0A0112C531BFCF4C54CF6D9AE3E02999653C (void);
// 0x0000009F System.Void Card::cardEffects(Unit,Player)
extern void Card_cardEffects_m87A5DA7394FCB4C5D734C641D518D51D708FA994 (void);
// 0x000000A0 System.Void Card::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Card_OnCollisionEnter2D_mC88C5AE8418E52EAC30319B2741B37A0919DC7BE (void);
// 0x000000A1 System.Void Card::.ctor()
extern void Card__ctor_m2ADE17E90583AE44842C04CD1E69AF1CC03DC8A8 (void);
// 0x000000A2 System.Void Deck::Start()
extern void Deck_Start_m7B9D31952401BE90306BAE90C220CEB34B8C1AAF (void);
// 0x000000A3 System.Void Deck::connectCards()
extern void Deck_connectCards_mEB0644B2E073F9BF827195DB9256DD683410A5C1 (void);
// 0x000000A4 System.Void Deck::hideCards()
extern void Deck_hideCards_mDEDBC9A43D9C4B27600CCF23160C20324BBA1423 (void);
// 0x000000A5 System.Void Deck::showCards()
extern void Deck_showCards_m57CC57CBD91A67C175DF7955AAC0C08D4501F007 (void);
// 0x000000A6 System.Void Deck::activateCards()
extern void Deck_activateCards_mF9D86E37042DD947831A880FFC45F83F2F730AC2 (void);
// 0x000000A7 System.Void Deck::deactivateCards()
extern void Deck_deactivateCards_m84192B707911AC22A783EB5C2C7BCA3FBD22ED5A (void);
// 0x000000A8 System.Void Deck::.ctor()
extern void Deck__ctor_m1699C8F29839AA379EA15AAE8D57B6108C6571CB (void);
// 0x000000A9 System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x000000AA System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x000000AB System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x000000AC EnemyAttack Enemy::chooseAttack()
extern void Enemy_chooseAttack_mD2599DB794C6F7F528B3F815A06D222FE54D5771 (void);
// 0x000000AD System.Int32 Enemy::chooseTargets(EnemyAttack)
extern void Enemy_chooseTargets_m88C95A5802CA2BBCFC68CCCD5743D35E430958A3 (void);
// 0x000000AE System.Void Enemy::activateAttack()
extern void Enemy_activateAttack_m2B596E2935F3C5D6ED29AE3BBD2096430F5CF110 (void);
// 0x000000AF System.Void Enemy::executeAttack(EnemyAttack,System.Int32)
extern void Enemy_executeAttack_mBCAE6DEE051F9FAE245CA0228016D8F7665E8ABF (void);
// 0x000000B0 System.Void EnemyAttack::.ctor(System.String,System.Collections.Generic.List`1<System.Int32[]>,System.Int32)
extern void EnemyAttack__ctor_mCC6A84818778B6DFE004491CB0A7084A927EF65A (void);
// 0x000000B1 System.Void EventManager::Awake()
extern void EventManager_Awake_mF6EAD589D3C0919D6C0E986A3FCE8EAD22F505A3 (void);
// 0x000000B2 System.Void EventManager::debugAnnounce()
extern void EventManager_debugAnnounce_m88E20710E905DAF4A3D588C174487E6F30F7D2A2 (void);
// 0x000000B3 System.Void EventManager::Update()
extern void EventManager_Update_m22642EEC74E5B0FDF52C79044924EA16B2A06205 (void);
// 0x000000B4 System.Void EventManager::.ctor()
extern void EventManager__ctor_mB83B8B21CFF0A302EFEBD4480BD115BE8353C1DB (void);
// 0x000000B5 System.Void EventManager/unitClicked::.ctor(System.Object,System.IntPtr)
extern void unitClicked__ctor_m96AC2F9A9BA6BA1BBE0B36082C1407A8716741E1 (void);
// 0x000000B6 System.Void EventManager/unitClicked::Invoke()
extern void unitClicked_Invoke_m54C0B532206B91B9CFC3E935CB774E8F926DDCF6 (void);
// 0x000000B7 System.IAsyncResult EventManager/unitClicked::BeginInvoke(System.AsyncCallback,System.Object)
extern void unitClicked_BeginInvoke_m23C0DB6865A6151598F011B63F5712C7FFBB7AC9 (void);
// 0x000000B8 System.Void EventManager/unitClicked::EndInvoke(System.IAsyncResult)
extern void unitClicked_EndInvoke_m51A17EE6EBABC9BADE889B78C79F35D4A1E0275D (void);
// 0x000000B9 System.Void EventManager/rightClick::.ctor(System.Object,System.IntPtr)
extern void rightClick__ctor_m15D76D10640A931441E7CCD00A834F492E637423 (void);
// 0x000000BA System.Void EventManager/rightClick::Invoke()
extern void rightClick_Invoke_mE01AEA1283BB5CBFAB40E9F4B1F9E1A3560D104B (void);
// 0x000000BB System.IAsyncResult EventManager/rightClick::BeginInvoke(System.AsyncCallback,System.Object)
extern void rightClick_BeginInvoke_m94FFDFB5B0A6B8AC13920F2001C0E9D525A24105 (void);
// 0x000000BC System.Void EventManager/rightClick::EndInvoke(System.IAsyncResult)
extern void rightClick_EndInvoke_m5B7D21944A31A555D5148CB94E7060779C2E92BF (void);
// 0x000000BD System.Void EventManager/cardTargetChosen::.ctor(System.Object,System.IntPtr)
extern void cardTargetChosen__ctor_m6CE1D8762DFF777590DA0CA76004D6CF06CD2E12 (void);
// 0x000000BE System.Void EventManager/cardTargetChosen::Invoke(Player,Unit)
extern void cardTargetChosen_Invoke_m0161A6EBA93C1944F6F797156EE917D47708AF5E (void);
// 0x000000BF System.IAsyncResult EventManager/cardTargetChosen::BeginInvoke(Player,Unit,System.AsyncCallback,System.Object)
extern void cardTargetChosen_BeginInvoke_m20761EDA24C78AF33CF8C28A6A94176231E71AEC (void);
// 0x000000C0 System.Void EventManager/cardTargetChosen::EndInvoke(System.IAsyncResult)
extern void cardTargetChosen_EndInvoke_m0EB0D6421CCB3A6DF0E621CC2000BC93ACA7EF89 (void);
// 0x000000C1 System.Void EventManager/removeActiveCards::.ctor(System.Object,System.IntPtr)
extern void removeActiveCards__ctor_m4B8F83959687C368C584E30E22F425413227CA03 (void);
// 0x000000C2 System.Void EventManager/removeActiveCards::Invoke(Player)
extern void removeActiveCards_Invoke_mDDB86457B4BCA4AC635519845ED1738DCC51D5A2 (void);
// 0x000000C3 System.IAsyncResult EventManager/removeActiveCards::BeginInvoke(Player,System.AsyncCallback,System.Object)
extern void removeActiveCards_BeginInvoke_mCC8A7DD83F9F3F388D51E58EF896CE81171D1DB5 (void);
// 0x000000C4 System.Void EventManager/removeActiveCards::EndInvoke(System.IAsyncResult)
extern void removeActiveCards_EndInvoke_m1934CC54AD6B18E61B8A73C4CA53DD294EE12900 (void);
// 0x000000C5 System.Void EventManager/aoeTargetDelegator::.ctor(System.Object,System.IntPtr)
extern void aoeTargetDelegator__ctor_m2304B11A813A5D23D7BE726AB034E60C6F082014 (void);
// 0x000000C6 System.Void EventManager/aoeTargetDelegator::Invoke(Unit)
extern void aoeTargetDelegator_Invoke_m12FD99A6A248078962855271FBAD29BD679DB751 (void);
// 0x000000C7 System.IAsyncResult EventManager/aoeTargetDelegator::BeginInvoke(Unit,System.AsyncCallback,System.Object)
extern void aoeTargetDelegator_BeginInvoke_mD4FA9794206EABB410D72671602C850E673F7775 (void);
// 0x000000C8 System.Void EventManager/aoeTargetDelegator::EndInvoke(System.IAsyncResult)
extern void aoeTargetDelegator_EndInvoke_m3E897ECB5C7F65F6ACBDB8D5BFE07BF9E4C5D9E7 (void);
// 0x000000C9 System.Void EventManager/removeAoEHighlight::.ctor(System.Object,System.IntPtr)
extern void removeAoEHighlight__ctor_mB094A641C3AFFF9E72F90F5C89EB56D346EF3961 (void);
// 0x000000CA System.Void EventManager/removeAoEHighlight::Invoke()
extern void removeAoEHighlight_Invoke_m964BAAEA21C2B92027DD2FF3D8786A7C0A472129 (void);
// 0x000000CB System.IAsyncResult EventManager/removeAoEHighlight::BeginInvoke(System.AsyncCallback,System.Object)
extern void removeAoEHighlight_BeginInvoke_m9FF734A42E75B4AA938D9C4954D46FCFB817598A (void);
// 0x000000CC System.Void EventManager/removeAoEHighlight::EndInvoke(System.IAsyncResult)
extern void removeAoEHighlight_EndInvoke_m3272D21F6894086C1A4CE479A749BB968DE8468D (void);
// 0x000000CD System.Void EventManager/attackAnnouncer::.ctor(System.Object,System.IntPtr)
extern void attackAnnouncer__ctor_m60FC6F2C27F9863BC21D62C602D74550CC291453 (void);
// 0x000000CE System.Void EventManager/attackAnnouncer::Invoke(System.String,System.String)
extern void attackAnnouncer_Invoke_m16E4C634668EF32FDB837E5F9417302153B1143F (void);
// 0x000000CF System.IAsyncResult EventManager/attackAnnouncer::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void attackAnnouncer_BeginInvoke_m1C56D89509786B78B958BC7291D078F7ADCC50EE (void);
// 0x000000D0 System.Void EventManager/attackAnnouncer::EndInvoke(System.IAsyncResult)
extern void attackAnnouncer_EndInvoke_m23D2B1B1B47A2405B8FB0D0B70ACF33A6CA05956 (void);
// 0x000000D1 System.Void EventManager/missAnnouncer::.ctor(System.Object,System.IntPtr)
extern void missAnnouncer__ctor_mBA9A21B92273A709016BB9D22CEC45140D647C8D (void);
// 0x000000D2 System.Void EventManager/missAnnouncer::Invoke(System.String,System.String)
extern void missAnnouncer_Invoke_m2CE32D3A641BC82D022040D220BFAA08AF0148D1 (void);
// 0x000000D3 System.IAsyncResult EventManager/missAnnouncer::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void missAnnouncer_BeginInvoke_mF03761B8AB200FA33D6A3910B54A48D2ABBDEA3F (void);
// 0x000000D4 System.Void EventManager/missAnnouncer::EndInvoke(System.IAsyncResult)
extern void missAnnouncer_EndInvoke_m3CDFC0EC3F5C97F2723F557844DC51CF3060AE26 (void);
// 0x000000D5 System.Void EventManager/turnEnd::.ctor(System.Object,System.IntPtr)
extern void turnEnd__ctor_m70C7A4429C3F878952EBB18EB4AFCD8D4E537C99 (void);
// 0x000000D6 System.Void EventManager/turnEnd::Invoke(Unit)
extern void turnEnd_Invoke_mCC5922C8A8DD4349B031DB1FBA1317B1810D44BC (void);
// 0x000000D7 System.IAsyncResult EventManager/turnEnd::BeginInvoke(Unit,System.AsyncCallback,System.Object)
extern void turnEnd_BeginInvoke_m0C89D9155A4C2ACCF0792820D19FF02C2D064853 (void);
// 0x000000D8 System.Void EventManager/turnEnd::EndInvoke(System.IAsyncResult)
extern void turnEnd_EndInvoke_mF5A9B5AB2494C1F93BB915C894E3CA563F360CA7 (void);
// 0x000000D9 System.Void EventManager/turnStart::.ctor(System.Object,System.IntPtr)
extern void turnStart__ctor_m2B64D5CF764CD265244B20A377BB995C05F32004 (void);
// 0x000000DA System.Void EventManager/turnStart::Invoke(Unit)
extern void turnStart_Invoke_m5ADF89380CED582E811C63F2FB3B8E62E629E9F9 (void);
// 0x000000DB System.IAsyncResult EventManager/turnStart::BeginInvoke(Unit,System.AsyncCallback,System.Object)
extern void turnStart_BeginInvoke_mBB43CA9B0BD0C050D85D368591496874CFFD6279 (void);
// 0x000000DC System.Void EventManager/turnStart::EndInvoke(System.IAsyncResult)
extern void turnStart_EndInvoke_m038FBF8EDB0B48C1C35BB3E4BF405658AC58F09D (void);
// 0x000000DD System.Void EventManager/unitDeath::.ctor(System.Object,System.IntPtr)
extern void unitDeath__ctor_m4E3A8B65452C207278BCC7B7C12E7299EEE3F166 (void);
// 0x000000DE System.Void EventManager/unitDeath::Invoke(Unit)
extern void unitDeath_Invoke_m4337595F5C9BF70D8D67EDB9B7FD5D0EFB72E91B (void);
// 0x000000DF System.IAsyncResult EventManager/unitDeath::BeginInvoke(Unit,System.AsyncCallback,System.Object)
extern void unitDeath_BeginInvoke_mEFCF979FF9D1A6BF9EA1D641C840B790827C9B29 (void);
// 0x000000E0 System.Void EventManager/unitDeath::EndInvoke(System.IAsyncResult)
extern void unitDeath_EndInvoke_mACBB157C09A47639DB4E71AF7F44304091B562C9 (void);
// 0x000000E1 System.Void EventManager/unitDamaged::.ctor(System.Object,System.IntPtr)
extern void unitDamaged__ctor_m4952E480B9316DB74144FF6FE5F71F370C885C3F (void);
// 0x000000E2 System.Void EventManager/unitDamaged::Invoke(Unit)
extern void unitDamaged_Invoke_mD00947B0B05D291C9E87157F7C1D369372291C5B (void);
// 0x000000E3 System.IAsyncResult EventManager/unitDamaged::BeginInvoke(Unit,System.AsyncCallback,System.Object)
extern void unitDamaged_BeginInvoke_mA8AE029035EC4C75A4C99D788AF64B7075DEC992 (void);
// 0x000000E4 System.Void EventManager/unitDamaged::EndInvoke(System.IAsyncResult)
extern void unitDamaged_EndInvoke_m3543E413AF769E3B2475A3ED48556FE0FC9EDB60 (void);
// 0x000000E5 System.Void EventManager/unitAttacked::.ctor(System.Object,System.IntPtr)
extern void unitAttacked__ctor_m7E11AF5C90B4D2301DE0812E849329F090792DB4 (void);
// 0x000000E6 System.Void EventManager/unitAttacked::Invoke(Unit)
extern void unitAttacked_Invoke_m8AB0D83582AE74CCCFF72D445D0442BB0DFA19AB (void);
// 0x000000E7 System.IAsyncResult EventManager/unitAttacked::BeginInvoke(Unit,System.AsyncCallback,System.Object)
extern void unitAttacked_BeginInvoke_m626DE48C5CC23E732FF8CB97BCD7DA243D4F339C (void);
// 0x000000E8 System.Void EventManager/unitAttacked::EndInvoke(System.IAsyncResult)
extern void unitAttacked_EndInvoke_m6A05902D0A2ABCA05E1E2B33CB2112432647AD86 (void);
// 0x000000E9 System.Void EventManager/testEventManagerUp::.ctor(System.Object,System.IntPtr)
extern void testEventManagerUp__ctor_m8B7C89D5C34DC1C10C26B3E325685201546F4173 (void);
// 0x000000EA System.Void EventManager/testEventManagerUp::Invoke()
extern void testEventManagerUp_Invoke_mB861D5D7F7494293E7BE7D59C5E6363496DA100F (void);
// 0x000000EB System.IAsyncResult EventManager/testEventManagerUp::BeginInvoke(System.AsyncCallback,System.Object)
extern void testEventManagerUp_BeginInvoke_mB8CBCD3C40B60BC40CFC1E96A0280745980C6BDF (void);
// 0x000000EC System.Void EventManager/testEventManagerUp::EndInvoke(System.IAsyncResult)
extern void testEventManagerUp_EndInvoke_mF2179510BCC4F9D91C581E41DFBC8E4DCB46D2E1 (void);
// 0x000000ED System.Void GameManager::setRandomSeed()
extern void GameManager_setRandomSeed_mCDDF6D1EAD3A8EE224D5701EF6B720483A44F85A (void);
// 0x000000EE System.Boolean GameManager::DetermineTargetHit(Unit,Unit)
extern void GameManager_DetermineTargetHit_mA9BF8791D1523662DCF5925EAE07468A5296448A (void);
// 0x000000EF System.Void GameManager::deactivateCard()
extern void GameManager_deactivateCard_m398C5468438F480C07D04E612B04AC572ED4E180 (void);
// 0x000000F0 System.Void GameManager::activateCard()
extern void GameManager_activateCard_m317CFFBF94629561163BCDE6497045FCC473A078 (void);
// 0x000000F1 Unit GameManager::getUnitFromPositionNumber(System.Int32)
extern void GameManager_getUnitFromPositionNumber_m4DA60FF9A5C40F4051B57872A61D4C472E491470 (void);
// 0x000000F2 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x000000F3 System.Void GameManager::.cctor()
extern void GameManager__cctor_m0872BB5B2E20B9183F004EB81D5129608114D285 (void);
// 0x000000F4 System.Void KeepOnRefresh::Start()
extern void KeepOnRefresh_Start_m3CC0FC7B054EE0AFD8B1AC3F1AEBF7260DAB82C0 (void);
// 0x000000F5 System.Void KeepOnRefresh::Update()
extern void KeepOnRefresh_Update_m820970809AF8CF76F677440B0A8E0282561734E1 (void);
// 0x000000F6 System.Void KeepOnRefresh::.ctor()
extern void KeepOnRefresh__ctor_mD209A00BA9FA514A05D2AACA47EA541FD486D805 (void);
// 0x000000F7 System.Void Accuracy::Start()
extern void Accuracy_Start_m980DB6F8331779DEAB8AF58F240C87878299FE59 (void);
// 0x000000F8 System.Void Accuracy::ActivatePersistant(Unit,System.Int32,System.Int32)
extern void Accuracy_ActivatePersistant_mB1E5F5E835689FA2F16B1C6D39BA13BD479DB0F6 (void);
// 0x000000F9 System.Void Accuracy::RemovePersistant(Unit)
extern void Accuracy_RemovePersistant_mC67CD70C344B200C4FA1779C6960D293DB989182 (void);
// 0x000000FA System.Void Accuracy::.ctor()
extern void Accuracy__ctor_m6AF036E76A3BC828771EEB0788E46C1C9129E9CC (void);
// 0x000000FB System.Void Armor::Start()
extern void Armor_Start_mB3C650FB54B35199D41EBF11F43D712EC553C8B3 (void);
// 0x000000FC System.Void Armor::ActivatePersistant(Unit,System.Int32,System.Int32)
extern void Armor_ActivatePersistant_m86540ED4CC12E8B7F6F83FBC166D00AD89331A95 (void);
// 0x000000FD System.Void Armor::RemovePersistant(Unit)
extern void Armor_RemovePersistant_m351E7AE550A71D93CFC41E15C9D945D0CEE0E3C4 (void);
// 0x000000FE System.Void Armor::.ctor()
extern void Armor__ctor_m6954859265EA92F2E34B645D91155382CCA068F2 (void);
// 0x000000FF System.Void Bleed::Start()
extern void Bleed_Start_m66A521E4C41A0258B60889132D0F82DFB8EF36F1 (void);
// 0x00000100 System.Void Bleed::TriggerPersistant(Unit)
extern void Bleed_TriggerPersistant_m80CE9648BEE9216A3238BEA89B94F9BDC994A987 (void);
// 0x00000101 System.Void Bleed::ActivatePersistant(Unit,System.Int32,System.Int32)
extern void Bleed_ActivatePersistant_mBCE5B5FC43D9C108B21296984419DA1B8956D7AC (void);
// 0x00000102 System.Void Bleed::RemovePersistant(Unit)
extern void Bleed_RemovePersistant_mFCED2B9EAD5F76600B9FF042D4547F8095A8718A (void);
// 0x00000103 System.Void Bleed::.ctor()
extern void Bleed__ctor_m70498D5457E1C51E0283C23656C79DD4045BBF35 (void);
// 0x00000104 System.Void Damage::Start()
extern void Damage_Start_mB344528E3D9EA1C7EB0F036AE5335456FB830B83 (void);
// 0x00000105 System.Void Damage::ActivatePersistant(Unit,System.Int32,System.Int32)
extern void Damage_ActivatePersistant_mDA286B08660076981A6426F0BC7DF2D2EA7F5CF8 (void);
// 0x00000106 System.Void Damage::RemovePersistant(Unit)
extern void Damage_RemovePersistant_m1C9E7911266D312047D604395C14027B2B4B0176 (void);
// 0x00000107 System.Void Damage::.ctor()
extern void Damage__ctor_m448736D790344E455BBE20282D3D73C91E6E2CAB (void);
// 0x00000108 System.Void Dodge::Start()
extern void Dodge_Start_mD051BAAE091535AD3F0D41FAB28C371524FF35C1 (void);
// 0x00000109 System.Void Dodge::ActivatePersistant(Unit,System.Int32,System.Int32)
extern void Dodge_ActivatePersistant_m968F718425BD2411A5DBB5B6DC1237D923D0F6AC (void);
// 0x0000010A System.Void Dodge::RemovePersistant(Unit)
extern void Dodge_RemovePersistant_m4C1AC66001511642C403227AE830825BB77E854F (void);
// 0x0000010B System.Void Dodge::.ctor()
extern void Dodge__ctor_mF2B67DAAE4FDA0BDC7962837BE577374DA570E52 (void);
// 0x0000010C System.Void Persistant::TriggerPersistant(Unit)
extern void Persistant_TriggerPersistant_m87D7DE610DB3DDCF82F1C64A87B8DDB33B9D776A (void);
// 0x0000010D System.Void Persistant::UpdatePersistantVariables(Unit)
extern void Persistant_UpdatePersistantVariables_m162BA3B2674CA53E51B64BBFA3A1BE2D09657A1B (void);
// 0x0000010E System.Void Persistant::TickPersistant(Unit)
extern void Persistant_TickPersistant_mEA1FC3442EAD7CEEB4D895E53D5ED00D00A49206 (void);
// 0x0000010F System.Void Persistant::RemovePersistant(Unit)
extern void Persistant_RemovePersistant_m96275C3A935F13AA2BBF252F3990C4B2B7996B01 (void);
// 0x00000110 System.Void Persistant::ActivatePersistant(Unit,System.Int32)
extern void Persistant_ActivatePersistant_mF22508DD9A91665863527C12F0B7D4A2513108B4 (void);
// 0x00000111 System.Void Persistant::AddTriggersToEventListener()
extern void Persistant_AddTriggersToEventListener_m2303DA75FFAE198E05A6B1A258FC602092C0A083 (void);
// 0x00000112 System.Void Persistant::RemoveTriggersFromEventListener()
extern void Persistant_RemoveTriggersFromEventListener_m92DFC7B10912C321AF6C1EDD4B524AA884E9DCD3 (void);
// 0x00000113 System.Void Persistant::.ctor()
extern void Persistant__ctor_m0CF8628709B0A20163A83C4D7392BC36A9FE2204 (void);
// 0x00000114 System.Void RagePersistant::Start()
extern void RagePersistant_Start_m912E0B8EEA5C8FF907E28815AD6470EF351CB16C (void);
// 0x00000115 System.Void RagePersistant::TriggerPersistant(Unit)
extern void RagePersistant_TriggerPersistant_m86DC1B86C7B23B1285669FC4AEDC1A785D86AB75 (void);
// 0x00000116 System.Void RagePersistant::ActivatePersistant(Unit,System.Int32)
extern void RagePersistant_ActivatePersistant_m91D0EA347B93714CA3639CFFFDE41C4DE3EE2280 (void);
// 0x00000117 System.Void RagePersistant::RemovePersistant(Unit)
extern void RagePersistant_RemovePersistant_mCCF10921348C08B31F856F543BF356CF11D80137 (void);
// 0x00000118 System.Single RagePersistant::CalculateRageDamage()
extern void RagePersistant_CalculateRageDamage_m134B8EC75F16535963F1657D9AE824878A6C2A84 (void);
// 0x00000119 System.Void RagePersistant::.ctor()
extern void RagePersistant__ctor_mF61146DA59B48C282DEC16BEB3DB1CEA46B19535 (void);
// 0x0000011A System.Void Player::Start()
extern void Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021 (void);
// 0x0000011B System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x0000011C System.Void Player::Update()
extern void Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3 (void);
// 0x0000011D System.Void Player::DrawCard()
extern void Player_DrawCard_m3FAB10BA5D373B68508C358C7ADC45DEFAAA2FD2 (void);
// 0x0000011E System.Void Player::deactivateDeck()
extern void Player_deactivateDeck_m406D4B60C3E58DD2AA93B71185CE5BE9EAACB46C (void);
// 0x0000011F System.Void Player::triggerAttack(System.Int32,System.String)
extern void Player_triggerAttack_m0A13A039E2CDB3A2BADE169EDA2CC43E6EFD7E29 (void);
// 0x00000120 System.Void Player::shuffleDeck()
extern void Player_shuffleDeck_mB4D7A514BFEFAB45266460CC823F16607B1BE7EB (void);
// 0x00000121 System.Void Player::focusHud()
extern void Player_focusHud_mD6D16FE52CC013F13F99491FCCD817B729D25723 (void);
// 0x00000122 System.Void BigSwig::Start()
extern void BigSwig_Start_mA249D3F93A85A59A3E120C85B3E4AD75D43C738B (void);
// 0x00000123 System.Void BigSwig::cardEffects(Unit,Player)
extern void BigSwig_cardEffects_m22B0DD66517B8F2BC9A7DEB246A112F769834C5A (void);
// 0x00000124 System.Void BigSwig::.ctor()
extern void BigSwig__ctor_m1621F3425CF563FC0427BDB634BB29B32CD8F5C2 (void);
// 0x00000125 System.Void Cleave::Start()
extern void Cleave_Start_mE82AB6289AF7D3DEA3FFA5E0658E90B0716E9749 (void);
// 0x00000126 System.Void Cleave::cardEffects(Unit,Player)
extern void Cleave_cardEffects_mAE19D8F3F642B2CB30F9B4F99C081EA9929EBF9B (void);
// 0x00000127 System.Void Cleave::.ctor()
extern void Cleave__ctor_mEE4B648F9BDD4EB94E2420C1BC33186096B73D6C (void);
// 0x00000128 System.Void Dismember::Start()
extern void Dismember_Start_m9F662B07903ED8D4BEDCA60EB734E93CEB7DFE86 (void);
// 0x00000129 System.Void Dismember::cardEffects(Unit,Player)
extern void Dismember_cardEffects_mF3D1B46A3545F5AAC376C1C2E192A7F54ED18262 (void);
// 0x0000012A System.Void Dismember::.ctor()
extern void Dismember__ctor_m087600F22EC648FAD6B65A357ACF7BCE265682B2 (void);
// 0x0000012B System.Void Howl::Start()
extern void Howl_Start_mB52BFE8C4298013D6675372D9E41DE7CEB09CD65 (void);
// 0x0000012C System.Void Howl::cardEffects(Unit,Player)
extern void Howl_cardEffects_m84454E9568C12C2B8B77BF5B3859214459FCE62D (void);
// 0x0000012D System.Void Howl::.ctor()
extern void Howl__ctor_m2853FEA40FAAD55679DDDAA8728A28F06405E3D5 (void);
// 0x0000012E System.Void RAGE::Start()
extern void RAGE_Start_mDC5B3040712F63FCF9B9053FAD38B5ED3667152D (void);
// 0x0000012F System.Void RAGE::cardEffects(Unit,Player)
extern void RAGE_cardEffects_m986C6C4CB8B8FF6A9FB796F543D855B94E326E29 (void);
// 0x00000130 System.Void RAGE::.ctor()
extern void RAGE__ctor_m64C1CE298C5408C728767A4F637EB38730E16291 (void);
// 0x00000131 System.Void REND::Start()
extern void REND_Start_mB15D5C99BE852DCE4590A3636F80BDEF012367AF (void);
// 0x00000132 System.Void REND::cardEffects(Unit,Player)
extern void REND_cardEffects_mC5382C11C45A007E9EB95876E3C06C48434BEFC6 (void);
// 0x00000133 System.Void REND::.ctor()
extern void REND__ctor_m63722DC92ADBD4B076F0A71FC25C25084D4C01E1 (void);
// 0x00000134 System.Void RecklessCharge::Start()
extern void RecklessCharge_Start_m1FD0B3ABEAEF164E6AE5919D05922FB55FC4BB6B (void);
// 0x00000135 System.Void RecklessCharge::cardEffects(Unit,Player)
extern void RecklessCharge_cardEffects_mE3022E9E1153E3A429B1A782315A813BF0036502 (void);
// 0x00000136 System.Void RecklessCharge::.ctor()
extern void RecklessCharge__ctor_m93C131BC5BEB25593E25BE6AB35CAB5D0A359138 (void);
// 0x00000137 System.Void SeeingRed::Start()
extern void SeeingRed_Start_m21B738DA3DD24F2CB04E70AD41F7153F8AF5ECD3 (void);
// 0x00000138 System.Void SeeingRed::cardEffects(Unit,Player)
extern void SeeingRed_cardEffects_mF6AF3B5F390BCA74E67F9556CEBD86B272DD0252 (void);
// 0x00000139 System.Void SeeingRed::.ctor()
extern void SeeingRed__ctor_mDA98AE220B48D57E408524EA89664377DF938DED (void);
// 0x0000013A System.Void ToughSkin::Start()
extern void ToughSkin_Start_mB4CA25B756E8E53CA51DF68E549E89E8B2684471 (void);
// 0x0000013B System.Void ToughSkin::cardEffects(Unit,Player)
extern void ToughSkin_cardEffects_m6562D95C322949BA99CDA6C01080805AAE3420C8 (void);
// 0x0000013C System.Void ToughSkin::.ctor()
extern void ToughSkin__ctor_mF732AE6F1DCCC796EFA6277020FB02909D4BBDB6 (void);
// 0x0000013D System.Void WarlordsTrophy::Start()
extern void WarlordsTrophy_Start_m7A1DD505F89A046B21EDF69B6698DFE8F8BB5E91 (void);
// 0x0000013E System.Void WarlordsTrophy::cardEffects(Unit,Player)
extern void WarlordsTrophy_cardEffects_m132A1C078D004EBBF23BA7D10E80A489D8102BA6 (void);
// 0x0000013F System.Void WarlordsTrophy::triggerFear(Unit)
extern void WarlordsTrophy_triggerFear_m6A5087A672780A265A9EBA4255A6C9F0B9B29453 (void);
// 0x00000140 System.Void WarlordsTrophy::.ctor()
extern void WarlordsTrophy__ctor_mBE0F8C80564BC282B67C5395B0316BD5D5CF4A0E (void);
// 0x00000141 System.Void GazelleStance::Start()
extern void GazelleStance_Start_mA5BC8761F7DFF3900646D1364FF0BE5765A22111 (void);
// 0x00000142 System.Void GazelleStance::cardEffects(Unit,Player)
extern void GazelleStance_cardEffects_mD7B30CC653AD07110373834C07F7C2C2BABE4C47 (void);
// 0x00000143 System.Void GazelleStance::.ctor()
extern void GazelleStance__ctor_mB59A77C3CD18B11BCAD4C18F3CE3876FCAC8127F (void);
// 0x00000144 System.Void MartialExecution::Start()
extern void MartialExecution_Start_m121908A84733B3491426BB9614B8BBBE5D936C18 (void);
// 0x00000145 System.Void MartialExecution::cardEffects(Unit,Player)
extern void MartialExecution_cardEffects_m0D0AE7E5EBAA6B0006C5B007CC6EAE2F35CED03E (void);
// 0x00000146 System.Void MartialExecution::.ctor()
extern void MartialExecution__ctor_m21BA61DC7E68C92BFF36774671BF16B1B8EB1045 (void);
// 0x00000147 System.Void MastersTempest::Start()
extern void MastersTempest_Start_m09BF75F74D3C1A63AA3479CD92A4026B454FBB92 (void);
// 0x00000148 System.Void MastersTempest::cardEffects(Unit,Player)
extern void MastersTempest_cardEffects_m01394A739B56DD882A31F0A4613A98DD8EADB2D0 (void);
// 0x00000149 System.Void MastersTempest::.ctor()
extern void MastersTempest__ctor_mF8ED5CA071A4BAE65D94F9FE3B9EA78F51AB7DF5 (void);
// 0x0000014A System.Void PantherStance::Start()
extern void PantherStance_Start_mB2F5A54200A73534D55CD11B067951CFF09E56A1 (void);
// 0x0000014B System.Void PantherStance::cardEffects(Unit,Player)
extern void PantherStance_cardEffects_m37C2256E26EE2F650A6618F11696B58D8CC76477 (void);
// 0x0000014C System.Void PantherStance::.ctor()
extern void PantherStance__ctor_mC407C6A4FDD370AF44599BB5C90BC3038876A75C (void);
// 0x0000014D System.Void RhinoStance::Start()
extern void RhinoStance_Start_m6CB4E3B68ACD46E62F2C7C13ABC1CD70208A024A (void);
// 0x0000014E System.Void RhinoStance::cardEffects(Unit,Player)
extern void RhinoStance_cardEffects_m698A9CE38DC93178A72E62B65CF7B63736948CA8 (void);
// 0x0000014F System.Void RhinoStance::.ctor()
extern void RhinoStance__ctor_mCDE6A671B5801CECEF2A057967E38541A3151F51 (void);
// 0x00000150 System.Void SliceAndDice::Start()
extern void SliceAndDice_Start_m3293CDAE7020E79FDEBE32802FF672A621AA5FDB (void);
// 0x00000151 System.Void SliceAndDice::cardEffects(Unit,Player)
extern void SliceAndDice_cardEffects_m0208FB8B87DEBB4CB6FD3E63D1DE155D0344DEB7 (void);
// 0x00000152 System.Void SliceAndDice::.ctor()
extern void SliceAndDice__ctor_m2D98D107562FFBA9CF23F2097154C4925DB22F65 (void);
// 0x00000153 System.Void FireBolt::Start()
extern void FireBolt_Start_mDEE3A446C55F5C353545DA7D83A67CC2AE8EF214 (void);
// 0x00000154 System.Void FireBolt::cardEffects(Unit,Player)
extern void FireBolt_cardEffects_mF3A4BA88CA0755E8EAA2F22DFA1A4AAB6AE4097F (void);
// 0x00000155 System.Void FireBolt::.ctor()
extern void FireBolt__ctor_m220D03E9D60745FE784AFA204FBA548A46B5FF5D (void);
// 0x00000156 System.Void FireStorm::Start()
extern void FireStorm_Start_m13C854762EA3DCFC194A69E5F8463C171575513F (void);
// 0x00000157 System.Void FireStorm::cardEffects(Unit,Player)
extern void FireStorm_cardEffects_mA3BCB1BEEEE75F2192B0D80E9836065F23B78BA4 (void);
// 0x00000158 System.Void FireStorm::.ctor()
extern void FireStorm__ctor_m28DA58D9C334DCF2F14B9FB2D554D67359B03EBB (void);
// 0x00000159 System.Void HeatMetal::Start()
extern void HeatMetal_Start_m29ED4D154B3709646E1200064DCC452531BC5832 (void);
// 0x0000015A System.Void HeatMetal::cardEffects(Unit,Player)
extern void HeatMetal_cardEffects_m27A69B6BED71D0E3E5B88DACC704D6FC71757202 (void);
// 0x0000015B System.Void HeatMetal::.ctor()
extern void HeatMetal__ctor_m92429EFFE968B26E116132B9C84735BFF07F2A51 (void);
// 0x0000015C System.Void IgniteWeapon::Start()
extern void IgniteWeapon_Start_m6C4D1DAD4C78B95F5CD8C07F55CF9B9766FDCF84 (void);
// 0x0000015D System.Void IgniteWeapon::cardEffects(Unit,Player)
extern void IgniteWeapon_cardEffects_mD3A34907DBDF92B44677BFFD7C91BB6C4BBC820C (void);
// 0x0000015E System.Void IgniteWeapon::.ctor()
extern void IgniteWeapon__ctor_m9BD3AE45D0C83348E91086434E3A78E0167B7194 (void);
// 0x0000015F System.Void LavaSlap::Start()
extern void LavaSlap_Start_mAA2FD19DFF2716735442BAD3FDA701EC22CC3FD4 (void);
// 0x00000160 System.Void LavaSlap::cardEffects(Unit,Player)
extern void LavaSlap_cardEffects_m49BFDE25BC64420EA459D6B4C152DF420AE279FD (void);
// 0x00000161 System.Void LavaSlap::.ctor()
extern void LavaSlap__ctor_mE55BD546804423207DB9935DC3FECEC77604D866 (void);
// 0x00000162 System.Void SolarFlare::Start()
extern void SolarFlare_Start_m0990E65B0EEF579EEA4726994A969B43011AD74B (void);
// 0x00000163 System.Void SolarFlare::cardEffects(Unit,Player)
extern void SolarFlare_cardEffects_m299578CD83A2803E26B41421030E2A186559FA6C (void);
// 0x00000164 System.Void SolarFlare::.ctor()
extern void SolarFlare__ctor_m3934657FD112BF7CD4427B6C2CD41CE07F434CFA (void);
// 0x00000165 System.Void Shank::Start()
extern void Shank_Start_m4EEBA17CD5D0583215354198D5DEFC8F33B7DA1A (void);
// 0x00000166 System.Void Shank::cardEffects(Unit,Player)
extern void Shank_cardEffects_mE3127F6B7C432916AE4419A27631C0F557174B75 (void);
// 0x00000167 System.Void Shank::.ctor()
extern void Shank__ctor_m7DE0C877792487F902809DE828F80467D0443349 (void);
// 0x00000168 System.Void DivineIntervention::Start()
extern void DivineIntervention_Start_m65E9DA62AB5E7832D7255D4D2F2F52F2F2112E8F (void);
// 0x00000169 System.Void DivineIntervention::cardEffects(Unit,Player)
extern void DivineIntervention_cardEffects_mF805EA84FA988F573F022AEB59811906CEC3A520 (void);
// 0x0000016A System.Void DivineIntervention::.ctor()
extern void DivineIntervention__ctor_mC9BFEAE22F57E2B31A9AA1FE85FA5DE8CC2E68C7 (void);
// 0x0000016B System.Void FlashOfLight::Start()
extern void FlashOfLight_Start_m30043C184483DA91F3DDB2C723E07F59C4C297E7 (void);
// 0x0000016C System.Void FlashOfLight::cardEffects(Unit,Player)
extern void FlashOfLight_cardEffects_mF7D969E329081ADB7519107EE1B96D979769AD59 (void);
// 0x0000016D System.Void FlashOfLight::.ctor()
extern void FlashOfLight__ctor_m180ABE1C825B533561A3C96CFC863EA21E3686E8 (void);
// 0x0000016E System.Void Glory::Start()
extern void Glory_Start_m9232DB54E653C4C8510DF9BDA38FE4C9A9A42722 (void);
// 0x0000016F System.Void Glory::cardEffects(Unit,Player)
extern void Glory_cardEffects_m4906C31F79209467462E4BA7C59CD6DBE8765EDB (void);
// 0x00000170 System.Void Glory::.ctor()
extern void Glory__ctor_mE78959EDD77FC254F6598826826B19C4709E0FB7 (void);
// 0x00000171 System.Void Heal::Start()
extern void Heal_Start_mFED609782FD3613899C578916ABDD7343816EE93 (void);
// 0x00000172 System.Void Heal::cardEffects(Unit,Player)
extern void Heal_cardEffects_mB3E34C998CE3A8384DCAA7A31B2F1DFEE40358BE (void);
// 0x00000173 System.Void Heal::.ctor()
extern void Heal__ctor_m34EE99E03F61CA1326B62D8007FDF4E89A75FC8F (void);
// 0x00000174 System.Void HolyGround::Start()
extern void HolyGround_Start_m3C7B943FBE424F29B9DB977C0358D7182CD6D1BE (void);
// 0x00000175 System.Void HolyGround::cardEffects(Unit,Player)
extern void HolyGround_cardEffects_mBA51CFC7B2899FCD929CD99ECA99B7D5FB162C5E (void);
// 0x00000176 System.Void HolyGround::.ctor()
extern void HolyGround__ctor_m60FD172EE782F2544DF4C1C7FB746BFB0CE81D14 (void);
// 0x00000177 System.Void HolyStaff::Start()
extern void HolyStaff_Start_mBD7547CB072716F9D0DDF55DD53478713CAB185E (void);
// 0x00000178 System.Void HolyStaff::cardEffects(Unit,Player)
extern void HolyStaff_cardEffects_mEFD33E3EA8F412293117D72C0226BDFE777B6952 (void);
// 0x00000179 System.Void HolyStaff::.ctor()
extern void HolyStaff__ctor_mAB1892BD491E2E5C36494805DCBD166D52953AE7 (void);
// 0x0000017A System.Void Smite::Start()
extern void Smite_Start_m6E9ABB2A6A191AD6626E42A48FD75B84FDA9E5EA (void);
// 0x0000017B System.Void Smite::cardEffects(Unit,Player)
extern void Smite_cardEffects_mDD97384BC6CBB8111E72230A120CBD2459AEACC4 (void);
// 0x0000017C System.Void Smite::.ctor()
extern void Smite__ctor_mF54DC9BCC2695B14C60F1530F0731079EAB6505D (void);
// 0x0000017D System.Void BringerOfDeathEnemy::Awake()
extern void BringerOfDeathEnemy_Awake_m9A3DAFADA1B6696B009378043D4279F04F71476A (void);
// 0x0000017E System.Void BringerOfDeathEnemy::Start()
extern void BringerOfDeathEnemy_Start_m51CC5344D4221F8DA190433C307008EEEC107F1E (void);
// 0x0000017F System.Void BringerOfDeathEnemy::executeAttack(EnemyAttack,System.Int32)
extern void BringerOfDeathEnemy_executeAttack_m4905EECF5FAA0CAA0B12325EA2B533ED42FE5C1D (void);
// 0x00000180 System.Void BringerOfDeathEnemy::unitAttackAnimation(System.Int32)
extern void BringerOfDeathEnemy_unitAttackAnimation_m09105C0B1CC1713F11CCE6F12BB07421DC4A2995 (void);
// 0x00000181 System.Void BringerOfDeathEnemy::.ctor()
extern void BringerOfDeathEnemy__ctor_mE070E625F83E9FEB4D5C94E8A19F8F0B32B5A30F (void);
// 0x00000182 System.Void NecromancerEnemy::Awake()
extern void NecromancerEnemy_Awake_m0E75F8BF5D40DEFD546A985FD1DBE37B6BC8F510 (void);
// 0x00000183 System.Void NecromancerEnemy::executeAttack(EnemyAttack,System.Int32)
extern void NecromancerEnemy_executeAttack_m941168B849A0E1132E82D8C3F0D3331248EEB8ED (void);
// 0x00000184 System.Void NecromancerEnemy::unitAttackAnimation(System.Int32)
extern void NecromancerEnemy_unitAttackAnimation_m29EC32A09C3F74BFB7698DDE1BFB58ED5910B0BB (void);
// 0x00000185 System.Void NecromancerEnemy::.ctor()
extern void NecromancerEnemy__ctor_m0725BD84F9533ABF0B36127606516675E3145267 (void);
// 0x00000186 System.Void RogueEnemy::Awake()
extern void RogueEnemy_Awake_m1DF96D37383FBCDB2EF0C3725283B7A9A7C4E843 (void);
// 0x00000187 System.Void RogueEnemy::executeAttack(EnemyAttack,System.Int32)
extern void RogueEnemy_executeAttack_m5A585EC53DCE48812BAE68C36D2F02040DCF19B2 (void);
// 0x00000188 System.Void RogueEnemy::unitAttackAnimation(System.Int32)
extern void RogueEnemy_unitAttackAnimation_mFEAA6F71367C52D424FFB068A7048EBEEAB447E5 (void);
// 0x00000189 System.Void RogueEnemy::.ctor()
extern void RogueEnemy__ctor_m453E9D9430FF0B20731473626D94011D8D3CB81D (void);
// 0x0000018A System.Void WarlockEnemy::Awake()
extern void WarlockEnemy_Awake_mAAC269CD97ED1ABAF7FC1501B3EF0381B2810F1D (void);
// 0x0000018B System.Void WarlockEnemy::executeAttack(EnemyAttack,System.Int32)
extern void WarlockEnemy_executeAttack_m2597DB13645EA75C81D6AD3D370DF45FA6C5F933 (void);
// 0x0000018C System.Void WarlockEnemy::unitAttackAnimation(System.Int32)
extern void WarlockEnemy_unitAttackAnimation_m47960705E9ACE6A6B9D05A200E5F8CB82301ECCA (void);
// 0x0000018D System.Void WarlockEnemy::.ctor()
extern void WarlockEnemy__ctor_m3BFEC86F391E623614F0F8AF1D2936A0AA0A7F1D (void);
// 0x0000018E System.Void FireMage::.ctor()
extern void FireMage__ctor_m1C43B75FDF47B4B1163A6B1C1A5935C59F72D77E (void);
// 0x0000018F System.Void FireMage::Start()
extern void FireMage_Start_m45005B1F9B63A4A5BD9DE757487ACCB5F7489018 (void);
// 0x00000190 System.Void FireMage::unitAttackAnimation(System.Int32)
extern void FireMage_unitAttackAnimation_m391BF201FEF2DBDCF578FE90F7F22400DE9B55CD (void);
// 0x00000191 System.Void BarbarianPlayer::Awake()
extern void BarbarianPlayer_Awake_m6975C01899526BF9FFAB6CDE993DA2742245CDAD (void);
// 0x00000192 System.Void BarbarianPlayer::unitAttackAnimation(System.Int32)
extern void BarbarianPlayer_unitAttackAnimation_m223CEC08448C3BBD8318A122344FBBBC41B4C9B6 (void);
// 0x00000193 System.Void BarbarianPlayer::.ctor()
extern void BarbarianPlayer__ctor_m14E5B4833C7717D33A8596690863A57B5703BF16 (void);
// 0x00000194 System.Void BlademasterPlayer::Awake()
extern void BlademasterPlayer_Awake_mDEEABAE528BA26907A2932387CA67082C19F8A19 (void);
// 0x00000195 System.Void BlademasterPlayer::unitAttackAnimation(System.Int32)
extern void BlademasterPlayer_unitAttackAnimation_m5345CC1CDC81943335D2A75E49284D3B41722224 (void);
// 0x00000196 System.Void BlademasterPlayer::.ctor()
extern void BlademasterPlayer__ctor_mCC07B2A1C0CA9DCB71AAFB47F11BD5232D602205 (void);
// 0x00000197 System.Void FireMagePlayer::Awake()
extern void FireMagePlayer_Awake_m356BDEED8FED794B2D0D9744E985246FB29A5721 (void);
// 0x00000198 System.Void FireMagePlayer::unitAttackAnimation(System.Int32)
extern void FireMagePlayer_unitAttackAnimation_m5CAC42A96C2749E41F925A9A61697158D349BFDC (void);
// 0x00000199 System.Void FireMagePlayer::.ctor()
extern void FireMagePlayer__ctor_mBAFBAD4D418CC12C354FB0D91A9EBE3BACEF0944 (void);
// 0x0000019A System.Void PriestPlayer::Awake()
extern void PriestPlayer_Awake_mB9D37B83E0CB77137D882A0DE068A3BE7DEC9DF3 (void);
// 0x0000019B System.Void PriestPlayer::unitAttackAnimation(System.Int32)
extern void PriestPlayer_unitAttackAnimation_m704F4A6EE4DF3C7979567B9CC3DFC951112E02C1 (void);
// 0x0000019C System.Void PriestPlayer::.ctor()
extern void PriestPlayer__ctor_m7015CEB6C571AFBFAD53DD09CCD4DC02ABAB2EB4 (void);
// 0x0000019D System.Void FireMageAnimation::Start()
extern void FireMageAnimation_Start_m9744FC8ADE0F019F4ECEC5F55337D4EE6C7C39F7 (void);
// 0x0000019E System.Void FireMageAnimation::Update()
extern void FireMageAnimation_Update_mF8E68B52F884A9F4F167F73724345EF5C7330ADF (void);
// 0x0000019F System.Void FireMageAnimation::.ctor()
extern void FireMageAnimation__ctor_mD3C8F1A239B6C41A75FF245B5AEF3BABDB59BDCF (void);
// 0x000001A0 System.Void Unit::Start()
extern void Unit_Start_mDA1EE65F2CDF938E70BB7D8DEFFEE34AF066D943 (void);
// 0x000001A1 System.Void Unit::Update()
extern void Unit_Update_m57C1F3E193B819FC928ED8D8D4DF500357DBC6D1 (void);
// 0x000001A2 System.Void Unit::TakeDamage(System.Single)
extern void Unit_TakeDamage_m0A507F2F78DDAB20CE032AAA11CAF2B210E6C2F2 (void);
// 0x000001A3 System.Void Unit::OnMouseDown()
extern void Unit_OnMouseDown_m0E84FC160EDA6EA0D70525DD70BE83E8E0DDB9D2 (void);
// 0x000001A4 System.Void Unit::OnMouseOver()
extern void Unit_OnMouseOver_mF7C7C77573479F82FDE4A57694FE3464D61BE5BE (void);
// 0x000001A5 System.Void Unit::OnMouseExit()
extern void Unit_OnMouseExit_m33EF972FF5AAF0A20CBF8B29E571393859FA5CC5 (void);
// 0x000001A6 System.Void Unit::focusHud()
extern void Unit_focusHud_m160BA605D1ED1B342532E05883FC10F789A03907 (void);
// 0x000001A7 System.Void Unit::triggerAttack(System.Int32,System.String)
extern void Unit_triggerAttack_m515D1D463617170205A902B472E2408760513683 (void);
// 0x000001A8 System.Void Unit::TriggerMiss(System.Int32,System.String)
extern void Unit_TriggerMiss_m4507702975F1D39AA0217395CD6E3F7F5ABE828A (void);
// 0x000001A9 System.Void Unit::unitAttackAnimation(System.Int32)
extern void Unit_unitAttackAnimation_mC3D45D69D029DA8F947EEB59CFC043138DF89461 (void);
// 0x000001AA System.Void Unit::SetHud(BattleHud)
extern void Unit_SetHud_m1DB626658355F549BF574D68AA8F67E19D2A7DDA (void);
// 0x000001AB System.Void Unit::heal(System.Int32)
extern void Unit_heal_m8B7E5912C431540B8EA75E03BD91845FAACBCD7D (void);
// 0x000001AC System.Void Unit::removeTargets()
extern void Unit_removeTargets_m8B05E9EB9B859982B0DA2CAE56515157CC7D25DE (void);
// 0x000001AD System.Void Unit::addAoETargetListener(Unit)
extern void Unit_addAoETargetListener_mE0B3C250E29FF561535D5393AC8B74F263616D75 (void);
// 0x000001AE System.Void Unit::highLightOnAoEHover(Unit)
extern void Unit_highLightOnAoEHover_m3BEC4215D4376251092314365A1B08E99DA97411 (void);
// 0x000001AF System.Void Unit::removeHighlight()
extern void Unit_removeHighlight_m809612AC49D2895D050E599E1F96FC8473A6F2F7 (void);
// 0x000001B0 System.Void Unit::ActivatePersistant(Persistant)
extern void Unit_ActivatePersistant_mF28E07A9D1BA1F8551D91C845E7E0968AD64EA88 (void);
// 0x000001B1 System.Void Unit::RemovePersistant(Persistant)
extern void Unit_RemovePersistant_mFFCEBFA19BD0B708757A0606A6AA04E0088AA9EF (void);
// 0x000001B2 System.Void Unit::.ctor()
extern void Unit__ctor_mC26B9B196D3C2E0A8C80BA5BAB65286FF5038C58 (void);
// 0x000001B3 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x000001B4 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x000001B5 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x000001B6 System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x000001B7 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x000001B8 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x000001B9 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x000001BA System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x000001BB System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x000001BC System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x000001BD System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x000001BE System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x000001BF System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x000001C0 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x000001C1 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x000001C2 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x000001C3 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x000001C4 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x000001C5 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x000001C6 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x000001C7 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x000001C8 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x000001C9 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x000001CA TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x000001CB System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x000001CC TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x000001CD System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x000001CE TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x000001CF System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x000001D0 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x000001D1 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x000001D2 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x000001D3 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x000001D4 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x000001D5 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x000001D6 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x000001D7 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x000001D8 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x000001D9 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x000001DA System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x000001DB System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x000001DC System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x000001DD System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x000001DE System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x000001DF System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x000001E0 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x000001E1 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x000001E2 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x000001E3 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x000001E4 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x000001E5 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x000001E6 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x000001E7 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x000001E8 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x000001E9 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x000001EA System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x000001EB System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x000001EC System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x000001ED System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x000001EE System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x000001EF System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x000001F0 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x000001F1 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x000001F2 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x000001F3 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x000001F4 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x000001F5 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x000001F6 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x000001F7 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x000001F8 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x000001F9 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x000001FA System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x000001FB System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x000001FC System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x000001FD System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x000001FE System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x000001FF System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x00000200 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x00000201 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x00000202 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x00000203 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x00000204 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x00000205 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x00000206 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x00000207 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x00000208 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x00000209 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x0000020A System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x0000020B System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x0000020C System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x0000020D UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x0000020E System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x0000020F System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x00000210 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x00000211 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x00000212 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x00000213 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x00000214 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x00000215 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x00000216 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x00000217 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x00000218 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x00000219 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x0000021A System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x0000021B System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x0000021C System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x0000021D System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x0000021E System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x0000021F System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x00000220 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x00000221 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x00000222 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x00000223 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x00000224 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x00000225 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x00000226 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x00000227 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x00000228 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x00000229 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x0000022A System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x0000022B System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x0000022C System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x0000022D System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x0000022E System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x0000022F System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x00000230 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x00000231 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x00000232 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x00000233 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x00000234 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x00000235 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x00000236 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x00000237 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x00000238 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x00000239 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x0000023A System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x0000023B System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x0000023C System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x0000023D System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x0000023E System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x0000023F System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x00000240 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x00000241 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x00000242 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x00000243 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x00000244 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x00000245 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x00000246 System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x00000247 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x00000248 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x00000249 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x0000024A System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x0000024B System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x0000024C System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x0000024D System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x0000024E System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x0000024F System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x00000250 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x00000251 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x00000252 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x00000253 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x00000254 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x00000255 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x00000256 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x00000257 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x00000258 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x00000259 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x0000025A System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x0000025B System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x0000025C System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x0000025D System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x0000025E System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x0000025F System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x00000260 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x00000261 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8 (void);
// 0x00000262 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B (void);
// 0x00000263 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C (void);
// 0x00000264 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28 (void);
// 0x00000265 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963 (void);
// 0x00000266 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8 (void);
// 0x00000267 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B (void);
// 0x00000268 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20 (void);
// 0x00000269 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3 (void);
// 0x0000026A System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A (void);
// 0x0000026B System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598 (void);
// 0x0000026C System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5 (void);
// 0x0000026D System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6 (void);
// 0x0000026E System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x0000026F System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x00000270 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x00000271 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x00000272 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x00000273 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x00000274 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x00000275 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x00000276 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x00000277 System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x00000278 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x00000279 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x0000027A System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x0000027B System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x0000027C System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x0000027D System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x0000027E System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x0000027F System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x00000280 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x00000281 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x00000282 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x00000283 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x00000284 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x00000285 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x00000286 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x00000287 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x00000288 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x00000289 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x0000028A System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x0000028B System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x0000028C System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x0000028D System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x0000028E System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x0000028F System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x00000290 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x00000291 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x00000292 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x00000293 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x00000294 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x00000295 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x00000296 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x00000297 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x00000298 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x00000299 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x0000029A System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x0000029B System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x0000029C System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x0000029D System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x0000029E System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x0000029F System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x000002A0 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x000002A1 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x000002A2 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x000002A3 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x000002A4 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x000002A5 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x000002A6 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x000002A7 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x000002A8 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x000002A9 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x000002AA System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x000002AB System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x000002AC System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x000002AD System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x000002AE System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x000002AF System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x000002B0 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x000002B1 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x000002B2 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x000002B3 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x000002B4 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x000002B5 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x000002B6 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x000002B7 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x000002B8 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x000002B9 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x000002BA System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x000002BB System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
static Il2CppMethodPointer s_methodPointers[699] = 
{
	Bandit_Start_mEB90DB27250F1DCCE72DAE8E2AD70564A371F810,
	Bandit_Update_m4AA56D8697178973D4D5445450331535AB0C78C5,
	Bandit__ctor_mBB3235C162847932D6883C51E25822D7A1DF4DAF,
	Sensor_Bandit_OnEnable_m4ED93834FCB211044DF2BACA55B8ABE01959922E,
	Sensor_Bandit_State_m4B507F8CCB75D39B4774381BCF26431C05C27F0E,
	Sensor_Bandit_OnTriggerEnter2D_mA863ABC3FE70839ADC7FEDFE5D4D35E056AF5CB2,
	Sensor_Bandit_OnTriggerExit2D_m88D705EA71FE3E1B4757D55C2E66C8893E3FAB24,
	Sensor_Bandit_Update_m4955C0055975BE059A9E175C6D515ED9D5C35595,
	Sensor_Bandit_Disable_mC2AF8AE55A2394EABA233CEE33A25F57A606F608,
	Sensor_Bandit__ctor_mCF95685F73F65AC3E74D714A3DC0F5022E97494E,
	ColorSwap_HeroKnight_Awake_mC300E77BAC55F3F3762EAB6ED443812CE7CED35E,
	ColorSwap_HeroKnight_OnValidate_m77635C1E40FFADF346D52DE69DE571DEDA4AC2A4,
	ColorSwap_HeroKnight_SwapDemoColors_m7451617A6E75CCC436C10D134AF83D2805A4968A,
	ColorSwap_HeroKnight_ColorFromInt_mE05FB84D8AD045174A319A09C1CB96818E63E6C2,
	ColorSwap_HeroKnight_ColorFromIntRGB_m207C14A2EAED72FFD3DB2D763E6894149E8B349F,
	ColorSwap_HeroKnight_InitColorSwapTex_mBFF880467D0FA6AB9A8B5C909354651BF7B7F9A2,
	ColorSwap_HeroKnight_SwapColor_m744618ACE4BDEF0C04FAFB80C5A3A108BF5C2A69,
	ColorSwap_HeroKnight_SwapColors_mE1194C38947A0A7047493FBF805C3DEB23F6BDDB,
	ColorSwap_HeroKnight_ClearColor_m4CA8CA5C65175664E6BD499A73E8F9461A5A5FEB,
	ColorSwap_HeroKnight_SwapAllSpritesColorsTemporarily_m1E4E97C84A47A22F27D8A65809F35C01DA177C58,
	ColorSwap_HeroKnight_ResetAllSpritesColors_m16C79ABF75ED77292FF5DB7EF6DD51DB0BABCEB9,
	ColorSwap_HeroKnight_ClearAllSpritesColors_m39806D8DF38844A8F8866FC0A23ED5C162AAF834,
	ColorSwap_HeroKnight__ctor_m164CB224E9E17E806EF9F87E0662D5A9A045732D,
	DestroyEvent_HeroKnight_destroyEvent_mABFA897B5C96D82C3AA82CE5425C0380EA336826,
	DestroyEvent_HeroKnight__ctor_mCDD3ED77C731A1FD2AFC845DA4A5EC957CD0B1E9,
	HeroKnight_Start_mB77EC0BF5BC694C067CC37A724F671933FF3A16E,
	HeroKnight_Update_mD460E5567A914F73972E559A4261DF780934100D,
	HeroKnight_AE_SlideDust_mF799813F26CBA8887202F82B369744097CD864D9,
	HeroKnight__ctor_m978840F0B37FAF48B70AC0F1B509DA1BD789CF52,
	Sensor_HeroKnight_OnEnable_m6550F3ADE6F149BDB3E4E81F905EAFB3D598C032,
	Sensor_HeroKnight_State_m26ACCFC47992B42B5BE0BB5E7708ED786903CCE8,
	Sensor_HeroKnight_OnTriggerEnter2D_m20D86F4E79BE4A4283629D9A4434E061D4681CD5,
	Sensor_HeroKnight_OnTriggerExit2D_m9865B7DD0D122E11D6CF9A22618467C7FBF41F59,
	Sensor_HeroKnight_Update_m29B63C74411E6E502E72C00CAD874F5C5744C0EB,
	Sensor_HeroKnight_Disable_m6759A3052DF6C5FBBA89E598D8C4E85E4C7B02C3,
	Sensor_HeroKnight__ctor_m20BB68D68636C8356A8B857FE0855BF7E61C37DB,
	ColorSwap_PixelHeroes_Awake_m5C9F26E1B3E3CBA8C1E5A8E21B7839217CBE495D,
	ColorSwap_PixelHeroes_OnValidate_m243DE130C38F16CD1A8FD4A09AADABACD7953451,
	ColorSwap_PixelHeroes_SwapDemoColors_mD57FB295D42BBEE6E7D9436FCC6A1F89AD24C585,
	ColorSwap_PixelHeroes_ColorFromInt_mB2DF7EE96D293C4BC36C7B81C637200B9F0A1DDB,
	ColorSwap_PixelHeroes_ColorFromIntRGB_m8B82CC9540BBFEAEC3C09F410DFFFFEF5B890131,
	ColorSwap_PixelHeroes_InitColorSwapTex_m0A3A9CCC7781993C1B8BF6F66105C7F4F00EE0A0,
	ColorSwap_PixelHeroes_SwapColor_mCB1A49EED094C75C475977E92BDED337B915FEBD,
	ColorSwap_PixelHeroes_SwapColors_mE082CC4D4842C679F9B62352D0201ADED785440B,
	ColorSwap_PixelHeroes_ClearColor_mE9B89123B76A3FD3744EB7C3EA65CBE0EF19A6C3,
	ColorSwap_PixelHeroes_SwapAllSpritesColorsTemporarily_m0677F7365342DB9E4C9AD4AA85BD392EFF6EA3E3,
	ColorSwap_PixelHeroes_ResetAllSpritesColors_m91F26DFCF3CE7F3B402BC1F6181B75DE4E9ABF7D,
	ColorSwap_PixelHeroes_ClearAllSpritesColors_m60A77F9607F545A633CB206B4D60DC0B0A060DE5,
	ColorSwap_PixelHeroes__ctor_m89B55CFDFE6005C89C7FFB3B2AE09C0B224122DA,
	Alchemist_Start_mA2E964110CBF6BCE68715BEF5DC9957BE9F80CDA,
	Alchemist_Update_m28E59E450C1AF2E6F2C432F84C523DFEC8750F94,
	Alchemist__ctor_mA95765BFFDFC2AD25D4B4F233A4E8E85D2E48DA8,
	Barbarian_Start_mDD53F735E2C3F320B46FE5631E9318429F13D64F,
	Barbarian_Update_m2A34B7C51D1477B4D7BA555BE50B1F71018BC0FD,
	Barbarian__ctor_m1EA293C871BA67DDFB6DF20CA95587E482C10802,
	Blademaster_Start_mA51C5116E3BF7828377DF6B9C9885C829D7178B5,
	Blademaster_Update_m6C28D795265E43ADFB010269703B7FBB4B4A7C69,
	Blademaster__ctor_mC08F1D1334C26E2DA10C754659269F1350381CA9,
	CharacterSelector_PixelHeroes_Start_m8844166B74754031FA361F8AFA065E9CF9C9A0D8,
	CharacterSelector_PixelHeroes_Update_m673B51FAF8802DA07770C8C0FFC3BD3CB1BF46BC,
	CharacterSelector_PixelHeroes_changeCharacter_mA03B037A4A28B5567505C5083BB3B09C10135FAD,
	CharacterSelector_PixelHeroes__ctor_m541DBE611870C51D8DBB72B009F822727D898751,
	ColorsCharacters__ctor_mC93F552E448526D6E23F301624A8DD2A7552BB60,
	Druid_Start_m1EE12E6CCE170D4791D9A14390EBD4B9633260DA,
	Druid_Update_m440A8E57C6368579A6ECAFF321F5FF25B0FE9508,
	Druid__ctor_m5CEC17B0C147470377DE5761E5C5488E39B4A7E4,
	Effects_DestroyEvent_destroyEvent_m37AB761B35C37BF937B3E5F35BE2253016F63C04,
	Effects_DestroyEvent__ctor_mF23E44F214E5DD362747BBC9B421C02449AF0372,
	Effects_Projectile_Start_mA70FD1335C00993E97A0138DEDD9F18F0A564207,
	Effects_Projectile_Update_mA2221CBC924A3018CE65A56F00BDEB7284F4622F,
	Effects_Projectile_OnTriggerEnter2D_m1681BCDA59DCC12CB4912DE330BC7948DFCCFC02,
	Effects_Projectile__ctor_m2A4E4E7591D5E605924087124B746F61B6A1E026,
	Firemage_Start_m697201DDB98ABF9A61B02E15FE70981589E6921A,
	Firemage_Update_mB6374D94CE7D30C0E48A39D9721E3D6CBDB752DD,
	Firemage__ctor_mD8F5E64A1BA2A779907849A89A87B59D370584BC,
	Necromancer_Start_m5E3710A74901CFB80CE4628893420BEAC1EC1C42,
	Necromancer_Update_mCF3D573C7B1143ADA73A04DB67D38BAEB1D66F49,
	Necromancer__ctor_m666CF782739C68FDDF0C7FEE617BC2FA4B46289E,
	Priest_Start_mD8A4D7389CDCE56823C0C4334DCE4D3358776379,
	Priest_Update_m46298572E41ED4657757D68844A1DCC0838D7506,
	Priest__ctor_mFD155429FC0A8F10C287295C0CE10BA43F4E2E17,
	Rogue_Start_mC43A0AE0DBF2EAC3E02F1BF10227244C374A1EA0,
	Rogue_Update_m2B1F193E99CFB06AFDA529DD05CD718B74353468,
	Rogue_SpawnProjectile_m55800C5A11644E4BAA5185022434DCA5CEB59172,
	Rogue__ctor_m44BF4BBB22E449A1A4FA47AB2A6DCAD65ECA3D16,
	Sensor_PixelHeroes_OnEnable_m2FEDAEFDAD059F7023BB6391D0525A6751054BE0,
	Sensor_PixelHeroes_State_mB7F972F38DE9D139BE420CB95B91584AAA6B0023,
	Sensor_PixelHeroes_OnTriggerEnter2D_mE7FB437A5753CC4923D6494862E948F30513EF7D,
	Sensor_PixelHeroes_OnTriggerExit2D_m986D159070DC74B5AD8CBE4534F84F71F9881207,
	Sensor_PixelHeroes_Update_mEB023E057D72EA444674C6FDD1633142F840BAEE,
	Sensor_PixelHeroes_Disable_m8BECF797F66C56EE7B68B513C850FCA8A5A75774,
	Sensor_PixelHeroes__ctor_mB7743927F55FC4DEA9EB137FB228C18547B4B803,
	StormMage_Start_mAE3A71C2C8FDCF34DE7381AA410C30C0D8BE8229,
	StormMage_Update_mB206A2C8D88882D4469149F340F991220B3E0DF5,
	StormMage__ctor_m0CFD67F741594BF89CEC6DCBFF53CFB9C70E41DC,
	Warlock_Start_mAD34A241B57717364EEA02F5E6CC1F3A9EDE08F3,
	Warlock_Update_m077D58BC2324D8D40A8AA0FC457F43ED5F3B6874,
	Warlock__ctor_m841198F0B86899788DE536E75E82D1393F898812,
	AoeCard_Start_mB0DE983A477E80AFDA05A742551296EF78D6257A,
	AoeCard_OnMouseDown_mD26A91C04DD174CCF5D27760A69AA33D99C1B714,
	AoeCard_setAoETargets_m54BFA79D70D1200CA65DB9BF765ECAD8E857E8AE,
	AoeCard_removeTargets_m6218929F1220C92EB7E4F2A0935764E9F6161360,
	AoeCard_triggerAttackAmount_m77FC9A3C6AE446306C67DD09727361B60BA12D0E,
	AoeCard__ctor_mBDCD34DFB12C8DB86C2840285C4D9F0553498076,
	U3CU3Ec__cctor_m9D072C2CF5C38693CA9FC97D7E524EDDBA8BEE33,
	U3CU3Ec__ctor_mD1E10EA9CFFDCE5DF7A58FA90F79DA0E31034DBA,
	U3CU3Ec_U3CsetAoETargetsU3Eb__4_0_m6E42BFC02EC593B9F5FA52A2EBB0532DD4AF3E64,
	U3CU3Ec_U3CsetAoETargetsU3Eb__4_1_mEC5598C4C69973255EC10279B6340CB50D3F08A4,
	U3CU3Ec_U3CsetAoETargetsU3Eb__4_2_mF1AC406C68DDA8A766A541C32E9E664F7EE96C2A,
	BattleHud_SetHP_m61D0507689470D313BFC6F8C88ABB2A0655909DD,
	BattleHud__ctor_m3846DD309B63E89C342F920DD76EE992832FE03B,
	BattleSystem_Start_m1025D66B5CF5D511911629CA264C0BD577630584,
	BattleSystem_Update_m624B962FB7102E5B072383F0657CD680BF63B4E7,
	BattleSystem_SetupBattle_m0D28E699F81949ECAA73B8591E2767488470AFFD,
	BattleSystem_PlayerTurn_m59442E1E51D3B5C5C7F855AEF5DFEA9FC5F35096,
	BattleSystem_PlayerAttack_m75E91E71E1E8BA5D80D9268AC9DC341BEA40ABFE,
	BattleSystem_EnemyTurn_m109A1BCD53ED5D8EE736B63A92BF883A33670DF6,
	BattleSystem_swapPlayerHud_m2EC845378E3B2D1CD3018C059CB038A087EF3ECF,
	BattleSystem_removeBattleHuds_m0CD24082EDBA873E0F71865650B3E1A96E098F71,
	BattleSystem_nextTurn_m391680418C9963C695024E2BA240A948B48AE27D,
	BattleSystem_setUpTurnOrder_m86C4EF9CF1C4EA5134F96AC49970D37B453217D6,
	BattleSystem_EndBattle_mB853A74689A6108B08183925B40DA14AF371D294,
	BattleSystem_RestartGame_m630345BC754A06D2D402321877BFE96F8DE0DE3A,
	BattleSystem_cardExecuted_mD3D553DCC4A65AD63544A382F0508FC89F693C03,
	BattleSystem_unitDeath_mC7A54488C8007417EDF30694421F34CB6F4334FC,
	BattleSystem_announceAttack_m18A613F492AFBFD2E49B4DA0B7E96462FF183603,
	BattleSystem_announceMiss_m829667AD76E199B6FC3365FDA4028C53490306F0,
	BattleSystem_turnStart_mB0ED601F7A04EADF190FF5EA71DEFD9C2AAE685C,
	BattleSystem_doNothing_mE3052618955C90ACAC2B4A28BDED6B058A98FAFA,
	BattleSystem_doNothing_m6B5599B1A53FEEC06A10EC65EB86EB04E1BB0D77,
	BattleSystem__ctor_mEA0CD6AB1AA5795FED3B2A0126F1A42ACE219035,
	U3CSetupBattleU3Ed__26__ctor_mC5002BC828A74E13D8D11C515DD7476E7F6DE633,
	U3CSetupBattleU3Ed__26_System_IDisposable_Dispose_m846F497C52C6AAA708BA240625C762A35246CE86,
	U3CSetupBattleU3Ed__26_MoveNext_m925CE0FF7A6F0DE37B28E3320EC16932A5172811,
	U3CSetupBattleU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m17796639047F5A07611CC6A7E43EEFE12783F6C7,
	U3CSetupBattleU3Ed__26_System_Collections_IEnumerator_Reset_mE3FFFF2F9ABAED7252A8972825B36C16D0153E29,
	U3CSetupBattleU3Ed__26_System_Collections_IEnumerator_get_Current_m3FCE34B11C0CAD0B43E3D122FB1E3C0EB0B33C14,
	U3CPlayerAttackU3Ed__28__ctor_mB66E5D13ACCA1DDFEEB3EBA579D2007F210EBACD,
	U3CPlayerAttackU3Ed__28_System_IDisposable_Dispose_m6DFB75D250FA56017A94B249B3F409E06D61ABF1,
	U3CPlayerAttackU3Ed__28_MoveNext_m81FB46EC2353316D4008C2D3D0C23B2087402C04,
	U3CPlayerAttackU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C2BE6E9A1853E9F24D3BB5C557F6755BFB1410,
	U3CPlayerAttackU3Ed__28_System_Collections_IEnumerator_Reset_m9CC238975D5E94582C5729DE2E61BFC61BE66275,
	U3CPlayerAttackU3Ed__28_System_Collections_IEnumerator_get_Current_m226747A1B97C392ABFA9299B8BB098F12ED54881,
	U3CEnemyTurnU3Ed__29__ctor_m5AC36FD6F5FDBD037EB7D941BD62D522B9D0EDFF,
	U3CEnemyTurnU3Ed__29_System_IDisposable_Dispose_m287F93E7AD702483674DF8561E0184C5A4802C6E,
	U3CEnemyTurnU3Ed__29_MoveNext_mAB93ACC2261DA5D3EC53E17CEA2D19436B444BAA,
	U3CEnemyTurnU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA0A457697536C41FC21944B39A8DE7CDEE0169D,
	U3CEnemyTurnU3Ed__29_System_Collections_IEnumerator_Reset_m5927CE10D791B5B23B8FA93202C35615F0A9FF70,
	U3CEnemyTurnU3Ed__29_System_Collections_IEnumerator_get_Current_mC8AC869BA6F125A9346609BE01FFB022438CC253,
	Card_hideCard_m3183E9F5E9F0C97C133BCABDD4A5135EA53680CD,
	Card_showCard_mFA2D37BD5BF2A7B80A4BAB5CE043193FCCB63496,
	Card_activateCard_m2AD708BBE6F4C3E9479946F598ACDEDF82A71A76,
	Card_deactivateCard_mA083E93A51ACF882284935934A65BF8A09C1BC41,
	Card_OnMouseDown_m5349CB231B19800A38CF4FC8FFEDFC0ED13127E4,
	Card_removeTargets_mF4C28F9A46BE124751D1C46A71B1B0B93C2F814D,
	Card_triggerCard_m10D85F1493D2F94E9FEBE32B528164C36E717F37,
	Card_triggerAttackAmount_mEBB465927AF112B973D375623AF1213B8D9F4BEE,
	Card_moveToDiscardPile_mADBE0A0112C531BFCF4C54CF6D9AE3E02999653C,
	Card_cardEffects_m87A5DA7394FCB4C5D734C641D518D51D708FA994,
	Card_OnCollisionEnter2D_mC88C5AE8418E52EAC30319B2741B37A0919DC7BE,
	Card__ctor_m2ADE17E90583AE44842C04CD1E69AF1CC03DC8A8,
	Deck_Start_m7B9D31952401BE90306BAE90C220CEB34B8C1AAF,
	Deck_connectCards_mEB0644B2E073F9BF827195DB9256DD683410A5C1,
	Deck_hideCards_mDEDBC9A43D9C4B27600CCF23160C20324BBA1423,
	Deck_showCards_m57CC57CBD91A67C175DF7955AAC0C08D4501F007,
	Deck_activateCards_mF9D86E37042DD947831A880FFC45F83F2F730AC2,
	Deck_deactivateCards_m84192B707911AC22A783EB5C2C7BCA3FBD22ED5A,
	Deck__ctor_m1699C8F29839AA379EA15AAE8D57B6108C6571CB,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_chooseAttack_mD2599DB794C6F7F528B3F815A06D222FE54D5771,
	Enemy_chooseTargets_m88C95A5802CA2BBCFC68CCCD5743D35E430958A3,
	Enemy_activateAttack_m2B596E2935F3C5D6ED29AE3BBD2096430F5CF110,
	Enemy_executeAttack_mBCAE6DEE051F9FAE245CA0228016D8F7665E8ABF,
	EnemyAttack__ctor_mCC6A84818778B6DFE004491CB0A7084A927EF65A,
	EventManager_Awake_mF6EAD589D3C0919D6C0E986A3FCE8EAD22F505A3,
	EventManager_debugAnnounce_m88E20710E905DAF4A3D588C174487E6F30F7D2A2,
	EventManager_Update_m22642EEC74E5B0FDF52C79044924EA16B2A06205,
	EventManager__ctor_mB83B8B21CFF0A302EFEBD4480BD115BE8353C1DB,
	unitClicked__ctor_m96AC2F9A9BA6BA1BBE0B36082C1407A8716741E1,
	unitClicked_Invoke_m54C0B532206B91B9CFC3E935CB774E8F926DDCF6,
	unitClicked_BeginInvoke_m23C0DB6865A6151598F011B63F5712C7FFBB7AC9,
	unitClicked_EndInvoke_m51A17EE6EBABC9BADE889B78C79F35D4A1E0275D,
	rightClick__ctor_m15D76D10640A931441E7CCD00A834F492E637423,
	rightClick_Invoke_mE01AEA1283BB5CBFAB40E9F4B1F9E1A3560D104B,
	rightClick_BeginInvoke_m94FFDFB5B0A6B8AC13920F2001C0E9D525A24105,
	rightClick_EndInvoke_m5B7D21944A31A555D5148CB94E7060779C2E92BF,
	cardTargetChosen__ctor_m6CE1D8762DFF777590DA0CA76004D6CF06CD2E12,
	cardTargetChosen_Invoke_m0161A6EBA93C1944F6F797156EE917D47708AF5E,
	cardTargetChosen_BeginInvoke_m20761EDA24C78AF33CF8C28A6A94176231E71AEC,
	cardTargetChosen_EndInvoke_m0EB0D6421CCB3A6DF0E621CC2000BC93ACA7EF89,
	removeActiveCards__ctor_m4B8F83959687C368C584E30E22F425413227CA03,
	removeActiveCards_Invoke_mDDB86457B4BCA4AC635519845ED1738DCC51D5A2,
	removeActiveCards_BeginInvoke_mCC8A7DD83F9F3F388D51E58EF896CE81171D1DB5,
	removeActiveCards_EndInvoke_m1934CC54AD6B18E61B8A73C4CA53DD294EE12900,
	aoeTargetDelegator__ctor_m2304B11A813A5D23D7BE726AB034E60C6F082014,
	aoeTargetDelegator_Invoke_m12FD99A6A248078962855271FBAD29BD679DB751,
	aoeTargetDelegator_BeginInvoke_mD4FA9794206EABB410D72671602C850E673F7775,
	aoeTargetDelegator_EndInvoke_m3E897ECB5C7F65F6ACBDB8D5BFE07BF9E4C5D9E7,
	removeAoEHighlight__ctor_mB094A641C3AFFF9E72F90F5C89EB56D346EF3961,
	removeAoEHighlight_Invoke_m964BAAEA21C2B92027DD2FF3D8786A7C0A472129,
	removeAoEHighlight_BeginInvoke_m9FF734A42E75B4AA938D9C4954D46FCFB817598A,
	removeAoEHighlight_EndInvoke_m3272D21F6894086C1A4CE479A749BB968DE8468D,
	attackAnnouncer__ctor_m60FC6F2C27F9863BC21D62C602D74550CC291453,
	attackAnnouncer_Invoke_m16E4C634668EF32FDB837E5F9417302153B1143F,
	attackAnnouncer_BeginInvoke_m1C56D89509786B78B958BC7291D078F7ADCC50EE,
	attackAnnouncer_EndInvoke_m23D2B1B1B47A2405B8FB0D0B70ACF33A6CA05956,
	missAnnouncer__ctor_mBA9A21B92273A709016BB9D22CEC45140D647C8D,
	missAnnouncer_Invoke_m2CE32D3A641BC82D022040D220BFAA08AF0148D1,
	missAnnouncer_BeginInvoke_mF03761B8AB200FA33D6A3910B54A48D2ABBDEA3F,
	missAnnouncer_EndInvoke_m3CDFC0EC3F5C97F2723F557844DC51CF3060AE26,
	turnEnd__ctor_m70C7A4429C3F878952EBB18EB4AFCD8D4E537C99,
	turnEnd_Invoke_mCC5922C8A8DD4349B031DB1FBA1317B1810D44BC,
	turnEnd_BeginInvoke_m0C89D9155A4C2ACCF0792820D19FF02C2D064853,
	turnEnd_EndInvoke_mF5A9B5AB2494C1F93BB915C894E3CA563F360CA7,
	turnStart__ctor_m2B64D5CF764CD265244B20A377BB995C05F32004,
	turnStart_Invoke_m5ADF89380CED582E811C63F2FB3B8E62E629E9F9,
	turnStart_BeginInvoke_mBB43CA9B0BD0C050D85D368591496874CFFD6279,
	turnStart_EndInvoke_m038FBF8EDB0B48C1C35BB3E4BF405658AC58F09D,
	unitDeath__ctor_m4E3A8B65452C207278BCC7B7C12E7299EEE3F166,
	unitDeath_Invoke_m4337595F5C9BF70D8D67EDB9B7FD5D0EFB72E91B,
	unitDeath_BeginInvoke_mEFCF979FF9D1A6BF9EA1D641C840B790827C9B29,
	unitDeath_EndInvoke_mACBB157C09A47639DB4E71AF7F44304091B562C9,
	unitDamaged__ctor_m4952E480B9316DB74144FF6FE5F71F370C885C3F,
	unitDamaged_Invoke_mD00947B0B05D291C9E87157F7C1D369372291C5B,
	unitDamaged_BeginInvoke_mA8AE029035EC4C75A4C99D788AF64B7075DEC992,
	unitDamaged_EndInvoke_m3543E413AF769E3B2475A3ED48556FE0FC9EDB60,
	unitAttacked__ctor_m7E11AF5C90B4D2301DE0812E849329F090792DB4,
	unitAttacked_Invoke_m8AB0D83582AE74CCCFF72D445D0442BB0DFA19AB,
	unitAttacked_BeginInvoke_m626DE48C5CC23E732FF8CB97BCD7DA243D4F339C,
	unitAttacked_EndInvoke_m6A05902D0A2ABCA05E1E2B33CB2112432647AD86,
	testEventManagerUp__ctor_m8B7C89D5C34DC1C10C26B3E325685201546F4173,
	testEventManagerUp_Invoke_mB861D5D7F7494293E7BE7D59C5E6363496DA100F,
	testEventManagerUp_BeginInvoke_mB8CBCD3C40B60BC40CFC1E96A0280745980C6BDF,
	testEventManagerUp_EndInvoke_mF2179510BCC4F9D91C581E41DFBC8E4DCB46D2E1,
	GameManager_setRandomSeed_mCDDF6D1EAD3A8EE224D5701EF6B720483A44F85A,
	GameManager_DetermineTargetHit_mA9BF8791D1523662DCF5925EAE07468A5296448A,
	GameManager_deactivateCard_m398C5468438F480C07D04E612B04AC572ED4E180,
	GameManager_activateCard_m317CFFBF94629561163BCDE6497045FCC473A078,
	GameManager_getUnitFromPositionNumber_m4DA60FF9A5C40F4051B57872A61D4C472E491470,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	GameManager__cctor_m0872BB5B2E20B9183F004EB81D5129608114D285,
	KeepOnRefresh_Start_m3CC0FC7B054EE0AFD8B1AC3F1AEBF7260DAB82C0,
	KeepOnRefresh_Update_m820970809AF8CF76F677440B0A8E0282561734E1,
	KeepOnRefresh__ctor_mD209A00BA9FA514A05D2AACA47EA541FD486D805,
	Accuracy_Start_m980DB6F8331779DEAB8AF58F240C87878299FE59,
	Accuracy_ActivatePersistant_mB1E5F5E835689FA2F16B1C6D39BA13BD479DB0F6,
	Accuracy_RemovePersistant_mC67CD70C344B200C4FA1779C6960D293DB989182,
	Accuracy__ctor_m6AF036E76A3BC828771EEB0788E46C1C9129E9CC,
	Armor_Start_mB3C650FB54B35199D41EBF11F43D712EC553C8B3,
	Armor_ActivatePersistant_m86540ED4CC12E8B7F6F83FBC166D00AD89331A95,
	Armor_RemovePersistant_m351E7AE550A71D93CFC41E15C9D945D0CEE0E3C4,
	Armor__ctor_m6954859265EA92F2E34B645D91155382CCA068F2,
	Bleed_Start_m66A521E4C41A0258B60889132D0F82DFB8EF36F1,
	Bleed_TriggerPersistant_m80CE9648BEE9216A3238BEA89B94F9BDC994A987,
	Bleed_ActivatePersistant_mBCE5B5FC43D9C108B21296984419DA1B8956D7AC,
	Bleed_RemovePersistant_mFCED2B9EAD5F76600B9FF042D4547F8095A8718A,
	Bleed__ctor_m70498D5457E1C51E0283C23656C79DD4045BBF35,
	Damage_Start_mB344528E3D9EA1C7EB0F036AE5335456FB830B83,
	Damage_ActivatePersistant_mDA286B08660076981A6426F0BC7DF2D2EA7F5CF8,
	Damage_RemovePersistant_m1C9E7911266D312047D604395C14027B2B4B0176,
	Damage__ctor_m448736D790344E455BBE20282D3D73C91E6E2CAB,
	Dodge_Start_mD051BAAE091535AD3F0D41FAB28C371524FF35C1,
	Dodge_ActivatePersistant_m968F718425BD2411A5DBB5B6DC1237D923D0F6AC,
	Dodge_RemovePersistant_m4C1AC66001511642C403227AE830825BB77E854F,
	Dodge__ctor_mF2B67DAAE4FDA0BDC7962837BE577374DA570E52,
	Persistant_TriggerPersistant_m87D7DE610DB3DDCF82F1C64A87B8DDB33B9D776A,
	Persistant_UpdatePersistantVariables_m162BA3B2674CA53E51B64BBFA3A1BE2D09657A1B,
	Persistant_TickPersistant_mEA1FC3442EAD7CEEB4D895E53D5ED00D00A49206,
	Persistant_RemovePersistant_m96275C3A935F13AA2BBF252F3990C4B2B7996B01,
	Persistant_ActivatePersistant_mF22508DD9A91665863527C12F0B7D4A2513108B4,
	Persistant_AddTriggersToEventListener_m2303DA75FFAE198E05A6B1A258FC602092C0A083,
	Persistant_RemoveTriggersFromEventListener_m92DFC7B10912C321AF6C1EDD4B524AA884E9DCD3,
	Persistant__ctor_m0CF8628709B0A20163A83C4D7392BC36A9FE2204,
	RagePersistant_Start_m912E0B8EEA5C8FF907E28815AD6470EF351CB16C,
	RagePersistant_TriggerPersistant_m86DC1B86C7B23B1285669FC4AEDC1A785D86AB75,
	RagePersistant_ActivatePersistant_m91D0EA347B93714CA3639CFFFDE41C4DE3EE2280,
	RagePersistant_RemovePersistant_mCCF10921348C08B31F856F543BF356CF11D80137,
	RagePersistant_CalculateRageDamage_m134B8EC75F16535963F1657D9AE824878A6C2A84,
	RagePersistant__ctor_mF61146DA59B48C282DEC16BEB3DB1CEA46B19535,
	Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3,
	Player_DrawCard_m3FAB10BA5D373B68508C358C7ADC45DEFAAA2FD2,
	Player_deactivateDeck_m406D4B60C3E58DD2AA93B71185CE5BE9EAACB46C,
	Player_triggerAttack_m0A13A039E2CDB3A2BADE169EDA2CC43E6EFD7E29,
	Player_shuffleDeck_mB4D7A514BFEFAB45266460CC823F16607B1BE7EB,
	Player_focusHud_mD6D16FE52CC013F13F99491FCCD817B729D25723,
	BigSwig_Start_mA249D3F93A85A59A3E120C85B3E4AD75D43C738B,
	BigSwig_cardEffects_m22B0DD66517B8F2BC9A7DEB246A112F769834C5A,
	BigSwig__ctor_m1621F3425CF563FC0427BDB634BB29B32CD8F5C2,
	Cleave_Start_mE82AB6289AF7D3DEA3FFA5E0658E90B0716E9749,
	Cleave_cardEffects_mAE19D8F3F642B2CB30F9B4F99C081EA9929EBF9B,
	Cleave__ctor_mEE4B648F9BDD4EB94E2420C1BC33186096B73D6C,
	Dismember_Start_m9F662B07903ED8D4BEDCA60EB734E93CEB7DFE86,
	Dismember_cardEffects_mF3D1B46A3545F5AAC376C1C2E192A7F54ED18262,
	Dismember__ctor_m087600F22EC648FAD6B65A357ACF7BCE265682B2,
	Howl_Start_mB52BFE8C4298013D6675372D9E41DE7CEB09CD65,
	Howl_cardEffects_m84454E9568C12C2B8B77BF5B3859214459FCE62D,
	Howl__ctor_m2853FEA40FAAD55679DDDAA8728A28F06405E3D5,
	RAGE_Start_mDC5B3040712F63FCF9B9053FAD38B5ED3667152D,
	RAGE_cardEffects_m986C6C4CB8B8FF6A9FB796F543D855B94E326E29,
	RAGE__ctor_m64C1CE298C5408C728767A4F637EB38730E16291,
	REND_Start_mB15D5C99BE852DCE4590A3636F80BDEF012367AF,
	REND_cardEffects_mC5382C11C45A007E9EB95876E3C06C48434BEFC6,
	REND__ctor_m63722DC92ADBD4B076F0A71FC25C25084D4C01E1,
	RecklessCharge_Start_m1FD0B3ABEAEF164E6AE5919D05922FB55FC4BB6B,
	RecklessCharge_cardEffects_mE3022E9E1153E3A429B1A782315A813BF0036502,
	RecklessCharge__ctor_m93C131BC5BEB25593E25BE6AB35CAB5D0A359138,
	SeeingRed_Start_m21B738DA3DD24F2CB04E70AD41F7153F8AF5ECD3,
	SeeingRed_cardEffects_mF6AF3B5F390BCA74E67F9556CEBD86B272DD0252,
	SeeingRed__ctor_mDA98AE220B48D57E408524EA89664377DF938DED,
	ToughSkin_Start_mB4CA25B756E8E53CA51DF68E549E89E8B2684471,
	ToughSkin_cardEffects_m6562D95C322949BA99CDA6C01080805AAE3420C8,
	ToughSkin__ctor_mF732AE6F1DCCC796EFA6277020FB02909D4BBDB6,
	WarlordsTrophy_Start_m7A1DD505F89A046B21EDF69B6698DFE8F8BB5E91,
	WarlordsTrophy_cardEffects_m132A1C078D004EBBF23BA7D10E80A489D8102BA6,
	WarlordsTrophy_triggerFear_m6A5087A672780A265A9EBA4255A6C9F0B9B29453,
	WarlordsTrophy__ctor_mBE0F8C80564BC282B67C5395B0316BD5D5CF4A0E,
	GazelleStance_Start_mA5BC8761F7DFF3900646D1364FF0BE5765A22111,
	GazelleStance_cardEffects_mD7B30CC653AD07110373834C07F7C2C2BABE4C47,
	GazelleStance__ctor_mB59A77C3CD18B11BCAD4C18F3CE3876FCAC8127F,
	MartialExecution_Start_m121908A84733B3491426BB9614B8BBBE5D936C18,
	MartialExecution_cardEffects_m0D0AE7E5EBAA6B0006C5B007CC6EAE2F35CED03E,
	MartialExecution__ctor_m21BA61DC7E68C92BFF36774671BF16B1B8EB1045,
	MastersTempest_Start_m09BF75F74D3C1A63AA3479CD92A4026B454FBB92,
	MastersTempest_cardEffects_m01394A739B56DD882A31F0A4613A98DD8EADB2D0,
	MastersTempest__ctor_mF8ED5CA071A4BAE65D94F9FE3B9EA78F51AB7DF5,
	PantherStance_Start_mB2F5A54200A73534D55CD11B067951CFF09E56A1,
	PantherStance_cardEffects_m37C2256E26EE2F650A6618F11696B58D8CC76477,
	PantherStance__ctor_mC407C6A4FDD370AF44599BB5C90BC3038876A75C,
	RhinoStance_Start_m6CB4E3B68ACD46E62F2C7C13ABC1CD70208A024A,
	RhinoStance_cardEffects_m698A9CE38DC93178A72E62B65CF7B63736948CA8,
	RhinoStance__ctor_mCDE6A671B5801CECEF2A057967E38541A3151F51,
	SliceAndDice_Start_m3293CDAE7020E79FDEBE32802FF672A621AA5FDB,
	SliceAndDice_cardEffects_m0208FB8B87DEBB4CB6FD3E63D1DE155D0344DEB7,
	SliceAndDice__ctor_m2D98D107562FFBA9CF23F2097154C4925DB22F65,
	FireBolt_Start_mDEE3A446C55F5C353545DA7D83A67CC2AE8EF214,
	FireBolt_cardEffects_mF3A4BA88CA0755E8EAA2F22DFA1A4AAB6AE4097F,
	FireBolt__ctor_m220D03E9D60745FE784AFA204FBA548A46B5FF5D,
	FireStorm_Start_m13C854762EA3DCFC194A69E5F8463C171575513F,
	FireStorm_cardEffects_mA3BCB1BEEEE75F2192B0D80E9836065F23B78BA4,
	FireStorm__ctor_m28DA58D9C334DCF2F14B9FB2D554D67359B03EBB,
	HeatMetal_Start_m29ED4D154B3709646E1200064DCC452531BC5832,
	HeatMetal_cardEffects_m27A69B6BED71D0E3E5B88DACC704D6FC71757202,
	HeatMetal__ctor_m92429EFFE968B26E116132B9C84735BFF07F2A51,
	IgniteWeapon_Start_m6C4D1DAD4C78B95F5CD8C07F55CF9B9766FDCF84,
	IgniteWeapon_cardEffects_mD3A34907DBDF92B44677BFFD7C91BB6C4BBC820C,
	IgniteWeapon__ctor_m9BD3AE45D0C83348E91086434E3A78E0167B7194,
	LavaSlap_Start_mAA2FD19DFF2716735442BAD3FDA701EC22CC3FD4,
	LavaSlap_cardEffects_m49BFDE25BC64420EA459D6B4C152DF420AE279FD,
	LavaSlap__ctor_mE55BD546804423207DB9935DC3FECEC77604D866,
	SolarFlare_Start_m0990E65B0EEF579EEA4726994A969B43011AD74B,
	SolarFlare_cardEffects_m299578CD83A2803E26B41421030E2A186559FA6C,
	SolarFlare__ctor_m3934657FD112BF7CD4427B6C2CD41CE07F434CFA,
	Shank_Start_m4EEBA17CD5D0583215354198D5DEFC8F33B7DA1A,
	Shank_cardEffects_mE3127F6B7C432916AE4419A27631C0F557174B75,
	Shank__ctor_m7DE0C877792487F902809DE828F80467D0443349,
	DivineIntervention_Start_m65E9DA62AB5E7832D7255D4D2F2F52F2F2112E8F,
	DivineIntervention_cardEffects_mF805EA84FA988F573F022AEB59811906CEC3A520,
	DivineIntervention__ctor_mC9BFEAE22F57E2B31A9AA1FE85FA5DE8CC2E68C7,
	FlashOfLight_Start_m30043C184483DA91F3DDB2C723E07F59C4C297E7,
	FlashOfLight_cardEffects_mF7D969E329081ADB7519107EE1B96D979769AD59,
	FlashOfLight__ctor_m180ABE1C825B533561A3C96CFC863EA21E3686E8,
	Glory_Start_m9232DB54E653C4C8510DF9BDA38FE4C9A9A42722,
	Glory_cardEffects_m4906C31F79209467462E4BA7C59CD6DBE8765EDB,
	Glory__ctor_mE78959EDD77FC254F6598826826B19C4709E0FB7,
	Heal_Start_mFED609782FD3613899C578916ABDD7343816EE93,
	Heal_cardEffects_mB3E34C998CE3A8384DCAA7A31B2F1DFEE40358BE,
	Heal__ctor_m34EE99E03F61CA1326B62D8007FDF4E89A75FC8F,
	HolyGround_Start_m3C7B943FBE424F29B9DB977C0358D7182CD6D1BE,
	HolyGround_cardEffects_mBA51CFC7B2899FCD929CD99ECA99B7D5FB162C5E,
	HolyGround__ctor_m60FD172EE782F2544DF4C1C7FB746BFB0CE81D14,
	HolyStaff_Start_mBD7547CB072716F9D0DDF55DD53478713CAB185E,
	HolyStaff_cardEffects_mEFD33E3EA8F412293117D72C0226BDFE777B6952,
	HolyStaff__ctor_mAB1892BD491E2E5C36494805DCBD166D52953AE7,
	Smite_Start_m6E9ABB2A6A191AD6626E42A48FD75B84FDA9E5EA,
	Smite_cardEffects_mDD97384BC6CBB8111E72230A120CBD2459AEACC4,
	Smite__ctor_mF54DC9BCC2695B14C60F1530F0731079EAB6505D,
	BringerOfDeathEnemy_Awake_m9A3DAFADA1B6696B009378043D4279F04F71476A,
	BringerOfDeathEnemy_Start_m51CC5344D4221F8DA190433C307008EEEC107F1E,
	BringerOfDeathEnemy_executeAttack_m4905EECF5FAA0CAA0B12325EA2B533ED42FE5C1D,
	BringerOfDeathEnemy_unitAttackAnimation_m09105C0B1CC1713F11CCE6F12BB07421DC4A2995,
	BringerOfDeathEnemy__ctor_mE070E625F83E9FEB4D5C94E8A19F8F0B32B5A30F,
	NecromancerEnemy_Awake_m0E75F8BF5D40DEFD546A985FD1DBE37B6BC8F510,
	NecromancerEnemy_executeAttack_m941168B849A0E1132E82D8C3F0D3331248EEB8ED,
	NecromancerEnemy_unitAttackAnimation_m29EC32A09C3F74BFB7698DDE1BFB58ED5910B0BB,
	NecromancerEnemy__ctor_m0725BD84F9533ABF0B36127606516675E3145267,
	RogueEnemy_Awake_m1DF96D37383FBCDB2EF0C3725283B7A9A7C4E843,
	RogueEnemy_executeAttack_m5A585EC53DCE48812BAE68C36D2F02040DCF19B2,
	RogueEnemy_unitAttackAnimation_mFEAA6F71367C52D424FFB068A7048EBEEAB447E5,
	RogueEnemy__ctor_m453E9D9430FF0B20731473626D94011D8D3CB81D,
	WarlockEnemy_Awake_mAAC269CD97ED1ABAF7FC1501B3EF0381B2810F1D,
	WarlockEnemy_executeAttack_m2597DB13645EA75C81D6AD3D370DF45FA6C5F933,
	WarlockEnemy_unitAttackAnimation_m47960705E9ACE6A6B9D05A200E5F8CB82301ECCA,
	WarlockEnemy__ctor_m3BFEC86F391E623614F0F8AF1D2936A0AA0A7F1D,
	FireMage__ctor_m1C43B75FDF47B4B1163A6B1C1A5935C59F72D77E,
	FireMage_Start_m45005B1F9B63A4A5BD9DE757487ACCB5F7489018,
	FireMage_unitAttackAnimation_m391BF201FEF2DBDCF578FE90F7F22400DE9B55CD,
	BarbarianPlayer_Awake_m6975C01899526BF9FFAB6CDE993DA2742245CDAD,
	BarbarianPlayer_unitAttackAnimation_m223CEC08448C3BBD8318A122344FBBBC41B4C9B6,
	BarbarianPlayer__ctor_m14E5B4833C7717D33A8596690863A57B5703BF16,
	BlademasterPlayer_Awake_mDEEABAE528BA26907A2932387CA67082C19F8A19,
	BlademasterPlayer_unitAttackAnimation_m5345CC1CDC81943335D2A75E49284D3B41722224,
	BlademasterPlayer__ctor_mCC07B2A1C0CA9DCB71AAFB47F11BD5232D602205,
	FireMagePlayer_Awake_m356BDEED8FED794B2D0D9744E985246FB29A5721,
	FireMagePlayer_unitAttackAnimation_m5CAC42A96C2749E41F925A9A61697158D349BFDC,
	FireMagePlayer__ctor_mBAFBAD4D418CC12C354FB0D91A9EBE3BACEF0944,
	PriestPlayer_Awake_mB9D37B83E0CB77137D882A0DE068A3BE7DEC9DF3,
	PriestPlayer_unitAttackAnimation_m704F4A6EE4DF3C7979567B9CC3DFC951112E02C1,
	PriestPlayer__ctor_m7015CEB6C571AFBFAD53DD09CCD4DC02ABAB2EB4,
	FireMageAnimation_Start_m9744FC8ADE0F019F4ECEC5F55337D4EE6C7C39F7,
	FireMageAnimation_Update_mF8E68B52F884A9F4F167F73724345EF5C7330ADF,
	FireMageAnimation__ctor_mD3C8F1A239B6C41A75FF245B5AEF3BABDB59BDCF,
	Unit_Start_mDA1EE65F2CDF938E70BB7D8DEFFEE34AF066D943,
	Unit_Update_m57C1F3E193B819FC928ED8D8D4DF500357DBC6D1,
	Unit_TakeDamage_m0A507F2F78DDAB20CE032AAA11CAF2B210E6C2F2,
	Unit_OnMouseDown_m0E84FC160EDA6EA0D70525DD70BE83E8E0DDB9D2,
	Unit_OnMouseOver_mF7C7C77573479F82FDE4A57694FE3464D61BE5BE,
	Unit_OnMouseExit_m33EF972FF5AAF0A20CBF8B29E571393859FA5CC5,
	Unit_focusHud_m160BA605D1ED1B342532E05883FC10F789A03907,
	Unit_triggerAttack_m515D1D463617170205A902B472E2408760513683,
	Unit_TriggerMiss_m4507702975F1D39AA0217395CD6E3F7F5ABE828A,
	Unit_unitAttackAnimation_mC3D45D69D029DA8F947EEB59CFC043138DF89461,
	Unit_SetHud_m1DB626658355F549BF574D68AA8F67E19D2A7DDA,
	Unit_heal_m8B7E5912C431540B8EA75E03BD91845FAACBCD7D,
	Unit_removeTargets_m8B05E9EB9B859982B0DA2CAE56515157CC7D25DE,
	Unit_addAoETargetListener_mE0B3C250E29FF561535D5393AC8B74F263616D75,
	Unit_highLightOnAoEHover_m3BEC4215D4376251092314365A1B08E99DA97411,
	Unit_removeHighlight_m809612AC49D2895D050E599E1F96FC8473A6F2F7,
	Unit_ActivatePersistant_mF28E07A9D1BA1F8551D91C845E7E0968AD64EA88,
	Unit_RemovePersistant_mFFCEBFA19BD0B708757A0606A6AA04E0088AA9EF,
	Unit__ctor_mC26B9B196D3C2E0A8C80BA5BAB65286FF5038C58,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
};
static const int32_t s_InvokerIndices[699] = 
{
	1541,
	1541,
	1541,
	1541,
	1519,
	1290,
	1290,
	1541,
	1309,
	1541,
	1541,
	1541,
	1541,
	2046,
	1892,
	1541,
	693,
	778,
	1280,
	1249,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1519,
	1290,
	1290,
	1541,
	1309,
	1541,
	1541,
	1541,
	1541,
	2046,
	1892,
	1541,
	693,
	778,
	1280,
	1249,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1280,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1290,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1519,
	1290,
	1290,
	1541,
	1309,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	488,
	1541,
	2429,
	1541,
	935,
	1115,
	944,
	1309,
	1541,
	1541,
	1541,
	1499,
	1290,
	1003,
	1003,
	1541,
	1541,
	1290,
	778,
	1541,
	1541,
	778,
	1290,
	778,
	778,
	1290,
	1290,
	1541,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	778,
	488,
	1541,
	778,
	1290,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1499,
	944,
	1541,
	774,
	486,
	1541,
	1541,
	1541,
	1541,
	776,
	1541,
	572,
	1290,
	776,
	1541,
	572,
	1290,
	776,
	778,
	227,
	1290,
	776,
	1290,
	384,
	1290,
	776,
	1290,
	384,
	1290,
	776,
	1541,
	572,
	1290,
	776,
	778,
	227,
	1290,
	776,
	778,
	227,
	1290,
	776,
	1290,
	384,
	1290,
	776,
	1290,
	384,
	1290,
	776,
	1290,
	384,
	1290,
	776,
	1290,
	384,
	1290,
	776,
	1290,
	384,
	1290,
	776,
	1541,
	572,
	1290,
	2429,
	2165,
	2429,
	2429,
	2324,
	1541,
	2429,
	1541,
	1541,
	1541,
	1541,
	478,
	1290,
	1541,
	1541,
	478,
	1290,
	1541,
	1541,
	1290,
	478,
	1290,
	1541,
	1541,
	478,
	1290,
	1541,
	1541,
	478,
	1290,
	1541,
	1290,
	1290,
	1290,
	1290,
	774,
	1541,
	1541,
	1541,
	1541,
	1290,
	774,
	1290,
	1521,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	721,
	1541,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1290,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	778,
	1541,
	1541,
	1541,
	774,
	1280,
	1541,
	1541,
	774,
	1280,
	1541,
	1541,
	774,
	1280,
	1541,
	1541,
	774,
	1280,
	1541,
	1541,
	1541,
	1280,
	1541,
	1280,
	1541,
	1541,
	1280,
	1541,
	1541,
	1280,
	1541,
	1541,
	1280,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1309,
	1541,
	1541,
	1541,
	1541,
	721,
	721,
	1280,
	1290,
	1280,
	1541,
	1290,
	1290,
	1541,
	1290,
	1290,
	1541,
	1541,
	1541,
	1290,
	1541,
	1541,
	1541,
	1541,
	1499,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	344,
	1541,
	344,
	1541,
	1499,
	1290,
	1499,
	1290,
	1499,
	1290,
	1499,
	1290,
	1499,
	1290,
	1541,
	1541,
	1290,
	1290,
	655,
	655,
	478,
	478,
	486,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1499,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1499,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1499,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1541,
	1541,
	1541,
	1003,
	1499,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1541,
	1541,
	1541,
	1541,
	1280,
	1541,
	1541,
	1541,
	655,
	655,
	478,
	478,
	486,
	1541,
	1541,
	1541,
	1541,
	1290,
	1290,
	1541,
	1541,
	1541,
	1541,
	1290,
	1541,
	1290,
	1290,
	1290,
	1290,
	1280,
	1541,
	1541,
	1541,
	1541,
	1280,
	1541,
	1541,
	1280,
	1541,
	1541,
	1499,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1541,
	1541,
	1290,
	1003,
	1003,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1499,
	1499,
	1541,
	2429,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1541,
	1541,
	1541,
	1499,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1541,
	1541,
	1290,
	1499,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1541,
	1541,
	1290,
	1499,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1541,
	1541,
	1290,
	1499,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1541,
	1541,
	1290,
	1499,
	1541,
	1541,
	527,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
	1541,
	1541,
	1003,
	1499,
	1541,
	1280,
	1541,
	1519,
	1499,
	1541,
	1499,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	699,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
